package com.truesoft.ltl.web.rest;

import com.truesoft.ltl.NewLtlApp;

import com.truesoft.ltl.domain.PostalCode;
import com.truesoft.ltl.repository.PostalCodeRepository;
import com.truesoft.ltl.service.PostalCodeService;
import com.truesoft.ltl.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PostalCodeResource REST controller.
 *
 * @see PostalCodeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewLtlApp.class)
public class PostalCodeResourceIntTest {

    private static final String DEFAULT_ZIP_CODE = "AAAAAAAAAA";
    private static final String UPDATED_ZIP_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_STATE = "AAAAAAAAAA";
    private static final String UPDATED_STATE = "BBBBBBBBBB";

    private static final String DEFAULT_CITY = "AAAAAAAAAA";
    private static final String UPDATED_CITY = "BBBBBBBBBB";

    private static final String DEFAULT_COUNTY = "AAAAAAAAAA";
    private static final String UPDATED_COUNTY = "BBBBBBBBBB";

    private static final Integer DEFAULT_STATE_FIPS = 1;
    private static final Integer UPDATED_STATE_FIPS = 2;

    private static final Integer DEFAULT_COUNTY_FIP = 1;
    private static final Integer UPDATED_COUNTY_FIP = 2;

    private static final Double DEFAULT_LATITUDE = 1D;
    private static final Double UPDATED_LATITUDE = 2D;

    private static final Double DEFAULT_LONGITUDE = 1D;
    private static final Double UPDATED_LONGITUDE = 2D;

    private static final String DEFAULT_PREFERENCE = "AAAAAAAAAA";
    private static final String UPDATED_PREFERENCE = "BBBBBBBBBB";

    private static final String DEFAULT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_TYPE = "BBBBBBBBBB";

    @Autowired
    private PostalCodeRepository postalCodeRepository;

    @Autowired
    private PostalCodeService postalCodeService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPostalCodeMockMvc;

    private PostalCode postalCode;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PostalCodeResource postalCodeResource = new PostalCodeResource(postalCodeService);
        this.restPostalCodeMockMvc = MockMvcBuilders.standaloneSetup(postalCodeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PostalCode createEntity(EntityManager em) {
        PostalCode postalCode = new PostalCode()
            .zipCode(DEFAULT_ZIP_CODE)
            .state(DEFAULT_STATE)
            .city(DEFAULT_CITY)
            .county(DEFAULT_COUNTY)
            .stateFips(DEFAULT_STATE_FIPS)
            .countyFip(DEFAULT_COUNTY_FIP)
            .latitude(DEFAULT_LATITUDE)
            .longitude(DEFAULT_LONGITUDE)
            .preference(DEFAULT_PREFERENCE)
            .type(DEFAULT_TYPE);
        return postalCode;
    }

    @Before
    public void initTest() {
        postalCode = createEntity(em);
    }

    @Test
    @Transactional
    public void createPostalCode() throws Exception {
        int databaseSizeBeforeCreate = postalCodeRepository.findAll().size();

        // Create the PostalCode
        restPostalCodeMockMvc.perform(post("/api/postal-codes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(postalCode)))
            .andExpect(status().isCreated());

        // Validate the PostalCode in the database
        List<PostalCode> postalCodeList = postalCodeRepository.findAll();
        assertThat(postalCodeList).hasSize(databaseSizeBeforeCreate + 1);
        PostalCode testPostalCode = postalCodeList.get(postalCodeList.size() - 1);
        assertThat(testPostalCode.getZipCode()).isEqualTo(DEFAULT_ZIP_CODE);
        assertThat(testPostalCode.getState()).isEqualTo(DEFAULT_STATE);
        assertThat(testPostalCode.getCity()).isEqualTo(DEFAULT_CITY);
        assertThat(testPostalCode.getCounty()).isEqualTo(DEFAULT_COUNTY);
        assertThat(testPostalCode.getStateFips()).isEqualTo(DEFAULT_STATE_FIPS);
        assertThat(testPostalCode.getCountyFip()).isEqualTo(DEFAULT_COUNTY_FIP);
        assertThat(testPostalCode.getLatitude()).isEqualTo(DEFAULT_LATITUDE);
        assertThat(testPostalCode.getLongitude()).isEqualTo(DEFAULT_LONGITUDE);
        assertThat(testPostalCode.getPreference()).isEqualTo(DEFAULT_PREFERENCE);
        assertThat(testPostalCode.getType()).isEqualTo(DEFAULT_TYPE);
    }

    @Test
    @Transactional
    public void createPostalCodeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = postalCodeRepository.findAll().size();

        // Create the PostalCode with an existing ID
        postalCode.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPostalCodeMockMvc.perform(post("/api/postal-codes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(postalCode)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<PostalCode> postalCodeList = postalCodeRepository.findAll();
        assertThat(postalCodeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllPostalCodes() throws Exception {
        // Initialize the database
        postalCodeRepository.saveAndFlush(postalCode);

        // Get all the postalCodeList
        restPostalCodeMockMvc.perform(get("/api/postal-codes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(postalCode.getId().intValue())))
            .andExpect(jsonPath("$.[*].zipCode").value(hasItem(DEFAULT_ZIP_CODE.toString())))
            .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE.toString())))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY.toString())))
            .andExpect(jsonPath("$.[*].county").value(hasItem(DEFAULT_COUNTY.toString())))
            .andExpect(jsonPath("$.[*].stateFips").value(hasItem(DEFAULT_STATE_FIPS)))
            .andExpect(jsonPath("$.[*].countyFip").value(hasItem(DEFAULT_COUNTY_FIP)))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].preference").value(hasItem(DEFAULT_PREFERENCE.toString())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())));
    }

    @Test
    @Transactional
    public void getPostalCode() throws Exception {
        // Initialize the database
        postalCodeRepository.saveAndFlush(postalCode);

        // Get the postalCode
        restPostalCodeMockMvc.perform(get("/api/postal-codes/{id}", postalCode.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(postalCode.getId().intValue()))
            .andExpect(jsonPath("$.zipCode").value(DEFAULT_ZIP_CODE.toString()))
            .andExpect(jsonPath("$.state").value(DEFAULT_STATE.toString()))
            .andExpect(jsonPath("$.city").value(DEFAULT_CITY.toString()))
            .andExpect(jsonPath("$.county").value(DEFAULT_COUNTY.toString()))
            .andExpect(jsonPath("$.stateFips").value(DEFAULT_STATE_FIPS))
            .andExpect(jsonPath("$.countyFip").value(DEFAULT_COUNTY_FIP))
            .andExpect(jsonPath("$.latitude").value(DEFAULT_LATITUDE.doubleValue()))
            .andExpect(jsonPath("$.longitude").value(DEFAULT_LONGITUDE.doubleValue()))
            .andExpect(jsonPath("$.preference").value(DEFAULT_PREFERENCE.toString()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPostalCode() throws Exception {
        // Get the postalCode
        restPostalCodeMockMvc.perform(get("/api/postal-codes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePostalCode() throws Exception {
        // Initialize the database
        postalCodeService.save(postalCode);

        int databaseSizeBeforeUpdate = postalCodeRepository.findAll().size();

        // Update the postalCode
        PostalCode updatedPostalCode = postalCodeRepository.findOne(postalCode.getId());
        updatedPostalCode
            .zipCode(UPDATED_ZIP_CODE)
            .state(UPDATED_STATE)
            .city(UPDATED_CITY)
            .county(UPDATED_COUNTY)
            .stateFips(UPDATED_STATE_FIPS)
            .countyFip(UPDATED_COUNTY_FIP)
            .latitude(UPDATED_LATITUDE)
            .longitude(UPDATED_LONGITUDE)
            .preference(UPDATED_PREFERENCE)
            .type(UPDATED_TYPE);

        restPostalCodeMockMvc.perform(put("/api/postal-codes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPostalCode)))
            .andExpect(status().isOk());

        // Validate the PostalCode in the database
        List<PostalCode> postalCodeList = postalCodeRepository.findAll();
        assertThat(postalCodeList).hasSize(databaseSizeBeforeUpdate);
        PostalCode testPostalCode = postalCodeList.get(postalCodeList.size() - 1);
        assertThat(testPostalCode.getZipCode()).isEqualTo(UPDATED_ZIP_CODE);
        assertThat(testPostalCode.getState()).isEqualTo(UPDATED_STATE);
        assertThat(testPostalCode.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testPostalCode.getCounty()).isEqualTo(UPDATED_COUNTY);
        assertThat(testPostalCode.getStateFips()).isEqualTo(UPDATED_STATE_FIPS);
        assertThat(testPostalCode.getCountyFip()).isEqualTo(UPDATED_COUNTY_FIP);
        assertThat(testPostalCode.getLatitude()).isEqualTo(UPDATED_LATITUDE);
        assertThat(testPostalCode.getLongitude()).isEqualTo(UPDATED_LONGITUDE);
        assertThat(testPostalCode.getPreference()).isEqualTo(UPDATED_PREFERENCE);
        assertThat(testPostalCode.getType()).isEqualTo(UPDATED_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingPostalCode() throws Exception {
        int databaseSizeBeforeUpdate = postalCodeRepository.findAll().size();

        // Create the PostalCode

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPostalCodeMockMvc.perform(put("/api/postal-codes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(postalCode)))
            .andExpect(status().isCreated());

        // Validate the PostalCode in the database
        List<PostalCode> postalCodeList = postalCodeRepository.findAll();
        assertThat(postalCodeList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePostalCode() throws Exception {
        // Initialize the database
        postalCodeService.save(postalCode);

        int databaseSizeBeforeDelete = postalCodeRepository.findAll().size();

        // Get the postalCode
        restPostalCodeMockMvc.perform(delete("/api/postal-codes/{id}", postalCode.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<PostalCode> postalCodeList = postalCodeRepository.findAll();
        assertThat(postalCodeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PostalCode.class);
        PostalCode postalCode1 = new PostalCode();
        postalCode1.setId(1L);
        PostalCode postalCode2 = new PostalCode();
        postalCode2.setId(postalCode1.getId());
        assertThat(postalCode1).isEqualTo(postalCode2);
        postalCode2.setId(2L);
        assertThat(postalCode1).isNotEqualTo(postalCode2);
        postalCode1.setId(null);
        assertThat(postalCode1).isNotEqualTo(postalCode2);
    }
}
