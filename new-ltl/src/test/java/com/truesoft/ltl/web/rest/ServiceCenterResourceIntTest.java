package com.truesoft.ltl.web.rest;

import com.truesoft.ltl.NewLtlApp;

import com.truesoft.ltl.domain.ServiceCenter;
import com.truesoft.ltl.repository.ServiceCenterRepository;
import com.truesoft.ltl.service.ServiceCenterService;
import com.truesoft.ltl.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ServiceCenterResource REST controller.
 *
 * @see ServiceCenterResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewLtlApp.class)
public class ServiceCenterResourceIntTest {

    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_ALPHA_CODE = "AAAAAAAAAA";
    private static final String UPDATED_ALPHA_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_CITY_STATE_ZIP = "AAAAAAAAAA";
    private static final String UPDATED_CITY_STATE_ZIP = "BBBBBBBBBB";

    private static final String DEFAULT_COUNTRY = "AAAAAAAAAA";
    private static final String UPDATED_COUNTRY = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_FAX = "AAAAAAAAAA";
    private static final String UPDATED_FAX = "BBBBBBBBBB";

    private static final String DEFAULT_MANAGER = "AAAAAAAAAA";
    private static final String UPDATED_MANAGER = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_PHONE = "AAAAAAAAAA";
    private static final String UPDATED_PHONE = "BBBBBBBBBB";

    @Autowired
    private ServiceCenterRepository serviceCenterRepository;

    @Autowired
    private ServiceCenterService serviceCenterService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restServiceCenterMockMvc;

    private ServiceCenter serviceCenter;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ServiceCenterResource serviceCenterResource = new ServiceCenterResource(serviceCenterService);
        this.restServiceCenterMockMvc = MockMvcBuilders.standaloneSetup(serviceCenterResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ServiceCenter createEntity(EntityManager em) {
        ServiceCenter serviceCenter = new ServiceCenter()
            .address(DEFAULT_ADDRESS)
            .alphaCode(DEFAULT_ALPHA_CODE)
            .cityStateZip(DEFAULT_CITY_STATE_ZIP)
            .country(DEFAULT_COUNTRY)
            .emailAddress(DEFAULT_EMAIL_ADDRESS)
            .fax(DEFAULT_FAX)
            .manager(DEFAULT_MANAGER)
            .name(DEFAULT_NAME)
            .phone(DEFAULT_PHONE);
        return serviceCenter;
    }

    @Before
    public void initTest() {
        serviceCenter = createEntity(em);
    }

    @Test
    @Transactional
    public void createServiceCenter() throws Exception {
        int databaseSizeBeforeCreate = serviceCenterRepository.findAll().size();

        // Create the ServiceCenter
        restServiceCenterMockMvc.perform(post("/api/service-centers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(serviceCenter)))
            .andExpect(status().isCreated());

        // Validate the ServiceCenter in the database
        List<ServiceCenter> serviceCenterList = serviceCenterRepository.findAll();
        assertThat(serviceCenterList).hasSize(databaseSizeBeforeCreate + 1);
        ServiceCenter testServiceCenter = serviceCenterList.get(serviceCenterList.size() - 1);
        assertThat(testServiceCenter.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testServiceCenter.getAlphaCode()).isEqualTo(DEFAULT_ALPHA_CODE);
        assertThat(testServiceCenter.getCityStateZip()).isEqualTo(DEFAULT_CITY_STATE_ZIP);
        assertThat(testServiceCenter.getCountry()).isEqualTo(DEFAULT_COUNTRY);
        assertThat(testServiceCenter.getEmailAddress()).isEqualTo(DEFAULT_EMAIL_ADDRESS);
        assertThat(testServiceCenter.getFax()).isEqualTo(DEFAULT_FAX);
        assertThat(testServiceCenter.getManager()).isEqualTo(DEFAULT_MANAGER);
        assertThat(testServiceCenter.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testServiceCenter.getPhone()).isEqualTo(DEFAULT_PHONE);
    }

    @Test
    @Transactional
    public void createServiceCenterWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = serviceCenterRepository.findAll().size();

        // Create the ServiceCenter with an existing ID
        serviceCenter.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restServiceCenterMockMvc.perform(post("/api/service-centers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(serviceCenter)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<ServiceCenter> serviceCenterList = serviceCenterRepository.findAll();
        assertThat(serviceCenterList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllServiceCenters() throws Exception {
        // Initialize the database
        serviceCenterRepository.saveAndFlush(serviceCenter);

        // Get all the serviceCenterList
        restServiceCenterMockMvc.perform(get("/api/service-centers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(serviceCenter.getId().intValue())))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].alphaCode").value(hasItem(DEFAULT_ALPHA_CODE.toString())))
            .andExpect(jsonPath("$.[*].cityStateZip").value(hasItem(DEFAULT_CITY_STATE_ZIP.toString())))
            .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY.toString())))
            .andExpect(jsonPath("$.[*].emailAddress").value(hasItem(DEFAULT_EMAIL_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].fax").value(hasItem(DEFAULT_FAX.toString())))
            .andExpect(jsonPath("$.[*].manager").value(hasItem(DEFAULT_MANAGER.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE.toString())));
    }

    @Test
    @Transactional
    public void getServiceCenter() throws Exception {
        // Initialize the database
        serviceCenterRepository.saveAndFlush(serviceCenter);

        // Get the serviceCenter
        restServiceCenterMockMvc.perform(get("/api/service-centers/{id}", serviceCenter.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(serviceCenter.getId().intValue()))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS.toString()))
            .andExpect(jsonPath("$.alphaCode").value(DEFAULT_ALPHA_CODE.toString()))
            .andExpect(jsonPath("$.cityStateZip").value(DEFAULT_CITY_STATE_ZIP.toString()))
            .andExpect(jsonPath("$.country").value(DEFAULT_COUNTRY.toString()))
            .andExpect(jsonPath("$.emailAddress").value(DEFAULT_EMAIL_ADDRESS.toString()))
            .andExpect(jsonPath("$.fax").value(DEFAULT_FAX.toString()))
            .andExpect(jsonPath("$.manager").value(DEFAULT_MANAGER.toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingServiceCenter() throws Exception {
        // Get the serviceCenter
        restServiceCenterMockMvc.perform(get("/api/service-centers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateServiceCenter() throws Exception {
        // Initialize the database
        serviceCenterService.save(serviceCenter);

        int databaseSizeBeforeUpdate = serviceCenterRepository.findAll().size();

        // Update the serviceCenter
        ServiceCenter updatedServiceCenter = serviceCenterRepository.findOne(serviceCenter.getId());
        updatedServiceCenter
            .address(UPDATED_ADDRESS)
            .alphaCode(UPDATED_ALPHA_CODE)
            .cityStateZip(UPDATED_CITY_STATE_ZIP)
            .country(UPDATED_COUNTRY)
            .emailAddress(UPDATED_EMAIL_ADDRESS)
            .fax(UPDATED_FAX)
            .manager(UPDATED_MANAGER)
            .name(UPDATED_NAME)
            .phone(UPDATED_PHONE);

        restServiceCenterMockMvc.perform(put("/api/service-centers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedServiceCenter)))
            .andExpect(status().isOk());

        // Validate the ServiceCenter in the database
        List<ServiceCenter> serviceCenterList = serviceCenterRepository.findAll();
        assertThat(serviceCenterList).hasSize(databaseSizeBeforeUpdate);
        ServiceCenter testServiceCenter = serviceCenterList.get(serviceCenterList.size() - 1);
        assertThat(testServiceCenter.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testServiceCenter.getAlphaCode()).isEqualTo(UPDATED_ALPHA_CODE);
        assertThat(testServiceCenter.getCityStateZip()).isEqualTo(UPDATED_CITY_STATE_ZIP);
        assertThat(testServiceCenter.getCountry()).isEqualTo(UPDATED_COUNTRY);
        assertThat(testServiceCenter.getEmailAddress()).isEqualTo(UPDATED_EMAIL_ADDRESS);
        assertThat(testServiceCenter.getFax()).isEqualTo(UPDATED_FAX);
        assertThat(testServiceCenter.getManager()).isEqualTo(UPDATED_MANAGER);
        assertThat(testServiceCenter.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testServiceCenter.getPhone()).isEqualTo(UPDATED_PHONE);
    }

    @Test
    @Transactional
    public void updateNonExistingServiceCenter() throws Exception {
        int databaseSizeBeforeUpdate = serviceCenterRepository.findAll().size();

        // Create the ServiceCenter

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restServiceCenterMockMvc.perform(put("/api/service-centers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(serviceCenter)))
            .andExpect(status().isCreated());

        // Validate the ServiceCenter in the database
        List<ServiceCenter> serviceCenterList = serviceCenterRepository.findAll();
        assertThat(serviceCenterList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteServiceCenter() throws Exception {
        // Initialize the database
        serviceCenterService.save(serviceCenter);

        int databaseSizeBeforeDelete = serviceCenterRepository.findAll().size();

        // Get the serviceCenter
        restServiceCenterMockMvc.perform(delete("/api/service-centers/{id}", serviceCenter.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ServiceCenter> serviceCenterList = serviceCenterRepository.findAll();
        assertThat(serviceCenterList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ServiceCenter.class);
        ServiceCenter serviceCenter1 = new ServiceCenter();
        serviceCenter1.setId(1L);
        ServiceCenter serviceCenter2 = new ServiceCenter();
        serviceCenter2.setId(serviceCenter1.getId());
        assertThat(serviceCenter1).isEqualTo(serviceCenter2);
        serviceCenter2.setId(2L);
        assertThat(serviceCenter1).isNotEqualTo(serviceCenter2);
        serviceCenter1.setId(null);
        assertThat(serviceCenter1).isNotEqualTo(serviceCenter2);
    }
}
