package com.truesoft.ltl.web.rest;

import com.truesoft.ltl.NewLtlApp;

import com.truesoft.ltl.domain.FreightUnit;
import com.truesoft.ltl.repository.FreightUnitRepository;
import com.truesoft.ltl.service.FreightUnitService;
import com.truesoft.ltl.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the FreightUnitResource REST controller.
 *
 * @see FreightUnitResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewLtlApp.class)
public class FreightUnitResourceIntTest {

    private static final Integer DEFAULT_ACTUAL_CLASS = 1;
    private static final Integer UPDATED_ACTUAL_CLASS = 2;

    private static final Integer DEFAULT_DIM_L = 1;
    private static final Integer UPDATED_DIM_L = 2;

    private static final Integer DEFAULT_DIM_W = 1;
    private static final Integer UPDATED_DIM_W = 2;

    private static final Integer DEFAULT_DIM_H = 1;
    private static final Integer UPDATED_DIM_H = 2;

    private static final String DEFAULT_DIM_UNIT = "AAAAAAAAAA";
    private static final String UPDATED_DIM_UNIT = "BBBBBBBBBB";

    private static final Integer DEFAULT_NMFC = 1;
    private static final Integer UPDATED_NMFC = 2;

    private static final Integer DEFAULT_NMFC_SUB = 1;
    private static final Integer UPDATED_NMFC_SUB = 2;

    private static final Integer DEFAULT_NUMBER_OF_UNITS = 1;
    private static final Integer UPDATED_NUMBER_OF_UNITS = 2;

    private static final Integer DEFAULT_RATED_CLASS = 1;
    private static final Integer UPDATED_RATED_CLASS = 2;

    private static final Integer DEFAULT_WEIGHT = 1;
    private static final Integer UPDATED_WEIGHT = 2;

    @Autowired
    private FreightUnitRepository freightUnitRepository;

    @Autowired
    private FreightUnitService freightUnitService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restFreightUnitMockMvc;

    private FreightUnit freightUnit;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        FreightUnitResource freightUnitResource = new FreightUnitResource(freightUnitService);
        this.restFreightUnitMockMvc = MockMvcBuilders.standaloneSetup(freightUnitResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FreightUnit createEntity(EntityManager em) {
        FreightUnit freightUnit = new FreightUnit()
            .actualClass(DEFAULT_ACTUAL_CLASS)
            .dimL(DEFAULT_DIM_L)
            .dimW(DEFAULT_DIM_W)
            .dimH(DEFAULT_DIM_H)
            .dimUnit(DEFAULT_DIM_UNIT)
            .nmfc(DEFAULT_NMFC)
            .nmfcSub(DEFAULT_NMFC_SUB)
            .numberOfUnits(DEFAULT_NUMBER_OF_UNITS)
            .ratedClass(DEFAULT_RATED_CLASS)
            .weight(DEFAULT_WEIGHT);
        return freightUnit;
    }

    @Before
    public void initTest() {
        freightUnit = createEntity(em);
    }

    @Test
    @Transactional
    public void createFreightUnit() throws Exception {
        int databaseSizeBeforeCreate = freightUnitRepository.findAll().size();

        // Create the FreightUnit
        restFreightUnitMockMvc.perform(post("/api/freight-units")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(freightUnit)))
            .andExpect(status().isCreated());

        // Validate the FreightUnit in the database
        List<FreightUnit> freightUnitList = freightUnitRepository.findAll();
        assertThat(freightUnitList).hasSize(databaseSizeBeforeCreate + 1);
        FreightUnit testFreightUnit = freightUnitList.get(freightUnitList.size() - 1);
        assertThat(testFreightUnit.getActualClass()).isEqualTo(DEFAULT_ACTUAL_CLASS);
        assertThat(testFreightUnit.getDimL()).isEqualTo(DEFAULT_DIM_L);
        assertThat(testFreightUnit.getDimW()).isEqualTo(DEFAULT_DIM_W);
        assertThat(testFreightUnit.getDimH()).isEqualTo(DEFAULT_DIM_H);
        assertThat(testFreightUnit.getDimUnit()).isEqualTo(DEFAULT_DIM_UNIT);
        assertThat(testFreightUnit.getNmfc()).isEqualTo(DEFAULT_NMFC);
        assertThat(testFreightUnit.getNmfcSub()).isEqualTo(DEFAULT_NMFC_SUB);
        assertThat(testFreightUnit.getNumberOfUnits()).isEqualTo(DEFAULT_NUMBER_OF_UNITS);
        assertThat(testFreightUnit.getRatedClass()).isEqualTo(DEFAULT_RATED_CLASS);
        assertThat(testFreightUnit.getWeight()).isEqualTo(DEFAULT_WEIGHT);
    }

    @Test
    @Transactional
    public void createFreightUnitWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = freightUnitRepository.findAll().size();

        // Create the FreightUnit with an existing ID
        freightUnit.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFreightUnitMockMvc.perform(post("/api/freight-units")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(freightUnit)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<FreightUnit> freightUnitList = freightUnitRepository.findAll();
        assertThat(freightUnitList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkWeightIsRequired() throws Exception {
        int databaseSizeBeforeTest = freightUnitRepository.findAll().size();
        // set the field null
        freightUnit.setWeight(null);

        // Create the FreightUnit, which fails.

        restFreightUnitMockMvc.perform(post("/api/freight-units")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(freightUnit)))
            .andExpect(status().isBadRequest());

        List<FreightUnit> freightUnitList = freightUnitRepository.findAll();
        assertThat(freightUnitList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllFreightUnits() throws Exception {
        // Initialize the database
        freightUnitRepository.saveAndFlush(freightUnit);

        // Get all the freightUnitList
        restFreightUnitMockMvc.perform(get("/api/freight-units?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(freightUnit.getId().intValue())))
            .andExpect(jsonPath("$.[*].actualClass").value(hasItem(DEFAULT_ACTUAL_CLASS)))
            .andExpect(jsonPath("$.[*].dimL").value(hasItem(DEFAULT_DIM_L)))
            .andExpect(jsonPath("$.[*].dimW").value(hasItem(DEFAULT_DIM_W)))
            .andExpect(jsonPath("$.[*].dimH").value(hasItem(DEFAULT_DIM_H)))
            .andExpect(jsonPath("$.[*].dimUnit").value(hasItem(DEFAULT_DIM_UNIT.toString())))
            .andExpect(jsonPath("$.[*].nmfc").value(hasItem(DEFAULT_NMFC)))
            .andExpect(jsonPath("$.[*].nmfcSub").value(hasItem(DEFAULT_NMFC_SUB)))
            .andExpect(jsonPath("$.[*].numberOfUnits").value(hasItem(DEFAULT_NUMBER_OF_UNITS)))
            .andExpect(jsonPath("$.[*].ratedClass").value(hasItem(DEFAULT_RATED_CLASS)))
            .andExpect(jsonPath("$.[*].weight").value(hasItem(DEFAULT_WEIGHT)));
    }

    @Test
    @Transactional
    public void getFreightUnit() throws Exception {
        // Initialize the database
        freightUnitRepository.saveAndFlush(freightUnit);

        // Get the freightUnit
        restFreightUnitMockMvc.perform(get("/api/freight-units/{id}", freightUnit.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(freightUnit.getId().intValue()))
            .andExpect(jsonPath("$.actualClass").value(DEFAULT_ACTUAL_CLASS))
            .andExpect(jsonPath("$.dimL").value(DEFAULT_DIM_L))
            .andExpect(jsonPath("$.dimW").value(DEFAULT_DIM_W))
            .andExpect(jsonPath("$.dimH").value(DEFAULT_DIM_H))
            .andExpect(jsonPath("$.dimUnit").value(DEFAULT_DIM_UNIT.toString()))
            .andExpect(jsonPath("$.nmfc").value(DEFAULT_NMFC))
            .andExpect(jsonPath("$.nmfcSub").value(DEFAULT_NMFC_SUB))
            .andExpect(jsonPath("$.numberOfUnits").value(DEFAULT_NUMBER_OF_UNITS))
            .andExpect(jsonPath("$.ratedClass").value(DEFAULT_RATED_CLASS))
            .andExpect(jsonPath("$.weight").value(DEFAULT_WEIGHT));
    }

    @Test
    @Transactional
    public void getNonExistingFreightUnit() throws Exception {
        // Get the freightUnit
        restFreightUnitMockMvc.perform(get("/api/freight-units/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFreightUnit() throws Exception {
        // Initialize the database
        freightUnitService.save(freightUnit);

        int databaseSizeBeforeUpdate = freightUnitRepository.findAll().size();

        // Update the freightUnit
        FreightUnit updatedFreightUnit = freightUnitRepository.findOne(freightUnit.getId());
        updatedFreightUnit
            .actualClass(UPDATED_ACTUAL_CLASS)
            .dimL(UPDATED_DIM_L)
            .dimW(UPDATED_DIM_W)
            .dimH(UPDATED_DIM_H)
            .dimUnit(UPDATED_DIM_UNIT)
            .nmfc(UPDATED_NMFC)
            .nmfcSub(UPDATED_NMFC_SUB)
            .numberOfUnits(UPDATED_NUMBER_OF_UNITS)
            .ratedClass(UPDATED_RATED_CLASS)
            .weight(UPDATED_WEIGHT);

        restFreightUnitMockMvc.perform(put("/api/freight-units")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedFreightUnit)))
            .andExpect(status().isOk());

        // Validate the FreightUnit in the database
        List<FreightUnit> freightUnitList = freightUnitRepository.findAll();
        assertThat(freightUnitList).hasSize(databaseSizeBeforeUpdate);
        FreightUnit testFreightUnit = freightUnitList.get(freightUnitList.size() - 1);
        assertThat(testFreightUnit.getActualClass()).isEqualTo(UPDATED_ACTUAL_CLASS);
        assertThat(testFreightUnit.getDimL()).isEqualTo(UPDATED_DIM_L);
        assertThat(testFreightUnit.getDimW()).isEqualTo(UPDATED_DIM_W);
        assertThat(testFreightUnit.getDimH()).isEqualTo(UPDATED_DIM_H);
        assertThat(testFreightUnit.getDimUnit()).isEqualTo(UPDATED_DIM_UNIT);
        assertThat(testFreightUnit.getNmfc()).isEqualTo(UPDATED_NMFC);
        assertThat(testFreightUnit.getNmfcSub()).isEqualTo(UPDATED_NMFC_SUB);
        assertThat(testFreightUnit.getNumberOfUnits()).isEqualTo(UPDATED_NUMBER_OF_UNITS);
        assertThat(testFreightUnit.getRatedClass()).isEqualTo(UPDATED_RATED_CLASS);
        assertThat(testFreightUnit.getWeight()).isEqualTo(UPDATED_WEIGHT);
    }

    @Test
    @Transactional
    public void updateNonExistingFreightUnit() throws Exception {
        int databaseSizeBeforeUpdate = freightUnitRepository.findAll().size();

        // Create the FreightUnit

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restFreightUnitMockMvc.perform(put("/api/freight-units")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(freightUnit)))
            .andExpect(status().isCreated());

        // Validate the FreightUnit in the database
        List<FreightUnit> freightUnitList = freightUnitRepository.findAll();
        assertThat(freightUnitList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteFreightUnit() throws Exception {
        // Initialize the database
        freightUnitService.save(freightUnit);

        int databaseSizeBeforeDelete = freightUnitRepository.findAll().size();

        // Get the freightUnit
        restFreightUnitMockMvc.perform(delete("/api/freight-units/{id}", freightUnit.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<FreightUnit> freightUnitList = freightUnitRepository.findAll();
        assertThat(freightUnitList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FreightUnit.class);
        FreightUnit freightUnit1 = new FreightUnit();
        freightUnit1.setId(1L);
        FreightUnit freightUnit2 = new FreightUnit();
        freightUnit2.setId(freightUnit1.getId());
        assertThat(freightUnit1).isEqualTo(freightUnit2);
        freightUnit2.setId(2L);
        assertThat(freightUnit1).isNotEqualTo(freightUnit2);
        freightUnit1.setId(null);
        assertThat(freightUnit1).isNotEqualTo(freightUnit2);
    }
}
