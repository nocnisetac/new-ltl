package com.truesoft.ltl.web.rest;

import com.truesoft.ltl.NewLtlApp;

import com.truesoft.ltl.domain.Accessorials;
import com.truesoft.ltl.repository.AccessorialsRepository;
import com.truesoft.ltl.service.AccessorialsService;
import com.truesoft.ltl.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AccessorialsResource REST controller.
 *
 * @see AccessorialsResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewLtlApp.class)
public class AccessorialsResourceIntTest {

    private static final Boolean DEFAULT_ARRIVAL_NOTIFICATION = false;
    private static final Boolean UPDATED_ARRIVAL_NOTIFICATION = true;

    private static final Boolean DEFAULT_APPOINTMENT = false;
    private static final Boolean UPDATED_APPOINTMENT = true;

    private static final Boolean DEFAULT_LIFTGATE_PICKUP = false;
    private static final Boolean UPDATED_LIFTGATE_PICKUP = true;

    private static final Boolean DEFAULT_LIFTGATE_DELIVERY = false;
    private static final Boolean UPDATED_LIFTGATE_DELIVERY = true;

    private static final Boolean DEFAULT_RESIDENTIAL_NONCOMMERCIAL_PICKUP = false;
    private static final Boolean UPDATED_RESIDENTIAL_NONCOMMERCIAL_PICKUP = true;

    private static final Boolean DEFAULT_RESIDENTIAL_NONCOMMERCIAL_DELIVERY = false;
    private static final Boolean UPDATED_RESIDENTIAL_NONCOMMERCIAL_DELIVERY = true;

    private static final Boolean DEFAULT_INSIDE_PICKUP = false;
    private static final Boolean UPDATED_INSIDE_PICKUP = true;

    private static final Boolean DEFAULT_INSIDE_DELIVERY = false;
    private static final Boolean UPDATED_INSIDE_DELIVERY = true;

    private static final Boolean DEFAULT_TRADESHOW_PICKUP = false;
    private static final Boolean UPDATED_TRADESHOW_PICKUP = true;

    private static final Boolean DEFAULT_TRADESHOW_DELIVERY = false;
    private static final Boolean UPDATED_TRADESHOW_DELIVERY = true;

    private static final Boolean DEFAULT_AIRPORT_PICKUP = false;
    private static final Boolean UPDATED_AIRPORT_PICKUP = true;

    private static final Boolean DEFAULT_AIRPORT_DELIVERY = false;
    private static final Boolean UPDATED_AIRPORT_DELIVERY = true;

    private static final Boolean DEFAULT_SCHOOLS_COLLEGES_CHURCHES_PICKUP = false;
    private static final Boolean UPDATED_SCHOOLS_COLLEGES_CHURCHES_PICKUP = true;

    private static final Boolean DEFAULT_SCHOOLS_COLLEGES_CHURCHES_DELIVERY = false;
    private static final Boolean UPDATED_SCHOOLS_COLLEGES_CHURCHES_DELIVERY = true;

    private static final Boolean DEFAULT_CONSTRUCTION_SITE_PICKUP = false;
    private static final Boolean UPDATED_CONSTRUCTION_SITE_PICKUP = true;

    private static final Boolean DEFAULT_CONSTRUCTION_SITE_DELIVERY = false;
    private static final Boolean UPDATED_CONSTRUCTION_SITE_DELIVERY = true;

    private static final Boolean DEFAULT_SECURED_LIMITED_ACCESS_PICKUP = false;
    private static final Boolean UPDATED_SECURED_LIMITED_ACCESS_PICKUP = true;

    private static final Boolean DEFAULT_SECURED_LIMITED_ACCESS_DELIVERY = false;
    private static final Boolean UPDATED_SECURED_LIMITED_ACCESS_DELIVERY = true;

    private static final Boolean DEFAULT_OVERLENGTH_12_TO_20 = false;
    private static final Boolean UPDATED_OVERLENGTH_12_TO_20 = true;

    private static final Boolean DEFAULT_OVERLENGTH_20_TO_28 = false;
    private static final Boolean UPDATED_OVERLENGTH_20_TO_28 = true;

    private static final Boolean DEFAULT_MINES_OR_SKI_RESORTS_DELIVERY = false;
    private static final Boolean UPDATED_MINES_OR_SKI_RESORTS_DELIVERY = true;

    private static final Boolean DEFAULT_SELF_STORAGE_DELIVERY = false;
    private static final Boolean UPDATED_SELF_STORAGE_DELIVERY = true;

    private static final Boolean DEFAULT_PROTECT_FROM_FREEZING = false;
    private static final Boolean UPDATED_PROTECT_FROM_FREEZING = true;

    private static final Boolean DEFAULT_HAZARDOUS_MATERIALS = false;
    private static final Boolean UPDATED_HAZARDOUS_MATERIALS = true;

    private static final Boolean DEFAULT_NOTIFICATION_PRIOR_TO_DELIVERY = false;
    private static final Boolean UPDATED_NOTIFICATION_PRIOR_TO_DELIVERY = true;

    private static final Boolean DEFAULT_NOTIFICATION_PRIOR_TO_PICKUP = false;
    private static final Boolean UPDATED_NOTIFICATION_PRIOR_TO_PICKUP = true;

    private static final BigDecimal DEFAULT_ADD_L_CARGO_LIABILITY = new BigDecimal(1);
    private static final BigDecimal UPDATED_ADD_L_CARGO_LIABILITY = new BigDecimal(2);

    private static final BigDecimal DEFAULT_COLLECT_ON_DELIVERY = new BigDecimal(1);
    private static final BigDecimal UPDATED_COLLECT_ON_DELIVERY = new BigDecimal(2);

    private static final String DEFAULT_SPECIAL_INSTRUCTIONS = "AAAAAAAAAA";
    private static final String UPDATED_SPECIAL_INSTRUCTIONS = "BBBBBBBBBB";

    @Autowired
    private AccessorialsRepository accessorialsRepository;

    @Autowired
    private AccessorialsService accessorialsService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAccessorialsMockMvc;

    private Accessorials accessorials;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        AccessorialsResource accessorialsResource = new AccessorialsResource(accessorialsService);
        this.restAccessorialsMockMvc = MockMvcBuilders.standaloneSetup(accessorialsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Accessorials createEntity(EntityManager em) {
        Accessorials accessorials = new Accessorials()
            .arrivalNotification(DEFAULT_ARRIVAL_NOTIFICATION)
            .appointment(DEFAULT_APPOINTMENT)
            .liftgatePickup(DEFAULT_LIFTGATE_PICKUP)
            .liftgateDelivery(DEFAULT_LIFTGATE_DELIVERY)
            .residentialNoncommercialPickup(DEFAULT_RESIDENTIAL_NONCOMMERCIAL_PICKUP)
            .residentialNoncommercialDelivery(DEFAULT_RESIDENTIAL_NONCOMMERCIAL_DELIVERY)
            .insidePickup(DEFAULT_INSIDE_PICKUP)
            .insideDelivery(DEFAULT_INSIDE_DELIVERY)
            .tradeshowPickup(DEFAULT_TRADESHOW_PICKUP)
            .tradeshowDelivery(DEFAULT_TRADESHOW_DELIVERY)
            .airportPickup(DEFAULT_AIRPORT_PICKUP)
            .airportDelivery(DEFAULT_AIRPORT_DELIVERY)
            .schoolsCollegesChurchesPickup(DEFAULT_SCHOOLS_COLLEGES_CHURCHES_PICKUP)
            .schoolsCollegesChurchesDelivery(DEFAULT_SCHOOLS_COLLEGES_CHURCHES_DELIVERY)
            .constructionSitePickup(DEFAULT_CONSTRUCTION_SITE_PICKUP)
            .constructionSiteDelivery(DEFAULT_CONSTRUCTION_SITE_DELIVERY)
            .securedLimitedAccessPickup(DEFAULT_SECURED_LIMITED_ACCESS_PICKUP)
            .securedLimitedAccessDelivery(DEFAULT_SECURED_LIMITED_ACCESS_DELIVERY)
            .overlength12to20(DEFAULT_OVERLENGTH_12_TO_20)
            .overlength20to28(DEFAULT_OVERLENGTH_20_TO_28)
            .minesOrSkiResortsDelivery(DEFAULT_MINES_OR_SKI_RESORTS_DELIVERY)
            .selfStorageDelivery(DEFAULT_SELF_STORAGE_DELIVERY)
            .protectFromFreezing(DEFAULT_PROTECT_FROM_FREEZING)
            .hazardousMaterials(DEFAULT_HAZARDOUS_MATERIALS)
            .notificationPriorToDelivery(DEFAULT_NOTIFICATION_PRIOR_TO_DELIVERY)
            .notificationPriorToPickup(DEFAULT_NOTIFICATION_PRIOR_TO_PICKUP)
            .addLCargoLiability(DEFAULT_ADD_L_CARGO_LIABILITY)
            .collectOnDelivery(DEFAULT_COLLECT_ON_DELIVERY)
            .specialInstructions(DEFAULT_SPECIAL_INSTRUCTIONS);
        return accessorials;
    }

    @Before
    public void initTest() {
        accessorials = createEntity(em);
    }

    @Test
    @Transactional
    public void createAccessorials() throws Exception {
        int databaseSizeBeforeCreate = accessorialsRepository.findAll().size();

        // Create the Accessorials
        restAccessorialsMockMvc.perform(post("/api/accessorials")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(accessorials)))
            .andExpect(status().isCreated());

        // Validate the Accessorials in the database
        List<Accessorials> accessorialsList = accessorialsRepository.findAll();
        assertThat(accessorialsList).hasSize(databaseSizeBeforeCreate + 1);
        Accessorials testAccessorials = accessorialsList.get(accessorialsList.size() - 1);
        assertThat(testAccessorials.isArrivalNotification()).isEqualTo(DEFAULT_ARRIVAL_NOTIFICATION);
        assertThat(testAccessorials.isAppointment()).isEqualTo(DEFAULT_APPOINTMENT);
        assertThat(testAccessorials.isLiftgatePickup()).isEqualTo(DEFAULT_LIFTGATE_PICKUP);
        assertThat(testAccessorials.isLiftgateDelivery()).isEqualTo(DEFAULT_LIFTGATE_DELIVERY);
        assertThat(testAccessorials.isResidentialNoncommercialPickup()).isEqualTo(DEFAULT_RESIDENTIAL_NONCOMMERCIAL_PICKUP);
        assertThat(testAccessorials.isResidentialNoncommercialDelivery()).isEqualTo(DEFAULT_RESIDENTIAL_NONCOMMERCIAL_DELIVERY);
        assertThat(testAccessorials.isInsidePickup()).isEqualTo(DEFAULT_INSIDE_PICKUP);
        assertThat(testAccessorials.isInsideDelivery()).isEqualTo(DEFAULT_INSIDE_DELIVERY);
        assertThat(testAccessorials.isTradeshowPickup()).isEqualTo(DEFAULT_TRADESHOW_PICKUP);
        assertThat(testAccessorials.isTradeshowDelivery()).isEqualTo(DEFAULT_TRADESHOW_DELIVERY);
        assertThat(testAccessorials.isAirportPickup()).isEqualTo(DEFAULT_AIRPORT_PICKUP);
        assertThat(testAccessorials.isAirportDelivery()).isEqualTo(DEFAULT_AIRPORT_DELIVERY);
        assertThat(testAccessorials.isSchoolsCollegesChurchesPickup()).isEqualTo(DEFAULT_SCHOOLS_COLLEGES_CHURCHES_PICKUP);
        assertThat(testAccessorials.isSchoolsCollegesChurchesDelivery()).isEqualTo(DEFAULT_SCHOOLS_COLLEGES_CHURCHES_DELIVERY);
        assertThat(testAccessorials.isConstructionSitePickup()).isEqualTo(DEFAULT_CONSTRUCTION_SITE_PICKUP);
        assertThat(testAccessorials.isConstructionSiteDelivery()).isEqualTo(DEFAULT_CONSTRUCTION_SITE_DELIVERY);
        assertThat(testAccessorials.isSecuredLimitedAccessPickup()).isEqualTo(DEFAULT_SECURED_LIMITED_ACCESS_PICKUP);
        assertThat(testAccessorials.isSecuredLimitedAccessDelivery()).isEqualTo(DEFAULT_SECURED_LIMITED_ACCESS_DELIVERY);
        assertThat(testAccessorials.isOverlength12to20()).isEqualTo(DEFAULT_OVERLENGTH_12_TO_20);
        assertThat(testAccessorials.isOverlength20to28()).isEqualTo(DEFAULT_OVERLENGTH_20_TO_28);
        assertThat(testAccessorials.isMinesOrSkiResortsDelivery()).isEqualTo(DEFAULT_MINES_OR_SKI_RESORTS_DELIVERY);
        assertThat(testAccessorials.isSelfStorageDelivery()).isEqualTo(DEFAULT_SELF_STORAGE_DELIVERY);
        assertThat(testAccessorials.isProtectFromFreezing()).isEqualTo(DEFAULT_PROTECT_FROM_FREEZING);
        assertThat(testAccessorials.isHazardousMaterials()).isEqualTo(DEFAULT_HAZARDOUS_MATERIALS);
        assertThat(testAccessorials.isNotificationPriorToDelivery()).isEqualTo(DEFAULT_NOTIFICATION_PRIOR_TO_DELIVERY);
        assertThat(testAccessorials.isNotificationPriorToPickup()).isEqualTo(DEFAULT_NOTIFICATION_PRIOR_TO_PICKUP);
        assertThat(testAccessorials.getAddLCargoLiability()).isEqualTo(DEFAULT_ADD_L_CARGO_LIABILITY);
        assertThat(testAccessorials.getCollectOnDelivery()).isEqualTo(DEFAULT_COLLECT_ON_DELIVERY);
        assertThat(testAccessorials.getSpecialInstructions()).isEqualTo(DEFAULT_SPECIAL_INSTRUCTIONS);
    }

    @Test
    @Transactional
    public void createAccessorialsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = accessorialsRepository.findAll().size();

        // Create the Accessorials with an existing ID
        accessorials.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAccessorialsMockMvc.perform(post("/api/accessorials")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(accessorials)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Accessorials> accessorialsList = accessorialsRepository.findAll();
        assertThat(accessorialsList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllAccessorials() throws Exception {
        // Initialize the database
        accessorialsRepository.saveAndFlush(accessorials);

        // Get all the accessorialsList
        restAccessorialsMockMvc.perform(get("/api/accessorials?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(accessorials.getId().intValue())))
            .andExpect(jsonPath("$.[*].arrivalNotification").value(hasItem(DEFAULT_ARRIVAL_NOTIFICATION.booleanValue())))
            .andExpect(jsonPath("$.[*].appointment").value(hasItem(DEFAULT_APPOINTMENT.booleanValue())))
            .andExpect(jsonPath("$.[*].liftgatePickup").value(hasItem(DEFAULT_LIFTGATE_PICKUP.booleanValue())))
            .andExpect(jsonPath("$.[*].liftgateDelivery").value(hasItem(DEFAULT_LIFTGATE_DELIVERY.booleanValue())))
            .andExpect(jsonPath("$.[*].residentialNoncommercialPickup").value(hasItem(DEFAULT_RESIDENTIAL_NONCOMMERCIAL_PICKUP.booleanValue())))
            .andExpect(jsonPath("$.[*].residentialNoncommercialDelivery").value(hasItem(DEFAULT_RESIDENTIAL_NONCOMMERCIAL_DELIVERY.booleanValue())))
            .andExpect(jsonPath("$.[*].insidePickup").value(hasItem(DEFAULT_INSIDE_PICKUP.booleanValue())))
            .andExpect(jsonPath("$.[*].insideDelivery").value(hasItem(DEFAULT_INSIDE_DELIVERY.booleanValue())))
            .andExpect(jsonPath("$.[*].tradeshowPickup").value(hasItem(DEFAULT_TRADESHOW_PICKUP.booleanValue())))
            .andExpect(jsonPath("$.[*].tradeshowDelivery").value(hasItem(DEFAULT_TRADESHOW_DELIVERY.booleanValue())))
            .andExpect(jsonPath("$.[*].airportPickup").value(hasItem(DEFAULT_AIRPORT_PICKUP.booleanValue())))
            .andExpect(jsonPath("$.[*].airportDelivery").value(hasItem(DEFAULT_AIRPORT_DELIVERY.booleanValue())))
            .andExpect(jsonPath("$.[*].schoolsCollegesChurchesPickup").value(hasItem(DEFAULT_SCHOOLS_COLLEGES_CHURCHES_PICKUP.booleanValue())))
            .andExpect(jsonPath("$.[*].schoolsCollegesChurchesDelivery").value(hasItem(DEFAULT_SCHOOLS_COLLEGES_CHURCHES_DELIVERY.booleanValue())))
            .andExpect(jsonPath("$.[*].constructionSitePickup").value(hasItem(DEFAULT_CONSTRUCTION_SITE_PICKUP.booleanValue())))
            .andExpect(jsonPath("$.[*].constructionSiteDelivery").value(hasItem(DEFAULT_CONSTRUCTION_SITE_DELIVERY.booleanValue())))
            .andExpect(jsonPath("$.[*].securedLimitedAccessPickup").value(hasItem(DEFAULT_SECURED_LIMITED_ACCESS_PICKUP.booleanValue())))
            .andExpect(jsonPath("$.[*].securedLimitedAccessDelivery").value(hasItem(DEFAULT_SECURED_LIMITED_ACCESS_DELIVERY.booleanValue())))
            .andExpect(jsonPath("$.[*].overlength12to20").value(hasItem(DEFAULT_OVERLENGTH_12_TO_20.booleanValue())))
            .andExpect(jsonPath("$.[*].overlength20to28").value(hasItem(DEFAULT_OVERLENGTH_20_TO_28.booleanValue())))
            .andExpect(jsonPath("$.[*].minesOrSkiResortsDelivery").value(hasItem(DEFAULT_MINES_OR_SKI_RESORTS_DELIVERY.booleanValue())))
            .andExpect(jsonPath("$.[*].selfStorageDelivery").value(hasItem(DEFAULT_SELF_STORAGE_DELIVERY.booleanValue())))
            .andExpect(jsonPath("$.[*].protectFromFreezing").value(hasItem(DEFAULT_PROTECT_FROM_FREEZING.booleanValue())))
            .andExpect(jsonPath("$.[*].hazardousMaterials").value(hasItem(DEFAULT_HAZARDOUS_MATERIALS.booleanValue())))
            .andExpect(jsonPath("$.[*].notificationPriorToDelivery").value(hasItem(DEFAULT_NOTIFICATION_PRIOR_TO_DELIVERY.booleanValue())))
            .andExpect(jsonPath("$.[*].notificationPriorToPickup").value(hasItem(DEFAULT_NOTIFICATION_PRIOR_TO_PICKUP.booleanValue())))
            .andExpect(jsonPath("$.[*].addLCargoLiability").value(hasItem(DEFAULT_ADD_L_CARGO_LIABILITY.intValue())))
            .andExpect(jsonPath("$.[*].collectOnDelivery").value(hasItem(DEFAULT_COLLECT_ON_DELIVERY.intValue())))
            .andExpect(jsonPath("$.[*].specialInstructions").value(hasItem(DEFAULT_SPECIAL_INSTRUCTIONS.toString())));
    }

    @Test
    @Transactional
    public void getAccessorials() throws Exception {
        // Initialize the database
        accessorialsRepository.saveAndFlush(accessorials);

        // Get the accessorials
        restAccessorialsMockMvc.perform(get("/api/accessorials/{id}", accessorials.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(accessorials.getId().intValue()))
            .andExpect(jsonPath("$.arrivalNotification").value(DEFAULT_ARRIVAL_NOTIFICATION.booleanValue()))
            .andExpect(jsonPath("$.appointment").value(DEFAULT_APPOINTMENT.booleanValue()))
            .andExpect(jsonPath("$.liftgatePickup").value(DEFAULT_LIFTGATE_PICKUP.booleanValue()))
            .andExpect(jsonPath("$.liftgateDelivery").value(DEFAULT_LIFTGATE_DELIVERY.booleanValue()))
            .andExpect(jsonPath("$.residentialNoncommercialPickup").value(DEFAULT_RESIDENTIAL_NONCOMMERCIAL_PICKUP.booleanValue()))
            .andExpect(jsonPath("$.residentialNoncommercialDelivery").value(DEFAULT_RESIDENTIAL_NONCOMMERCIAL_DELIVERY.booleanValue()))
            .andExpect(jsonPath("$.insidePickup").value(DEFAULT_INSIDE_PICKUP.booleanValue()))
            .andExpect(jsonPath("$.insideDelivery").value(DEFAULT_INSIDE_DELIVERY.booleanValue()))
            .andExpect(jsonPath("$.tradeshowPickup").value(DEFAULT_TRADESHOW_PICKUP.booleanValue()))
            .andExpect(jsonPath("$.tradeshowDelivery").value(DEFAULT_TRADESHOW_DELIVERY.booleanValue()))
            .andExpect(jsonPath("$.airportPickup").value(DEFAULT_AIRPORT_PICKUP.booleanValue()))
            .andExpect(jsonPath("$.airportDelivery").value(DEFAULT_AIRPORT_DELIVERY.booleanValue()))
            .andExpect(jsonPath("$.schoolsCollegesChurchesPickup").value(DEFAULT_SCHOOLS_COLLEGES_CHURCHES_PICKUP.booleanValue()))
            .andExpect(jsonPath("$.schoolsCollegesChurchesDelivery").value(DEFAULT_SCHOOLS_COLLEGES_CHURCHES_DELIVERY.booleanValue()))
            .andExpect(jsonPath("$.constructionSitePickup").value(DEFAULT_CONSTRUCTION_SITE_PICKUP.booleanValue()))
            .andExpect(jsonPath("$.constructionSiteDelivery").value(DEFAULT_CONSTRUCTION_SITE_DELIVERY.booleanValue()))
            .andExpect(jsonPath("$.securedLimitedAccessPickup").value(DEFAULT_SECURED_LIMITED_ACCESS_PICKUP.booleanValue()))
            .andExpect(jsonPath("$.securedLimitedAccessDelivery").value(DEFAULT_SECURED_LIMITED_ACCESS_DELIVERY.booleanValue()))
            .andExpect(jsonPath("$.overlength12to20").value(DEFAULT_OVERLENGTH_12_TO_20.booleanValue()))
            .andExpect(jsonPath("$.overlength20to28").value(DEFAULT_OVERLENGTH_20_TO_28.booleanValue()))
            .andExpect(jsonPath("$.minesOrSkiResortsDelivery").value(DEFAULT_MINES_OR_SKI_RESORTS_DELIVERY.booleanValue()))
            .andExpect(jsonPath("$.selfStorageDelivery").value(DEFAULT_SELF_STORAGE_DELIVERY.booleanValue()))
            .andExpect(jsonPath("$.protectFromFreezing").value(DEFAULT_PROTECT_FROM_FREEZING.booleanValue()))
            .andExpect(jsonPath("$.hazardousMaterials").value(DEFAULT_HAZARDOUS_MATERIALS.booleanValue()))
            .andExpect(jsonPath("$.notificationPriorToDelivery").value(DEFAULT_NOTIFICATION_PRIOR_TO_DELIVERY.booleanValue()))
            .andExpect(jsonPath("$.notificationPriorToPickup").value(DEFAULT_NOTIFICATION_PRIOR_TO_PICKUP.booleanValue()))
            .andExpect(jsonPath("$.addLCargoLiability").value(DEFAULT_ADD_L_CARGO_LIABILITY.intValue()))
            .andExpect(jsonPath("$.collectOnDelivery").value(DEFAULT_COLLECT_ON_DELIVERY.intValue()))
            .andExpect(jsonPath("$.specialInstructions").value(DEFAULT_SPECIAL_INSTRUCTIONS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAccessorials() throws Exception {
        // Get the accessorials
        restAccessorialsMockMvc.perform(get("/api/accessorials/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAccessorials() throws Exception {
        // Initialize the database
        accessorialsService.save(accessorials);

        int databaseSizeBeforeUpdate = accessorialsRepository.findAll().size();

        // Update the accessorials
        Accessorials updatedAccessorials = accessorialsRepository.findOne(accessorials.getId());
        updatedAccessorials
            .arrivalNotification(UPDATED_ARRIVAL_NOTIFICATION)
            .appointment(UPDATED_APPOINTMENT)
            .liftgatePickup(UPDATED_LIFTGATE_PICKUP)
            .liftgateDelivery(UPDATED_LIFTGATE_DELIVERY)
            .residentialNoncommercialPickup(UPDATED_RESIDENTIAL_NONCOMMERCIAL_PICKUP)
            .residentialNoncommercialDelivery(UPDATED_RESIDENTIAL_NONCOMMERCIAL_DELIVERY)
            .insidePickup(UPDATED_INSIDE_PICKUP)
            .insideDelivery(UPDATED_INSIDE_DELIVERY)
            .tradeshowPickup(UPDATED_TRADESHOW_PICKUP)
            .tradeshowDelivery(UPDATED_TRADESHOW_DELIVERY)
            .airportPickup(UPDATED_AIRPORT_PICKUP)
            .airportDelivery(UPDATED_AIRPORT_DELIVERY)
            .schoolsCollegesChurchesPickup(UPDATED_SCHOOLS_COLLEGES_CHURCHES_PICKUP)
            .schoolsCollegesChurchesDelivery(UPDATED_SCHOOLS_COLLEGES_CHURCHES_DELIVERY)
            .constructionSitePickup(UPDATED_CONSTRUCTION_SITE_PICKUP)
            .constructionSiteDelivery(UPDATED_CONSTRUCTION_SITE_DELIVERY)
            .securedLimitedAccessPickup(UPDATED_SECURED_LIMITED_ACCESS_PICKUP)
            .securedLimitedAccessDelivery(UPDATED_SECURED_LIMITED_ACCESS_DELIVERY)
            .overlength12to20(UPDATED_OVERLENGTH_12_TO_20)
            .overlength20to28(UPDATED_OVERLENGTH_20_TO_28)
            .minesOrSkiResortsDelivery(UPDATED_MINES_OR_SKI_RESORTS_DELIVERY)
            .selfStorageDelivery(UPDATED_SELF_STORAGE_DELIVERY)
            .protectFromFreezing(UPDATED_PROTECT_FROM_FREEZING)
            .hazardousMaterials(UPDATED_HAZARDOUS_MATERIALS)
            .notificationPriorToDelivery(UPDATED_NOTIFICATION_PRIOR_TO_DELIVERY)
            .notificationPriorToPickup(UPDATED_NOTIFICATION_PRIOR_TO_PICKUP)
            .addLCargoLiability(UPDATED_ADD_L_CARGO_LIABILITY)
            .collectOnDelivery(UPDATED_COLLECT_ON_DELIVERY)
            .specialInstructions(UPDATED_SPECIAL_INSTRUCTIONS);

        restAccessorialsMockMvc.perform(put("/api/accessorials")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAccessorials)))
            .andExpect(status().isOk());

        // Validate the Accessorials in the database
        List<Accessorials> accessorialsList = accessorialsRepository.findAll();
        assertThat(accessorialsList).hasSize(databaseSizeBeforeUpdate);
        Accessorials testAccessorials = accessorialsList.get(accessorialsList.size() - 1);
        assertThat(testAccessorials.isArrivalNotification()).isEqualTo(UPDATED_ARRIVAL_NOTIFICATION);
        assertThat(testAccessorials.isAppointment()).isEqualTo(UPDATED_APPOINTMENT);
        assertThat(testAccessorials.isLiftgatePickup()).isEqualTo(UPDATED_LIFTGATE_PICKUP);
        assertThat(testAccessorials.isLiftgateDelivery()).isEqualTo(UPDATED_LIFTGATE_DELIVERY);
        assertThat(testAccessorials.isResidentialNoncommercialPickup()).isEqualTo(UPDATED_RESIDENTIAL_NONCOMMERCIAL_PICKUP);
        assertThat(testAccessorials.isResidentialNoncommercialDelivery()).isEqualTo(UPDATED_RESIDENTIAL_NONCOMMERCIAL_DELIVERY);
        assertThat(testAccessorials.isInsidePickup()).isEqualTo(UPDATED_INSIDE_PICKUP);
        assertThat(testAccessorials.isInsideDelivery()).isEqualTo(UPDATED_INSIDE_DELIVERY);
        assertThat(testAccessorials.isTradeshowPickup()).isEqualTo(UPDATED_TRADESHOW_PICKUP);
        assertThat(testAccessorials.isTradeshowDelivery()).isEqualTo(UPDATED_TRADESHOW_DELIVERY);
        assertThat(testAccessorials.isAirportPickup()).isEqualTo(UPDATED_AIRPORT_PICKUP);
        assertThat(testAccessorials.isAirportDelivery()).isEqualTo(UPDATED_AIRPORT_DELIVERY);
        assertThat(testAccessorials.isSchoolsCollegesChurchesPickup()).isEqualTo(UPDATED_SCHOOLS_COLLEGES_CHURCHES_PICKUP);
        assertThat(testAccessorials.isSchoolsCollegesChurchesDelivery()).isEqualTo(UPDATED_SCHOOLS_COLLEGES_CHURCHES_DELIVERY);
        assertThat(testAccessorials.isConstructionSitePickup()).isEqualTo(UPDATED_CONSTRUCTION_SITE_PICKUP);
        assertThat(testAccessorials.isConstructionSiteDelivery()).isEqualTo(UPDATED_CONSTRUCTION_SITE_DELIVERY);
        assertThat(testAccessorials.isSecuredLimitedAccessPickup()).isEqualTo(UPDATED_SECURED_LIMITED_ACCESS_PICKUP);
        assertThat(testAccessorials.isSecuredLimitedAccessDelivery()).isEqualTo(UPDATED_SECURED_LIMITED_ACCESS_DELIVERY);
        assertThat(testAccessorials.isOverlength12to20()).isEqualTo(UPDATED_OVERLENGTH_12_TO_20);
        assertThat(testAccessorials.isOverlength20to28()).isEqualTo(UPDATED_OVERLENGTH_20_TO_28);
        assertThat(testAccessorials.isMinesOrSkiResortsDelivery()).isEqualTo(UPDATED_MINES_OR_SKI_RESORTS_DELIVERY);
        assertThat(testAccessorials.isSelfStorageDelivery()).isEqualTo(UPDATED_SELF_STORAGE_DELIVERY);
        assertThat(testAccessorials.isProtectFromFreezing()).isEqualTo(UPDATED_PROTECT_FROM_FREEZING);
        assertThat(testAccessorials.isHazardousMaterials()).isEqualTo(UPDATED_HAZARDOUS_MATERIALS);
        assertThat(testAccessorials.isNotificationPriorToDelivery()).isEqualTo(UPDATED_NOTIFICATION_PRIOR_TO_DELIVERY);
        assertThat(testAccessorials.isNotificationPriorToPickup()).isEqualTo(UPDATED_NOTIFICATION_PRIOR_TO_PICKUP);
        assertThat(testAccessorials.getAddLCargoLiability()).isEqualTo(UPDATED_ADD_L_CARGO_LIABILITY);
        assertThat(testAccessorials.getCollectOnDelivery()).isEqualTo(UPDATED_COLLECT_ON_DELIVERY);
        assertThat(testAccessorials.getSpecialInstructions()).isEqualTo(UPDATED_SPECIAL_INSTRUCTIONS);
    }

    @Test
    @Transactional
    public void updateNonExistingAccessorials() throws Exception {
        int databaseSizeBeforeUpdate = accessorialsRepository.findAll().size();

        // Create the Accessorials

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAccessorialsMockMvc.perform(put("/api/accessorials")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(accessorials)))
            .andExpect(status().isCreated());

        // Validate the Accessorials in the database
        List<Accessorials> accessorialsList = accessorialsRepository.findAll();
        assertThat(accessorialsList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAccessorials() throws Exception {
        // Initialize the database
        accessorialsService.save(accessorials);

        int databaseSizeBeforeDelete = accessorialsRepository.findAll().size();

        // Get the accessorials
        restAccessorialsMockMvc.perform(delete("/api/accessorials/{id}", accessorials.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Accessorials> accessorialsList = accessorialsRepository.findAll();
        assertThat(accessorialsList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Accessorials.class);
        Accessorials accessorials1 = new Accessorials();
        accessorials1.setId(1L);
        Accessorials accessorials2 = new Accessorials();
        accessorials2.setId(accessorials1.getId());
        assertThat(accessorials1).isEqualTo(accessorials2);
        accessorials2.setId(2L);
        assertThat(accessorials1).isNotEqualTo(accessorials2);
        accessorials1.setId(null);
        assertThat(accessorials1).isNotEqualTo(accessorials2);
    }
}
