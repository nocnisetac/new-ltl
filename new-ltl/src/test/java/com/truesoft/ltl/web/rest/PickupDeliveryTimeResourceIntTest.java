package com.truesoft.ltl.web.rest;

import com.truesoft.ltl.NewLtlApp;

import com.truesoft.ltl.domain.PickupDeliveryTime;
import com.truesoft.ltl.repository.PickupDeliveryTimeRepository;
import com.truesoft.ltl.service.PickupDeliveryTimeService;
import com.truesoft.ltl.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PickupDeliveryTimeResource REST controller.
 *
 * @see PickupDeliveryTimeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewLtlApp.class)
public class PickupDeliveryTimeResourceIntTest {

    private static final LocalDate DEFAULT_PICKUP_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_PICKUP_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_PICKUP_WINDOW_FROM = "AAAAAAAAAA";
    private static final String UPDATED_PICKUP_WINDOW_FROM = "BBBBBBBBBB";

    private static final String DEFAULT_PICKUP_WINDOW_TO = "AAAAAAAAAA";
    private static final String UPDATED_PICKUP_WINDOW_TO = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DELIVERY_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DELIVERY_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_DELIVERY_WINDOW_FROM = "AAAAAAAAAA";
    private static final String UPDATED_DELIVERY_WINDOW_FROM = "BBBBBBBBBB";

    private static final String DEFAULT_DELIVERY_WINDOW_TO = "AAAAAAAAAA";
    private static final String UPDATED_DELIVERY_WINDOW_TO = "BBBBBBBBBB";

    @Autowired
    private PickupDeliveryTimeRepository pickupDeliveryTimeRepository;

    @Autowired
    private PickupDeliveryTimeService pickupDeliveryTimeService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPickupDeliveryTimeMockMvc;

    private PickupDeliveryTime pickupDeliveryTime;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PickupDeliveryTimeResource pickupDeliveryTimeResource = new PickupDeliveryTimeResource(pickupDeliveryTimeService);
        this.restPickupDeliveryTimeMockMvc = MockMvcBuilders.standaloneSetup(pickupDeliveryTimeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PickupDeliveryTime createEntity(EntityManager em) {
        PickupDeliveryTime pickupDeliveryTime = new PickupDeliveryTime()
            .pickupDate(DEFAULT_PICKUP_DATE)
            .pickupWindowFrom(DEFAULT_PICKUP_WINDOW_FROM)
            .pickupWindowTo(DEFAULT_PICKUP_WINDOW_TO)
            .deliveryDate(DEFAULT_DELIVERY_DATE)
            .deliveryWindowFrom(DEFAULT_DELIVERY_WINDOW_FROM)
            .deliveryWindowTo(DEFAULT_DELIVERY_WINDOW_TO);
        return pickupDeliveryTime;
    }

    @Before
    public void initTest() {
        pickupDeliveryTime = createEntity(em);
    }

    @Test
    @Transactional
    public void createPickupDeliveryTime() throws Exception {
        int databaseSizeBeforeCreate = pickupDeliveryTimeRepository.findAll().size();

        // Create the PickupDeliveryTime
        restPickupDeliveryTimeMockMvc.perform(post("/api/pickup-delivery-times")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pickupDeliveryTime)))
            .andExpect(status().isCreated());

        // Validate the PickupDeliveryTime in the database
        List<PickupDeliveryTime> pickupDeliveryTimeList = pickupDeliveryTimeRepository.findAll();
        assertThat(pickupDeliveryTimeList).hasSize(databaseSizeBeforeCreate + 1);
        PickupDeliveryTime testPickupDeliveryTime = pickupDeliveryTimeList.get(pickupDeliveryTimeList.size() - 1);
        assertThat(testPickupDeliveryTime.getPickupDate()).isEqualTo(DEFAULT_PICKUP_DATE);
        assertThat(testPickupDeliveryTime.getPickupWindowFrom()).isEqualTo(DEFAULT_PICKUP_WINDOW_FROM);
        assertThat(testPickupDeliveryTime.getPickupWindowTo()).isEqualTo(DEFAULT_PICKUP_WINDOW_TO);
        assertThat(testPickupDeliveryTime.getDeliveryDate()).isEqualTo(DEFAULT_DELIVERY_DATE);
        assertThat(testPickupDeliveryTime.getDeliveryWindowFrom()).isEqualTo(DEFAULT_DELIVERY_WINDOW_FROM);
        assertThat(testPickupDeliveryTime.getDeliveryWindowTo()).isEqualTo(DEFAULT_DELIVERY_WINDOW_TO);
    }

    @Test
    @Transactional
    public void createPickupDeliveryTimeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = pickupDeliveryTimeRepository.findAll().size();

        // Create the PickupDeliveryTime with an existing ID
        pickupDeliveryTime.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPickupDeliveryTimeMockMvc.perform(post("/api/pickup-delivery-times")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pickupDeliveryTime)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<PickupDeliveryTime> pickupDeliveryTimeList = pickupDeliveryTimeRepository.findAll();
        assertThat(pickupDeliveryTimeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllPickupDeliveryTimes() throws Exception {
        // Initialize the database
        pickupDeliveryTimeRepository.saveAndFlush(pickupDeliveryTime);

        // Get all the pickupDeliveryTimeList
        restPickupDeliveryTimeMockMvc.perform(get("/api/pickup-delivery-times?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pickupDeliveryTime.getId().intValue())))
            .andExpect(jsonPath("$.[*].pickupDate").value(hasItem(DEFAULT_PICKUP_DATE.toString())))
            .andExpect(jsonPath("$.[*].pickupWindowFrom").value(hasItem(DEFAULT_PICKUP_WINDOW_FROM.toString())))
            .andExpect(jsonPath("$.[*].pickupWindowTo").value(hasItem(DEFAULT_PICKUP_WINDOW_TO.toString())))
            .andExpect(jsonPath("$.[*].deliveryDate").value(hasItem(DEFAULT_DELIVERY_DATE.toString())))
            .andExpect(jsonPath("$.[*].deliveryWindowFrom").value(hasItem(DEFAULT_DELIVERY_WINDOW_FROM.toString())))
            .andExpect(jsonPath("$.[*].deliveryWindowTo").value(hasItem(DEFAULT_DELIVERY_WINDOW_TO.toString())));
    }

    @Test
    @Transactional
    public void getPickupDeliveryTime() throws Exception {
        // Initialize the database
        pickupDeliveryTimeRepository.saveAndFlush(pickupDeliveryTime);

        // Get the pickupDeliveryTime
        restPickupDeliveryTimeMockMvc.perform(get("/api/pickup-delivery-times/{id}", pickupDeliveryTime.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(pickupDeliveryTime.getId().intValue()))
            .andExpect(jsonPath("$.pickupDate").value(DEFAULT_PICKUP_DATE.toString()))
            .andExpect(jsonPath("$.pickupWindowFrom").value(DEFAULT_PICKUP_WINDOW_FROM.toString()))
            .andExpect(jsonPath("$.pickupWindowTo").value(DEFAULT_PICKUP_WINDOW_TO.toString()))
            .andExpect(jsonPath("$.deliveryDate").value(DEFAULT_DELIVERY_DATE.toString()))
            .andExpect(jsonPath("$.deliveryWindowFrom").value(DEFAULT_DELIVERY_WINDOW_FROM.toString()))
            .andExpect(jsonPath("$.deliveryWindowTo").value(DEFAULT_DELIVERY_WINDOW_TO.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPickupDeliveryTime() throws Exception {
        // Get the pickupDeliveryTime
        restPickupDeliveryTimeMockMvc.perform(get("/api/pickup-delivery-times/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePickupDeliveryTime() throws Exception {
        // Initialize the database
        pickupDeliveryTimeService.save(pickupDeliveryTime);

        int databaseSizeBeforeUpdate = pickupDeliveryTimeRepository.findAll().size();

        // Update the pickupDeliveryTime
        PickupDeliveryTime updatedPickupDeliveryTime = pickupDeliveryTimeRepository.findOne(pickupDeliveryTime.getId());
        updatedPickupDeliveryTime
            .pickupDate(UPDATED_PICKUP_DATE)
            .pickupWindowFrom(UPDATED_PICKUP_WINDOW_FROM)
            .pickupWindowTo(UPDATED_PICKUP_WINDOW_TO)
            .deliveryDate(UPDATED_DELIVERY_DATE)
            .deliveryWindowFrom(UPDATED_DELIVERY_WINDOW_FROM)
            .deliveryWindowTo(UPDATED_DELIVERY_WINDOW_TO);

        restPickupDeliveryTimeMockMvc.perform(put("/api/pickup-delivery-times")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPickupDeliveryTime)))
            .andExpect(status().isOk());

        // Validate the PickupDeliveryTime in the database
        List<PickupDeliveryTime> pickupDeliveryTimeList = pickupDeliveryTimeRepository.findAll();
        assertThat(pickupDeliveryTimeList).hasSize(databaseSizeBeforeUpdate);
        PickupDeliveryTime testPickupDeliveryTime = pickupDeliveryTimeList.get(pickupDeliveryTimeList.size() - 1);
        assertThat(testPickupDeliveryTime.getPickupDate()).isEqualTo(UPDATED_PICKUP_DATE);
        assertThat(testPickupDeliveryTime.getPickupWindowFrom()).isEqualTo(UPDATED_PICKUP_WINDOW_FROM);
        assertThat(testPickupDeliveryTime.getPickupWindowTo()).isEqualTo(UPDATED_PICKUP_WINDOW_TO);
        assertThat(testPickupDeliveryTime.getDeliveryDate()).isEqualTo(UPDATED_DELIVERY_DATE);
        assertThat(testPickupDeliveryTime.getDeliveryWindowFrom()).isEqualTo(UPDATED_DELIVERY_WINDOW_FROM);
        assertThat(testPickupDeliveryTime.getDeliveryWindowTo()).isEqualTo(UPDATED_DELIVERY_WINDOW_TO);
    }

    @Test
    @Transactional
    public void updateNonExistingPickupDeliveryTime() throws Exception {
        int databaseSizeBeforeUpdate = pickupDeliveryTimeRepository.findAll().size();

        // Create the PickupDeliveryTime

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPickupDeliveryTimeMockMvc.perform(put("/api/pickup-delivery-times")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pickupDeliveryTime)))
            .andExpect(status().isCreated());

        // Validate the PickupDeliveryTime in the database
        List<PickupDeliveryTime> pickupDeliveryTimeList = pickupDeliveryTimeRepository.findAll();
        assertThat(pickupDeliveryTimeList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePickupDeliveryTime() throws Exception {
        // Initialize the database
        pickupDeliveryTimeService.save(pickupDeliveryTime);

        int databaseSizeBeforeDelete = pickupDeliveryTimeRepository.findAll().size();

        // Get the pickupDeliveryTime
        restPickupDeliveryTimeMockMvc.perform(delete("/api/pickup-delivery-times/{id}", pickupDeliveryTime.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<PickupDeliveryTime> pickupDeliveryTimeList = pickupDeliveryTimeRepository.findAll();
        assertThat(pickupDeliveryTimeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PickupDeliveryTime.class);
        PickupDeliveryTime pickupDeliveryTime1 = new PickupDeliveryTime();
        pickupDeliveryTime1.setId(1L);
        PickupDeliveryTime pickupDeliveryTime2 = new PickupDeliveryTime();
        pickupDeliveryTime2.setId(pickupDeliveryTime1.getId());
        assertThat(pickupDeliveryTime1).isEqualTo(pickupDeliveryTime2);
        pickupDeliveryTime2.setId(2L);
        assertThat(pickupDeliveryTime1).isNotEqualTo(pickupDeliveryTime2);
        pickupDeliveryTime1.setId(null);
        assertThat(pickupDeliveryTime1).isNotEqualTo(pickupDeliveryTime2);
    }
}
