package com.truesoft.ltl.web.rest;

import com.truesoft.ltl.NewLtlApp;

import com.truesoft.ltl.domain.AddressBook;
import com.truesoft.ltl.repository.AddressBookRepository;
import com.truesoft.ltl.service.AddressBookService;
import com.truesoft.ltl.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AddressBookResource REST controller.
 *
 * @see AddressBookResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewLtlApp.class)
public class AddressBookResourceIntTest {

    private static final String DEFAULT_COMPANY_NAME = "AAAAAAAAAA";
    private static final String UPDATED_COMPANY_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_PHONE = "AAAAAAAAAA";
    private static final String UPDATED_PHONE = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_FAX = "AAAAAAAAAA";
    private static final String UPDATED_FAX = "BBBBBBBBBB";

    private static final String DEFAULT_ACCOUNT_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_ACCOUNT_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_TYPE = "BBBBBBBBBB";

    @Autowired
    private AddressBookRepository addressBookRepository;

    @Autowired
    private AddressBookService addressBookService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAddressBookMockMvc;

    private AddressBook addressBook;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        AddressBookResource addressBookResource = new AddressBookResource(addressBookService);
        this.restAddressBookMockMvc = MockMvcBuilders.standaloneSetup(addressBookResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AddressBook createEntity(EntityManager em) {
        AddressBook addressBook = new AddressBook()
            .companyName(DEFAULT_COMPANY_NAME)
            .address(DEFAULT_ADDRESS)
            .phone(DEFAULT_PHONE)
            .email(DEFAULT_EMAIL)
            .fax(DEFAULT_FAX)
            .accountNumber(DEFAULT_ACCOUNT_NUMBER)
            .type(DEFAULT_TYPE);
        return addressBook;
    }

    @Before
    public void initTest() {
        addressBook = createEntity(em);
    }

    @Test
    @Transactional
    public void createAddressBook() throws Exception {
        int databaseSizeBeforeCreate = addressBookRepository.findAll().size();

        // Create the AddressBook
        restAddressBookMockMvc.perform(post("/api/address-books")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(addressBook)))
            .andExpect(status().isCreated());

        // Validate the AddressBook in the database
        List<AddressBook> addressBookList = addressBookRepository.findAll();
        assertThat(addressBookList).hasSize(databaseSizeBeforeCreate + 1);
        AddressBook testAddressBook = addressBookList.get(addressBookList.size() - 1);
        assertThat(testAddressBook.getCompanyName()).isEqualTo(DEFAULT_COMPANY_NAME);
        assertThat(testAddressBook.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testAddressBook.getPhone()).isEqualTo(DEFAULT_PHONE);
        assertThat(testAddressBook.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testAddressBook.getFax()).isEqualTo(DEFAULT_FAX);
        assertThat(testAddressBook.getAccountNumber()).isEqualTo(DEFAULT_ACCOUNT_NUMBER);
        assertThat(testAddressBook.getType()).isEqualTo(DEFAULT_TYPE);
    }

    @Test
    @Transactional
    public void createAddressBookWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = addressBookRepository.findAll().size();

        // Create the AddressBook with an existing ID
        addressBook.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAddressBookMockMvc.perform(post("/api/address-books")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(addressBook)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<AddressBook> addressBookList = addressBookRepository.findAll();
        assertThat(addressBookList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkCompanyNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = addressBookRepository.findAll().size();
        // set the field null
        addressBook.setCompanyName(null);

        // Create the AddressBook, which fails.

        restAddressBookMockMvc.perform(post("/api/address-books")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(addressBook)))
            .andExpect(status().isBadRequest());

        List<AddressBook> addressBookList = addressBookRepository.findAll();
        assertThat(addressBookList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAddressIsRequired() throws Exception {
        int databaseSizeBeforeTest = addressBookRepository.findAll().size();
        // set the field null
        addressBook.setAddress(null);

        // Create the AddressBook, which fails.

        restAddressBookMockMvc.perform(post("/api/address-books")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(addressBook)))
            .andExpect(status().isBadRequest());

        List<AddressBook> addressBookList = addressBookRepository.findAll();
        assertThat(addressBookList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPhoneIsRequired() throws Exception {
        int databaseSizeBeforeTest = addressBookRepository.findAll().size();
        // set the field null
        addressBook.setPhone(null);

        // Create the AddressBook, which fails.

        restAddressBookMockMvc.perform(post("/api/address-books")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(addressBook)))
            .andExpect(status().isBadRequest());

        List<AddressBook> addressBookList = addressBookRepository.findAll();
        assertThat(addressBookList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEmailIsRequired() throws Exception {
        int databaseSizeBeforeTest = addressBookRepository.findAll().size();
        // set the field null
        addressBook.setEmail(null);

        // Create the AddressBook, which fails.

        restAddressBookMockMvc.perform(post("/api/address-books")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(addressBook)))
            .andExpect(status().isBadRequest());

        List<AddressBook> addressBookList = addressBookRepository.findAll();
        assertThat(addressBookList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFaxIsRequired() throws Exception {
        int databaseSizeBeforeTest = addressBookRepository.findAll().size();
        // set the field null
        addressBook.setFax(null);

        // Create the AddressBook, which fails.

        restAddressBookMockMvc.perform(post("/api/address-books")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(addressBook)))
            .andExpect(status().isBadRequest());

        List<AddressBook> addressBookList = addressBookRepository.findAll();
        assertThat(addressBookList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAccountNumberIsRequired() throws Exception {
        int databaseSizeBeforeTest = addressBookRepository.findAll().size();
        // set the field null
        addressBook.setAccountNumber(null);

        // Create the AddressBook, which fails.

        restAddressBookMockMvc.perform(post("/api/address-books")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(addressBook)))
            .andExpect(status().isBadRequest());

        List<AddressBook> addressBookList = addressBookRepository.findAll();
        assertThat(addressBookList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = addressBookRepository.findAll().size();
        // set the field null
        addressBook.setType(null);

        // Create the AddressBook, which fails.

        restAddressBookMockMvc.perform(post("/api/address-books")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(addressBook)))
            .andExpect(status().isBadRequest());

        List<AddressBook> addressBookList = addressBookRepository.findAll();
        assertThat(addressBookList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAddressBooks() throws Exception {
        // Initialize the database
        addressBookRepository.saveAndFlush(addressBook);

        // Get all the addressBookList
        restAddressBookMockMvc.perform(get("/api/address-books?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(addressBook.getId().intValue())))
            .andExpect(jsonPath("$.[*].companyName").value(hasItem(DEFAULT_COMPANY_NAME.toString())))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE.toString())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].fax").value(hasItem(DEFAULT_FAX.toString())))
            .andExpect(jsonPath("$.[*].accountNumber").value(hasItem(DEFAULT_ACCOUNT_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())));
    }

    @Test
    @Transactional
    public void getAddressBook() throws Exception {
        // Initialize the database
        addressBookRepository.saveAndFlush(addressBook);

        // Get the addressBook
        restAddressBookMockMvc.perform(get("/api/address-books/{id}", addressBook.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(addressBook.getId().intValue()))
            .andExpect(jsonPath("$.companyName").value(DEFAULT_COMPANY_NAME.toString()))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS.toString()))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.fax").value(DEFAULT_FAX.toString()))
            .andExpect(jsonPath("$.accountNumber").value(DEFAULT_ACCOUNT_NUMBER.toString()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAddressBook() throws Exception {
        // Get the addressBook
        restAddressBookMockMvc.perform(get("/api/address-books/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAddressBook() throws Exception {
        // Initialize the database
        addressBookService.save(addressBook);

        int databaseSizeBeforeUpdate = addressBookRepository.findAll().size();

        // Update the addressBook
        AddressBook updatedAddressBook = addressBookRepository.findOne(addressBook.getId());
        updatedAddressBook
            .companyName(UPDATED_COMPANY_NAME)
            .address(UPDATED_ADDRESS)
            .phone(UPDATED_PHONE)
            .email(UPDATED_EMAIL)
            .fax(UPDATED_FAX)
            .accountNumber(UPDATED_ACCOUNT_NUMBER)
            .type(UPDATED_TYPE);

        restAddressBookMockMvc.perform(put("/api/address-books")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAddressBook)))
            .andExpect(status().isOk());

        // Validate the AddressBook in the database
        List<AddressBook> addressBookList = addressBookRepository.findAll();
        assertThat(addressBookList).hasSize(databaseSizeBeforeUpdate);
        AddressBook testAddressBook = addressBookList.get(addressBookList.size() - 1);
        assertThat(testAddressBook.getCompanyName()).isEqualTo(UPDATED_COMPANY_NAME);
        assertThat(testAddressBook.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testAddressBook.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testAddressBook.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testAddressBook.getFax()).isEqualTo(UPDATED_FAX);
        assertThat(testAddressBook.getAccountNumber()).isEqualTo(UPDATED_ACCOUNT_NUMBER);
        assertThat(testAddressBook.getType()).isEqualTo(UPDATED_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingAddressBook() throws Exception {
        int databaseSizeBeforeUpdate = addressBookRepository.findAll().size();

        // Create the AddressBook

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAddressBookMockMvc.perform(put("/api/address-books")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(addressBook)))
            .andExpect(status().isCreated());

        // Validate the AddressBook in the database
        List<AddressBook> addressBookList = addressBookRepository.findAll();
        assertThat(addressBookList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAddressBook() throws Exception {
        // Initialize the database
        addressBookService.save(addressBook);

        int databaseSizeBeforeDelete = addressBookRepository.findAll().size();

        // Get the addressBook
        restAddressBookMockMvc.perform(delete("/api/address-books/{id}", addressBook.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<AddressBook> addressBookList = addressBookRepository.findAll();
        assertThat(addressBookList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AddressBook.class);
        AddressBook addressBook1 = new AddressBook();
        addressBook1.setId(1L);
        AddressBook addressBook2 = new AddressBook();
        addressBook2.setId(addressBook1.getId());
        assertThat(addressBook1).isEqualTo(addressBook2);
        addressBook2.setId(2L);
        assertThat(addressBook1).isNotEqualTo(addressBook2);
        addressBook1.setId(null);
        assertThat(addressBook1).isNotEqualTo(addressBook2);
    }
}
