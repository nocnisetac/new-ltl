package com.truesoft.ltl.repository;

import com.truesoft.ltl.domain.FreightUnit;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import java.util.List;

/**
 * Spring Data JPA repository for the FreightUnit entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FreightUnitRepository extends JpaRepository<FreightUnit,Long> {

    @Query("select freight_unit from FreightUnit freight_unit where freight_unit.user.login = ?#{principal.username}")
    List<FreightUnit> findByUserIsCurrentUser();
    
}
