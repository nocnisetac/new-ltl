package com.truesoft.ltl.repository;

import com.truesoft.ltl.domain.ServiceCenter;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ServiceCenter entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ServiceCenterRepository extends JpaRepository<ServiceCenter,Long> {
    
}
