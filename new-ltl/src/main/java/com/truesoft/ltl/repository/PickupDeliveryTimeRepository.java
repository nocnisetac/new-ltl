package com.truesoft.ltl.repository;

import com.truesoft.ltl.domain.PickupDeliveryTime;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import java.util.List;

/**
 * Spring Data JPA repository for the PickupDeliveryTime entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PickupDeliveryTimeRepository extends JpaRepository<PickupDeliveryTime,Long> {

    @Query("select pickup_delivery_time from PickupDeliveryTime pickup_delivery_time where pickup_delivery_time.user.login = ?#{principal.username}")
    List<PickupDeliveryTime> findByUserIsCurrentUser();
    
}
