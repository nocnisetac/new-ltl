package com.truesoft.ltl.repository;

import com.truesoft.ltl.domain.Accessorials;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import java.util.List;

/**
 * Spring Data JPA repository for the Accessorials entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AccessorialsRepository extends JpaRepository<Accessorials,Long> {

    @Query("select accessorials from Accessorials accessorials where accessorials.user.login = ?#{principal.username}")
    List<Accessorials> findByUserIsCurrentUser();
    
}
