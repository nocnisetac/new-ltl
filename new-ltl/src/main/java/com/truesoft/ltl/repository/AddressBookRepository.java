package com.truesoft.ltl.repository;

import com.truesoft.ltl.domain.AddressBook;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import java.util.List;

/**
 * Spring Data JPA repository for the AddressBook entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AddressBookRepository extends JpaRepository<AddressBook,Long> {

    @Query("select address_book from AddressBook address_book where address_book.user.login = ?#{principal.username}")
    List<AddressBook> findByUserIsCurrentUser();
    
}
