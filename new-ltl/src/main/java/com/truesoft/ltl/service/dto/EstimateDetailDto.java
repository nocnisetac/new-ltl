package com.truesoft.ltl.service.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * A EstimateDetailDto.
 */

public class EstimateDetailDto {

    private Long id;
    private BigDecimal discountAmount;
    private BigDecimal discountPercentage;
    private BigDecimal discountedFreightCharge;
    private BigDecimal fuelSurcharge;
    private BigDecimal grossFreightCharge;
    private BigDecimal guaranteedServiceOption;
    private BigDecimal internationalCharge;
    private BigDecimal netFreightCharge;
    private BigDecimal totalAccessorialCharge;
    private Boolean variableAccessorialRate;
    private List<ODFLRateQuoteDto> responses = new ArrayList<>();
    private List<AccessorialChargeDto> accessorialCharges = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public EstimateDetailDto discountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
        return this;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    public BigDecimal getDiscountPercentage() {
        return discountPercentage;
    }

    public EstimateDetailDto discountPercentage(BigDecimal discountPercentage) {
        this.discountPercentage = discountPercentage;
        return this;
    }

    public void setDiscountPercentage(BigDecimal discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public BigDecimal getDiscountedFreightCharge() {
        return discountedFreightCharge;
    }

    public EstimateDetailDto discountedFreightCharge(BigDecimal discountedFreightCharge) {
        this.discountedFreightCharge = discountedFreightCharge;
        return this;
    }

    public void setDiscountedFreightCharge(BigDecimal discountedFreightCharge) {
        this.discountedFreightCharge = discountedFreightCharge;
    }

    public BigDecimal getFuelSurcharge() {
        return fuelSurcharge;
    }

    public EstimateDetailDto fuelSurcharge(BigDecimal fuelSurcharge) {
        this.fuelSurcharge = fuelSurcharge;
        return this;
    }

    public void setFuelSurcharge(BigDecimal fuelSurcharge) {
        this.fuelSurcharge = fuelSurcharge;
    }

    public BigDecimal getGrossFreightCharge() {
        return grossFreightCharge;
    }

    public EstimateDetailDto grossFreightCharge(BigDecimal grossFreightCharge) {
        this.grossFreightCharge = grossFreightCharge;
        return this;
    }

    public void setGrossFreightCharge(BigDecimal grossFreightCharge) {
        this.grossFreightCharge = grossFreightCharge;
    }

    public BigDecimal getGuaranteedServiceOption() {
        return guaranteedServiceOption;
    }

    public EstimateDetailDto guaranteedServiceOption(BigDecimal guaranteedServiceOption) {
        this.guaranteedServiceOption = guaranteedServiceOption;
        return this;
    }

    public void setGuaranteedServiceOption(BigDecimal guaranteedServiceOption) {
        this.guaranteedServiceOption = guaranteedServiceOption;
    }

    public BigDecimal getInternationalCharge() {
        return internationalCharge;
    }

    public EstimateDetailDto internationalCharge(BigDecimal internationalCharge) {
        this.internationalCharge = internationalCharge;
        return this;
    }

    public void setInternationalCharge(BigDecimal internationalCharge) {
        this.internationalCharge = internationalCharge;
    }

    public BigDecimal getNetFreightCharge() {
        return netFreightCharge;
    }

    public EstimateDetailDto netFreightCharge(BigDecimal netFreightCharge) {
        this.netFreightCharge = netFreightCharge;
        return this;
    }

    public void setNetFreightCharge(BigDecimal netFreightCharge) {
        this.netFreightCharge = netFreightCharge;
    }

    public BigDecimal getTotalAccessorialCharge() {
        return totalAccessorialCharge;
    }

    public EstimateDetailDto totalAccessorialCharge(BigDecimal totalAccessorialCharge) {
        this.totalAccessorialCharge = totalAccessorialCharge;
        return this;
    }

    public void setTotalAccessorialCharge(BigDecimal totalAccessorialCharge) {
        this.totalAccessorialCharge = totalAccessorialCharge;
    }

    public Boolean isVariableAccessorialRate() {
        return variableAccessorialRate;
    }

    public EstimateDetailDto variableAccessorialRate(Boolean variableAccessorialRate) {
        this.variableAccessorialRate = variableAccessorialRate;
        return this;
    }

    public void setVariableAccessorialRate(Boolean variableAccessorialRate) {
        this.variableAccessorialRate = variableAccessorialRate;
    }

    public List<ODFLRateQuoteDto> getResponses() {
        return responses;
    }

    public EstimateDetailDto responses(List<ODFLRateQuoteDto> oDFLRateQuoteDtos) {
        this.responses = oDFLRateQuoteDtos;
        return this;
    }

    public EstimateDetailDto addResponse(ODFLRateQuoteDto oDFLRateQuoteDto) {
        this.responses.add(oDFLRateQuoteDto);
        oDFLRateQuoteDto.getRateEstimates().add(this);
        return this;
    }

    public EstimateDetailDto removeResponse(ODFLRateQuoteDto oDFLRateQuoteDto) {
        this.responses.remove(oDFLRateQuoteDto);
        oDFLRateQuoteDto.getRateEstimates().remove(this);
        return this;
    }

    public void setResponses(List<ODFLRateQuoteDto> oDFLRateQuoteDtos) {
        this.responses = oDFLRateQuoteDtos;
    }

    public List<AccessorialChargeDto> getAccessorialCharges() {
        return accessorialCharges;
    }

    public EstimateDetailDto accessorialCharges(List<AccessorialChargeDto> accessorialChargeDtos) {
        this.accessorialCharges = accessorialChargeDtos;
        return this;
    }

    public EstimateDetailDto addAccessorialCharges(AccessorialChargeDto accessorialChargeDto) {
        this.accessorialCharges.add(accessorialChargeDto);
        accessorialChargeDto.getEstimatedDetails().add(this);
        return this;
    }

    public EstimateDetailDto removeAccessorialCharges(AccessorialChargeDto accessorialChargeDto) {
        this.accessorialCharges.remove(accessorialChargeDto);
        accessorialChargeDto.getEstimatedDetails().remove(this);
        return this;
    }

    public void setAccessorialCharges(List<AccessorialChargeDto> accessorialChargeDtos) {
        this.accessorialCharges = accessorialChargeDtos;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        EstimateDetailDto estimateDetailDto = (EstimateDetailDto) o;
        if (estimateDetailDto.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), estimateDetailDto.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EstimateDetailDto{" +
            "id=" + getId() +
            ", discountAmount='" + getDiscountAmount() + "'" +
            ", discountPercentage='" + getDiscountPercentage() + "'" +
            ", discountedFreightCharge='" + getDiscountedFreightCharge() + "'" +
            ", fuelSurcharge='" + getFuelSurcharge() + "'" +
            ", grossFreightCharge='" + getGrossFreightCharge() + "'" +
            ", guaranteedServiceOption='" + getGuaranteedServiceOption() + "'" +
            ", internationalCharge='" + getInternationalCharge() + "'" +
            ", netFreightCharge='" + getNetFreightCharge() + "'" +
            ", totalAccessorialCharge='" + getTotalAccessorialCharge() + "'" +
            ", variableAccessorialRate='" + isVariableAccessorialRate() + "'" +
            "}";
    }
}
