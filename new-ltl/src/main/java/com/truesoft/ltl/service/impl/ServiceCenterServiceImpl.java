package com.truesoft.ltl.service.impl;

import com.truesoft.ltl.service.ServiceCenterService;
import com.truesoft.ltl.domain.ServiceCenter;
import com.truesoft.ltl.repository.ServiceCenterRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing ServiceCenter.
 */
@Service
@Transactional
public class ServiceCenterServiceImpl implements ServiceCenterService{

    private final Logger log = LoggerFactory.getLogger(ServiceCenterServiceImpl.class);

    private final ServiceCenterRepository serviceCenterRepository;

    public ServiceCenterServiceImpl(ServiceCenterRepository serviceCenterRepository) {
        this.serviceCenterRepository = serviceCenterRepository;
    }

    /**
     * Save a serviceCenter.
     *
     * @param serviceCenter the entity to save
     * @return the persisted entity
     */
    @Override
    public ServiceCenter save(ServiceCenter serviceCenter) {
        log.debug("Request to save ServiceCenter : {}", serviceCenter);
        return serviceCenterRepository.save(serviceCenter);
    }

    /**
     *  Get all the serviceCenters.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<ServiceCenter> findAll() {
        log.debug("Request to get all ServiceCenters");
        return serviceCenterRepository.findAll();
    }

    /**
     *  Get one serviceCenter by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public ServiceCenter findOne(Long id) {
        log.debug("Request to get ServiceCenter : {}", id);
        return serviceCenterRepository.findOne(id);
    }

    /**
     *  Delete the  serviceCenter by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ServiceCenter : {}", id);
        serviceCenterRepository.delete(id);
    }
}
