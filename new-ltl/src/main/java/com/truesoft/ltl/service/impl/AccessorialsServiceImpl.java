package com.truesoft.ltl.service.impl;

import com.truesoft.ltl.service.AccessorialsService;
import com.truesoft.ltl.domain.Accessorials;
import com.truesoft.ltl.repository.AccessorialsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing Accessorials.
 */
@Service
@Transactional
public class AccessorialsServiceImpl implements AccessorialsService{

    private final Logger log = LoggerFactory.getLogger(AccessorialsServiceImpl.class);

    private final AccessorialsRepository accessorialsRepository;

    public AccessorialsServiceImpl(AccessorialsRepository accessorialsRepository) {
        this.accessorialsRepository = accessorialsRepository;
    }

    /**
     * Save a accessorials.
     *
     * @param accessorials the entity to save
     * @return the persisted entity
     */
    @Override
    public Accessorials save(Accessorials accessorials) {
        log.debug("Request to save Accessorials : {}", accessorials);
        return accessorialsRepository.save(accessorials);
    }

    /**
     *  Get all the accessorials.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<Accessorials> findAll() {
        log.debug("Request to get all Accessorials");
        return accessorialsRepository.findAll();
    }

    /**
     *  Get one accessorials by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Accessorials findOne(Long id) {
        log.debug("Request to get Accessorials : {}", id);
        return accessorialsRepository.findOne(id);
    }

    /**
     *  Delete the  accessorials by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Accessorials : {}", id);
        accessorialsRepository.delete(id);
    }
}
