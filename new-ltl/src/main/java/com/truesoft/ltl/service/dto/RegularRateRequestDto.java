package com.truesoft.ltl.service.dto;


import com.truesoft.ltl.domain.Accessorials;
import com.truesoft.ltl.domain.AddressBook;
import com.truesoft.ltl.domain.FreightUnit;
import com.truesoft.ltl.domain.PickupDeliveryTime;
import com.truesoft.ltl.domain.User;
import javax.validation.constraints.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * A RegularRateRequestDto.
 */

public class RegularRateRequestDto {

    private Long id;
    private Boolean saveOrigin;
    private Boolean saveDestination;
    private Boolean saveFreightUnits;
    private Boolean savePickupDeliveryTime;
    private Boolean saveAccessorials;
    
    @NotNull
    private User user;

    @NotNull
    private AddressBook origin;

    @NotNull
    private AddressBook destination;

    private Accessorials accessorials;

    private PickupDeliveryTime pickupDeliveryTime;

    public PickupDeliveryTime getPickupDeliveryTime() {
		return pickupDeliveryTime;
	}

	public void setPickupDeliveryTime(PickupDeliveryTime pickupDeliveryTime) {
		this.pickupDeliveryTime = pickupDeliveryTime;
	}

	private List<FreightUnit> freightUnits = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isSaveOrigin() {
        return saveOrigin;
    }

    public RegularRateRequestDto saveOrigin(Boolean saveOrigin) {
        this.saveOrigin = saveOrigin;
        return this;
    }

    public void setSaveOrigin(Boolean saveOrigin) {
        this.saveOrigin = saveOrigin;
    }

    public Boolean isSaveDestination() {
        return saveDestination;
    }

    public RegularRateRequestDto saveDestination(Boolean saveDestination) {
        this.saveDestination = saveDestination;
        return this;
    }

    public void setSaveDestination(Boolean saveDestination) {
        this.saveDestination = saveDestination;
    }

    public Boolean isSaveFreightUnits() {
        return saveFreightUnits;
    }

    public RegularRateRequestDto saveFreightUnits(Boolean saveFreightUnits) {
        this.saveFreightUnits = saveFreightUnits;
        return this;
    }

    public void setSaveFreightUnits(Boolean saveFreightUnits) {
        this.saveFreightUnits = saveFreightUnits;
    }

    public Boolean isSavePickupDeliveryTime() {
        return savePickupDeliveryTime;
    }

    public RegularRateRequestDto savePickupDeliveryTime(Boolean savePickupDeliveryTime) {
        this.savePickupDeliveryTime = savePickupDeliveryTime;
        return this;
    }

    public void setSavePickupDeliveryTime(Boolean savePickupDeliveryTime) {
        this.savePickupDeliveryTime = savePickupDeliveryTime;
    }

    public Boolean isSaveAccessorials() {
        return saveAccessorials;
    }

    public RegularRateRequestDto saveAccessorials(Boolean saveAccessorials) {
        this.saveAccessorials = saveAccessorials;
        return this;
    }

    public void setSaveAccessorials(Boolean saveAccessorials) {
        this.saveAccessorials = saveAccessorials;
    }

    public User getUser() {
        return user;
    }

    public RegularRateRequestDto user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public AddressBook getOrigin() {
        return origin;
    }

    public RegularRateRequestDto origin(AddressBook addressBook) {
        this.origin = addressBook;
        return this;
    }

    public void setOrigin(AddressBook addressBook) {
        this.origin = addressBook;
    }

    public AddressBook getDestination() {
        return destination;
    }

    public RegularRateRequestDto destination(AddressBook addressBook) {
        this.destination = addressBook;
        return this;
    }

    public void setDestination(AddressBook addressBook) {
        this.destination = addressBook;
    }

    public Accessorials getAccessorials() {
        return accessorials;
    }

    public RegularRateRequestDto accessorials(Accessorials accessorials) {
        this.accessorials = accessorials;
        return this;
    }

    public void setAccessorials(Accessorials accessorials) {
        this.accessorials = accessorials;
    }

    public List<FreightUnit> getFreightUnits() {
        return freightUnits;
    }

    public RegularRateRequestDto freightUnits(List<FreightUnit> freightUnits) {
        this.freightUnits = freightUnits;
        return this;
    }

    public RegularRateRequestDto addFreightUnits(FreightUnit freightUnit) {
        this.freightUnits.add(freightUnit);
        return this;
    }

    public RegularRateRequestDto removeFreightUnits(FreightUnit freightUnit) {
        this.freightUnits.remove(freightUnit);
        return this;
    }

    public void setFreightUnits(List<FreightUnit> freightUnits) {
        this.freightUnits = freightUnits;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RegularRateRequestDto regularRateRequestDto = (RegularRateRequestDto) o;
        if (regularRateRequestDto.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), regularRateRequestDto.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

	@Override
	public String toString() {
		return "RegularRateRequestDto [id=" + id + ", saveOrigin=" + saveOrigin + ", saveDestination=" + saveDestination
				+ ", saveFreightUnits=" + saveFreightUnits + ", savePickupDeliveryTime=" + savePickupDeliveryTime
				+ ", saveAccessorials=" + saveAccessorials + ", user=" + user + ", origin=" + origin + ", destination="
				+ destination + ", accessorials=" + accessorials + ", pickupDeliveryTime=" + pickupDeliveryTime
				+ ", freightUnits=" + freightUnits + "]";
	}

    
}
