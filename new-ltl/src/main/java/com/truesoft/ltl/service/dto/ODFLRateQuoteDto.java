package com.truesoft.ltl.service.dto;

import java.util.HashSet;
import java.util.Set;

import com.truesoft.ltl.domain.ServiceCenter;

import java.util.Objects;

/**
 * A ODFLRateQuoteDto.
 */
public class ODFLRateQuoteDto {

    private Long id;
    private String destinationInterlineCarrierName;
    private String destinationInterlineScac;
    private String destinationServiceCenter;
    private String errorMessages;
    private String originatingInterlineCarrierName;
    private String originatingInterlineScac;
    private String originatingServiceCenter;
    private String referenceNumber;
    private Boolean success;
    
    private ServiceCenter serviceCentar;

    private Set<CityDto> destinationCities = new HashSet<>();

    private Set<EstimateDetailDto> rateEstimates = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDestinationInterlineCarrierName() {
        return destinationInterlineCarrierName;
    }

    public ODFLRateQuoteDto destinationInterlineCarrierName(String destinationInterlineCarrierName) {
        this.destinationInterlineCarrierName = destinationInterlineCarrierName;
        return this;
    }

    public void setDestinationInterlineCarrierName(String destinationInterlineCarrierName) {
        this.destinationInterlineCarrierName = destinationInterlineCarrierName;
    }

    public String getDestinationInterlineScac() {
        return destinationInterlineScac;
    }

    public ODFLRateQuoteDto destinationInterlineScac(String destinationInterlineScac) {
        this.destinationInterlineScac = destinationInterlineScac;
        return this;
    }

    public void setDestinationInterlineScac(String destinationInterlineScac) {
        this.destinationInterlineScac = destinationInterlineScac;
    }

    public String getDestinationServiceCenter() {
        return destinationServiceCenter;
    }

    public ODFLRateQuoteDto destinationServiceCenter(String destinationServiceCenter) {
        this.destinationServiceCenter = destinationServiceCenter;
        return this;
    }

    public void setDestinationServiceCenter(String destinationServiceCenter) {
        this.destinationServiceCenter = destinationServiceCenter;
    }

    public String getErrorMessages() {
        return errorMessages;
    }

    public ODFLRateQuoteDto errorMessages(String errorMessages) {
        this.errorMessages = errorMessages;
        return this;
    }

    public void setErrorMessages(String errorMessages) {
        this.errorMessages = errorMessages;
    }

    public String getOriginatingInterlineCarrierName() {
        return originatingInterlineCarrierName;
    }

    public ODFLRateQuoteDto originatingInterlineCarrierName(String originatingInterlineCarrierName) {
        this.originatingInterlineCarrierName = originatingInterlineCarrierName;
        return this;
    }

    public void setOriginatingInterlineCarrierName(String originatingInterlineCarrierName) {
        this.originatingInterlineCarrierName = originatingInterlineCarrierName;
    }

    public String getOriginatingInterlineScac() {
        return originatingInterlineScac;
    }

    public ODFLRateQuoteDto originatingInterlineScac(String originatingInterlineScac) {
        this.originatingInterlineScac = originatingInterlineScac;
        return this;
    }

    public void setOriginatingInterlineScac(String originatingInterlineScac) {
        this.originatingInterlineScac = originatingInterlineScac;
    }

    public String getOriginatingServiceCenter() {
        return originatingServiceCenter;
    }

    public ODFLRateQuoteDto originatingServiceCenter(String originatingServiceCenter) {
        this.originatingServiceCenter = originatingServiceCenter;
        return this;
    }

    public void setOriginatingServiceCenter(String originatingServiceCenter) {
        this.originatingServiceCenter = originatingServiceCenter;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public ODFLRateQuoteDto referenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
        return this;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public Boolean isSuccess() {
        return success;
    }

    public ODFLRateQuoteDto success(Boolean success) {
        this.success = success;
        return this;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public ServiceCenter getServiceCentar() {
        return serviceCentar;
    }

    public ODFLRateQuoteDto serviceCentar(ServiceCenter serviceCenter) {
        this.serviceCentar = serviceCenter;
        return this;
    }

    public void setServiceCentar(ServiceCenter serviceCenter) {
        this.serviceCentar = serviceCenter;
    }

    public Set<CityDto> getDestinationCities() {
        return destinationCities;
    }

    public ODFLRateQuoteDto destinationCities(Set<CityDto> cityDtos) {
        this.destinationCities = cityDtos;
        return this;
    }

    public ODFLRateQuoteDto addDestinationCities(CityDto cityDto) {
        this.destinationCities.add(cityDto);
        cityDto.getResponses().add(this);
        return this;
    }

    public ODFLRateQuoteDto removeDestinationCities(CityDto cityDto) {
        this.destinationCities.remove(cityDto);
        cityDto.getResponses().remove(this);
        return this;
    }

    public void setDestinationCities(Set<CityDto> cityDtos) {
        this.destinationCities = cityDtos;
    }

    public Set<EstimateDetailDto> getRateEstimates() {
        return rateEstimates;
    }

    public ODFLRateQuoteDto rateEstimates(Set<EstimateDetailDto> estimateDetailDtos) {
        this.rateEstimates = estimateDetailDtos;
        return this;
    }

    public ODFLRateQuoteDto addRateEstimate(EstimateDetailDto estimateDetailDto) {
        this.rateEstimates.add(estimateDetailDto);
        estimateDetailDto.getResponses().add(this);
        return this;
    }

    public ODFLRateQuoteDto removeRateEstimate(EstimateDetailDto estimateDetailDto) {
        this.rateEstimates.remove(estimateDetailDto);
        estimateDetailDto.getResponses().remove(this);
        return this;
    }

    public void setRateEstimates(Set<EstimateDetailDto> estimateDetailDtos) {
        this.rateEstimates = estimateDetailDtos;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ODFLRateQuoteDto oDFLRateQuoteDto = (ODFLRateQuoteDto) o;
        if (oDFLRateQuoteDto.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), oDFLRateQuoteDto.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ODFLRateQuoteDto{" +
            "id=" + getId() +
            ", destinationInterlineCarrierName='" + getDestinationInterlineCarrierName() + "'" +
            ", destinationInterlineScac='" + getDestinationInterlineScac() + "'" +
            ", destinationServiceCenter='" + getDestinationServiceCenter() + "'" +
            ", errorMessages='" + getErrorMessages() + "'" +
            ", originatingInterlineCarrierName='" + getOriginatingInterlineCarrierName() + "'" +
            ", originatingInterlineScac='" + getOriginatingInterlineScac() + "'" +
            ", originatingServiceCenter='" + getOriginatingServiceCenter() + "'" +
            ", referenceNumber='" + getReferenceNumber() + "'" +
            ", success='" + isSuccess() + "'" +
            "}";
    }
}
