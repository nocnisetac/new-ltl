package com.truesoft.ltl.service;

import com.truesoft.ltl.domain.PostalCode;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing PostalCode.
 */
public interface PostalCodeService {

    /**
     * Save a postalCode.
     *
     * @param postalCode the entity to save
     * @return the persisted entity
     */
    PostalCode save(PostalCode postalCode);

    /**
     *  Get all the postalCodes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<PostalCode> findAll(Pageable pageable);

    /**
     *  Get the "id" postalCode.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    PostalCode findOne(Long id);

    /**
     *  Delete the "id" postalCode.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
