package com.truesoft.ltl.service;

import com.truesoft.ltl.domain.FreightUnit;
import java.util.List;

/**
 * Service Interface for managing FreightUnit.
 */
public interface FreightUnitService {

    /**
     * Save a freightUnit.
     *
     * @param freightUnit the entity to save
     * @return the persisted entity
     */
    FreightUnit save(FreightUnit freightUnit);

    /**
     *  Get all the freightUnits.
     *
     *  @return the list of entities
     */
    List<FreightUnit> findAll();

    /**
     *  Get the "id" freightUnit.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    FreightUnit findOne(Long id);

    /**
     *  Delete the "id" freightUnit.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
