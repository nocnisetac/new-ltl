package com.truesoft.ltl.service;

import com.truesoft.ltl.domain.AddressBook;
import java.util.List;

/**
 * Service Interface for managing AddressBook.
 */
public interface AddressBookService {

    /**
     * Save a addressBook.
     *
     * @param addressBook the entity to save
     * @return the persisted entity
     */
    AddressBook save(AddressBook addressBook);

    /**
     *  Get all the addressBooks.
     *
     *  @return the list of entities
     */
    List<AddressBook> findAll();

    /**
     *  Get the "id" addressBook.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    AddressBook findOne(Long id);

    /**
     *  Delete the "id" addressBook.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
