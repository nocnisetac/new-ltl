package com.truesoft.ltl.service.impl;

import com.truesoft.ltl.service.AddressBookService;
import com.truesoft.ltl.domain.AddressBook;
import com.truesoft.ltl.repository.AddressBookRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing AddressBook.
 */
@Service
@Transactional
public class AddressBookServiceImpl implements AddressBookService{

    private final Logger log = LoggerFactory.getLogger(AddressBookServiceImpl.class);

    private final AddressBookRepository addressBookRepository;

    public AddressBookServiceImpl(AddressBookRepository addressBookRepository) {
        this.addressBookRepository = addressBookRepository;
    }

    /**
     * Save a addressBook.
     *
     * @param addressBook the entity to save
     * @return the persisted entity
     */
    @Override
    public AddressBook save(AddressBook addressBook) {
        log.debug("Request to save AddressBook : {}", addressBook);
        return addressBookRepository.save(addressBook);
    }

    /**
     *  Get all the addressBooks.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<AddressBook> findAll() {
        log.debug("Request to get all AddressBooks");
        return addressBookRepository.findAll();
    }

    /**
     *  Get one addressBook by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public AddressBook findOne(Long id) {
        log.debug("Request to get AddressBook : {}", id);
        return addressBookRepository.findOne(id);
    }

    /**
     *  Delete the  addressBook by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete AddressBook : {}", id);
        addressBookRepository.delete(id);
    }
}
