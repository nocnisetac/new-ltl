package com.truesoft.ltl.service.dto;

import com.truesoft.ltl.domain.Accessorials;
import com.truesoft.ltl.domain.FreightUnit;
import com.truesoft.ltl.domain.User;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * A ODFLRateRequestDto.
 */

public class ODFLRateRequestDto {

    private Long id;
    private Boolean saveAccessorials;
    private Double codAmount;
    private Double contactPhoneNumber;
    private String cubicUnits;
    private String currencyFormat;
    private ZonedDateTime deliveryDateTime;
    private String destinationCountry;
    private String destinationPostalCode;
    private Double discount;
    private ZonedDateTime dropDateTime;
    private String email;
    private String equipmentType;
    private String firstName;
    private Integer insuranceAmount;
    private String lastName;
    private Integer linearFeet;
    private Integer loosePieces;
    private String mexicoServiceCenter;
    private String movement;
    private Integer numberPallets;
    private Integer numberStackablePallets;
    private String odfl4MePassword;
    private String odfl4MeUser;
    private Long odflCustomerAccount;
    private String originCountry;
    private String originPostalCode;
    private ZonedDateTime pickupDateTime;
    private Boolean requestReferenceNumber;
    private Boolean sendEmailOffers;
    private String shipType;
    private Integer tariff;
    private Integer totalCubicVolume;
    private String weightUnits;
    
    private User user;
    
    private Accessorials accessorials;
    
    private List<FreightUnit> freightUnits = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isSaveAccessorials() {
        return saveAccessorials;
    }

    public ODFLRateRequestDto saveAccessorials(Boolean saveAccessorials) {
        this.saveAccessorials = saveAccessorials;
        return this;
    }

    public void setSaveAccessorials(Boolean saveAccessorials) {
        this.saveAccessorials = saveAccessorials;
    }

    public Double getCodAmount() {
        return codAmount;
    }

    public ODFLRateRequestDto codAmount(Double codAmount) {
        this.codAmount = codAmount;
        return this;
    }

    public void setCodAmount(Double codAmount) {
        this.codAmount = codAmount;
    }

    public Double getContactPhoneNumber() {
        return contactPhoneNumber;
    }

    public ODFLRateRequestDto contactPhoneNumber(Double contactPhoneNumber) {
        this.contactPhoneNumber = contactPhoneNumber;
        return this;
    }

    public void setContactPhoneNumber(Double contactPhoneNumber) {
        this.contactPhoneNumber = contactPhoneNumber;
    }

    public String getCubicUnits() {
        return cubicUnits;
    }

    public ODFLRateRequestDto cubicUnits(String cubicUnits) {
        this.cubicUnits = cubicUnits;
        return this;
    }

    public void setCubicUnits(String cubicUnits) {
        this.cubicUnits = cubicUnits;
    }

    public String getCurrencyFormat() {
        return currencyFormat;
    }

    public ODFLRateRequestDto currencyFormat(String currencyFormat) {
        this.currencyFormat = currencyFormat;
        return this;
    }

    public void setCurrencyFormat(String currencyFormat) {
        this.currencyFormat = currencyFormat;
    }

    public ZonedDateTime getDeliveryDateTime() {
        return deliveryDateTime;
    }

    public ODFLRateRequestDto deliveryDateTime(ZonedDateTime deliveryDateTime) {
        this.deliveryDateTime = deliveryDateTime;
        return this;
    }

    public void setDeliveryDateTime(ZonedDateTime deliveryDateTime) {
        this.deliveryDateTime = deliveryDateTime;
    }

    public String getDestinationCountry() {
        return destinationCountry;
    }

    public ODFLRateRequestDto destinationCountry(String destinationCountry) {
        this.destinationCountry = destinationCountry;
        return this;
    }

    public void setDestinationCountry(String destinationCountry) {
        this.destinationCountry = destinationCountry;
    }

    public String getDestinationPostalCode() {
        return destinationPostalCode;
    }

    public ODFLRateRequestDto destinationPostalCode(String destinationPostalCode) {
        this.destinationPostalCode = destinationPostalCode;
        return this;
    }

    public void setDestinationPostalCode(String destinationPostalCode) {
        this.destinationPostalCode = destinationPostalCode;
    }

    public Double getDiscount() {
        return discount;
    }

    public ODFLRateRequestDto discount(Double discount) {
        this.discount = discount;
        return this;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public ZonedDateTime getDropDateTime() {
        return dropDateTime;
    }

    public ODFLRateRequestDto dropDateTime(ZonedDateTime dropDateTime) {
        this.dropDateTime = dropDateTime;
        return this;
    }

    public void setDropDateTime(ZonedDateTime dropDateTime) {
        this.dropDateTime = dropDateTime;
    }

    public String getEmail() {
        return email;
    }

    public ODFLRateRequestDto email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEquipmentType() {
        return equipmentType;
    }

    public ODFLRateRequestDto equipmentType(String equipmentType) {
        this.equipmentType = equipmentType;
        return this;
    }

    public void setEquipmentType(String equipmentType) {
        this.equipmentType = equipmentType;
    }

    public String getFirstName() {
        return firstName;
    }

    public ODFLRateRequestDto firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Integer getInsuranceAmount() {
        return insuranceAmount;
    }

    public ODFLRateRequestDto insuranceAmount(Integer insuranceAmount) {
        this.insuranceAmount = insuranceAmount;
        return this;
    }

    public void setInsuranceAmount(Integer insuranceAmount) {
        this.insuranceAmount = insuranceAmount;
    }

    public String getLastName() {
        return lastName;
    }

    public ODFLRateRequestDto lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getLinearFeet() {
        return linearFeet;
    }

    public ODFLRateRequestDto linearFeet(Integer linearFeet) {
        this.linearFeet = linearFeet;
        return this;
    }

    public void setLinearFeet(Integer linearFeet) {
        this.linearFeet = linearFeet;
    }

    public Integer getLoosePieces() {
        return loosePieces;
    }

    public ODFLRateRequestDto loosePieces(Integer loosePieces) {
        this.loosePieces = loosePieces;
        return this;
    }

    public void setLoosePieces(Integer loosePieces) {
        this.loosePieces = loosePieces;
    }

    public String getMexicoServiceCenter() {
        return mexicoServiceCenter;
    }

    public ODFLRateRequestDto mexicoServiceCenter(String mexicoServiceCenter) {
        this.mexicoServiceCenter = mexicoServiceCenter;
        return this;
    }

    public void setMexicoServiceCenter(String mexicoServiceCenter) {
        this.mexicoServiceCenter = mexicoServiceCenter;
    }

    public String getMovement() {
        return movement;
    }

    public ODFLRateRequestDto movement(String movement) {
        this.movement = movement;
        return this;
    }

    public void setMovement(String movement) {
        this.movement = movement;
    }

    public Integer getNumberPallets() {
        return numberPallets;
    }

    public ODFLRateRequestDto numberPallets(Integer numberPallets) {
        this.numberPallets = numberPallets;
        return this;
    }

    public void setNumberPallets(Integer numberPallets) {
        this.numberPallets = numberPallets;
    }

    public Integer getNumberStackablePallets() {
        return numberStackablePallets;
    }

    public ODFLRateRequestDto numberStackablePallets(Integer numberStackablePallets) {
        this.numberStackablePallets = numberStackablePallets;
        return this;
    }

    public void setNumberStackablePallets(Integer numberStackablePallets) {
        this.numberStackablePallets = numberStackablePallets;
    }

    public String getOdfl4MePassword() {
        return odfl4MePassword;
    }

    public ODFLRateRequestDto odfl4MePassword(String odfl4MePassword) {
        this.odfl4MePassword = odfl4MePassword;
        return this;
    }

    public void setOdfl4MePassword(String odfl4MePassword) {
        this.odfl4MePassword = odfl4MePassword;
    }

    public String getOdfl4MeUser() {
        return odfl4MeUser;
    }

    public ODFLRateRequestDto odfl4MeUser(String odfl4MeUser) {
        this.odfl4MeUser = odfl4MeUser;
        return this;
    }

    public void setOdfl4MeUser(String odfl4MeUser) {
        this.odfl4MeUser = odfl4MeUser;
    }

    public Long getOdflCustomerAccount() {
        return odflCustomerAccount;
    }

    public ODFLRateRequestDto odflCustomerAccount(Long odflCustomerAccount) {
        this.odflCustomerAccount = odflCustomerAccount;
        return this;
    }

    public void setOdflCustomerAccount(Long odflCustomerAccount) {
        this.odflCustomerAccount = odflCustomerAccount;
    }

    public String getOriginCountry() {
        return originCountry;
    }

    public ODFLRateRequestDto originCountry(String originCountry) {
        this.originCountry = originCountry;
        return this;
    }

    public void setOriginCountry(String originCountry) {
        this.originCountry = originCountry;
    }

    public String getOriginPostalCode() {
        return originPostalCode;
    }

    public ODFLRateRequestDto originPostalCode(String originPostalCode) {
        this.originPostalCode = originPostalCode;
        return this;
    }

    public void setOriginPostalCode(String originPostalCode) {
        this.originPostalCode = originPostalCode;
    }

    public ZonedDateTime getPickupDateTime() {
        return pickupDateTime;
    }

    public ODFLRateRequestDto pickupDateTime(ZonedDateTime pickupDateTime) {
        this.pickupDateTime = pickupDateTime;
        return this;
    }

    public void setPickupDateTime(ZonedDateTime pickupDateTime) {
        this.pickupDateTime = pickupDateTime;
    }

    public Boolean isRequestReferenceNumber() {
        return requestReferenceNumber;
    }

    public ODFLRateRequestDto requestReferenceNumber(Boolean requestReferenceNumber) {
        this.requestReferenceNumber = requestReferenceNumber;
        return this;
    }

    public void setRequestReferenceNumber(Boolean requestReferenceNumber) {
        this.requestReferenceNumber = requestReferenceNumber;
    }

    public Boolean isSendEmailOffers() {
        return sendEmailOffers;
    }

    public ODFLRateRequestDto sendEmailOffers(Boolean sendEmailOffers) {
        this.sendEmailOffers = sendEmailOffers;
        return this;
    }

    public void setSendEmailOffers(Boolean sendEmailOffers) {
        this.sendEmailOffers = sendEmailOffers;
    }

    public String getShipType() {
        return shipType;
    }

    public ODFLRateRequestDto shipType(String shipType) {
        this.shipType = shipType;
        return this;
    }

    public void setShipType(String shipType) {
        this.shipType = shipType;
    }

    public Integer getTariff() {
        return tariff;
    }

    public ODFLRateRequestDto tariff(Integer tariff) {
        this.tariff = tariff;
        return this;
    }

    public void setTariff(Integer tariff) {
        this.tariff = tariff;
    }

    public Integer getTotalCubicVolume() {
        return totalCubicVolume;
    }

    public ODFLRateRequestDto totalCubicVolume(Integer totalCubicVolume) {
        this.totalCubicVolume = totalCubicVolume;
        return this;
    }

    public void setTotalCubicVolume(Integer totalCubicVolume) {
        this.totalCubicVolume = totalCubicVolume;
    }

    public String getWeightUnits() {
        return weightUnits;
    }

    public ODFLRateRequestDto weightUnits(String weightUnits) {
        this.weightUnits = weightUnits;
        return this;
    }

    public void setWeightUnits(String weightUnits) {
        this.weightUnits = weightUnits;
    }

    public User getUser() {
        return user;
    }

    public ODFLRateRequestDto user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Accessorials getAccessorials() {
        return accessorials;
    }

    public ODFLRateRequestDto accessorials(Accessorials accessorials) {
        this.accessorials = accessorials;
        return this;
    }

    public void setAccessorials(Accessorials accessorials) {
        this.accessorials = accessorials;
    }

    public List<FreightUnit> getFreightUnits() {
        return freightUnits;
    }

    public ODFLRateRequestDto freightUnits(List<FreightUnit> freightUnits) {
        this.freightUnits = freightUnits;
        return this;
    }

    public ODFLRateRequestDto addFreightUnits(FreightUnit freightUnit) {
        this.freightUnits.add(freightUnit);
        return this;
    }

    public ODFLRateRequestDto removeFreightUnits(FreightUnit freightUnit) {
        this.freightUnits.remove(freightUnit);
        return this;
    }

    public void setFreightUnits(List<FreightUnit> freightUnits) {
        this.freightUnits = freightUnits;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ODFLRateRequestDto oDFLRateRequestDto = (ODFLRateRequestDto) o;
        if (oDFLRateRequestDto.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), oDFLRateRequestDto.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ODFLRateRequestDto{" +
            "id=" + getId() +
            ", saveAccessorials='" + isSaveAccessorials() + "'" +
            ", codAmount='" + getCodAmount() + "'" +
            ", contactPhoneNumber='" + getContactPhoneNumber() + "'" +
            ", cubicUnits='" + getCubicUnits() + "'" +
            ", currencyFormat='" + getCurrencyFormat() + "'" +
            ", deliveryDateTime='" + getDeliveryDateTime() + "'" +
            ", destinationCountry='" + getDestinationCountry() + "'" +
            ", destinationPostalCode='" + getDestinationPostalCode() + "'" +
            ", discount='" + getDiscount() + "'" +
            ", dropDateTime='" + getDropDateTime() + "'" +
            ", email='" + getEmail() + "'" +
            ", equipmentType='" + getEquipmentType() + "'" +
            ", firstName='" + getFirstName() + "'" +
            ", insuranceAmount='" + getInsuranceAmount() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", linearFeet='" + getLinearFeet() + "'" +
            ", loosePieces='" + getLoosePieces() + "'" +
            ", mexicoServiceCenter='" + getMexicoServiceCenter() + "'" +
            ", movement='" + getMovement() + "'" +
            ", numberPallets='" + getNumberPallets() + "'" +
            ", numberStackablePallets='" + getNumberStackablePallets() + "'" +
            ", odfl4MePassword='" + getOdfl4MePassword() + "'" +
            ", odfl4MeUser='" + getOdfl4MeUser() + "'" +
            ", odflCustomerAccount='" + getOdflCustomerAccount() + "'" +
            ", originCountry='" + getOriginCountry() + "'" +
            ", originPostalCode='" + getOriginPostalCode() + "'" +
            ", pickupDateTime='" + getPickupDateTime() + "'" +
            ", requestReferenceNumber='" + isRequestReferenceNumber() + "'" +
            ", sendEmailOffers='" + isSendEmailOffers() + "'" +
            ", shipType='" + getShipType() + "'" +
            ", tariff='" + getTariff() + "'" +
            ", totalCubicVolume='" + getTotalCubicVolume() + "'" +
            ", weightUnits='" + getWeightUnits() + "'" +
            "}";
    }
}
