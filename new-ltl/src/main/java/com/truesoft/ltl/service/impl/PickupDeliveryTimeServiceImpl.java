package com.truesoft.ltl.service.impl;

import com.truesoft.ltl.service.PickupDeliveryTimeService;
import com.truesoft.ltl.domain.PickupDeliveryTime;
import com.truesoft.ltl.repository.PickupDeliveryTimeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing PickupDeliveryTime.
 */
@Service
@Transactional
public class PickupDeliveryTimeServiceImpl implements PickupDeliveryTimeService{

    private final Logger log = LoggerFactory.getLogger(PickupDeliveryTimeServiceImpl.class);

    private final PickupDeliveryTimeRepository pickupDeliveryTimeRepository;

    public PickupDeliveryTimeServiceImpl(PickupDeliveryTimeRepository pickupDeliveryTimeRepository) {
        this.pickupDeliveryTimeRepository = pickupDeliveryTimeRepository;
    }

    /**
     * Save a pickupDeliveryTime.
     *
     * @param pickupDeliveryTime the entity to save
     * @return the persisted entity
     */
    @Override
    public PickupDeliveryTime save(PickupDeliveryTime pickupDeliveryTime) {
        log.debug("Request to save PickupDeliveryTime : {}", pickupDeliveryTime);
        return pickupDeliveryTimeRepository.save(pickupDeliveryTime);
    }

    /**
     *  Get all the pickupDeliveryTimes.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<PickupDeliveryTime> findAll() {
        log.debug("Request to get all PickupDeliveryTimes");
        return pickupDeliveryTimeRepository.findAll();
    }

    /**
     *  Get one pickupDeliveryTime by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public PickupDeliveryTime findOne(Long id) {
        log.debug("Request to get PickupDeliveryTime : {}", id);
        return pickupDeliveryTimeRepository.findOne(id);
    }

    /**
     *  Delete the  pickupDeliveryTime by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PickupDeliveryTime : {}", id);
        pickupDeliveryTimeRepository.delete(id);
    }
}
