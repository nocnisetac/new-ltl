package com.truesoft.ltl.service;

import com.truesoft.ltl.domain.ServiceCenter;
import java.util.List;

/**
 * Service Interface for managing ServiceCenter.
 */
public interface ServiceCenterService {

    /**
     * Save a serviceCenter.
     *
     * @param serviceCenter the entity to save
     * @return the persisted entity
     */
    ServiceCenter save(ServiceCenter serviceCenter);

    /**
     *  Get all the serviceCenters.
     *
     *  @return the list of entities
     */
    List<ServiceCenter> findAll();

    /**
     *  Get the "id" serviceCenter.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    ServiceCenter findOne(Long id);

    /**
     *  Delete the "id" serviceCenter.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
