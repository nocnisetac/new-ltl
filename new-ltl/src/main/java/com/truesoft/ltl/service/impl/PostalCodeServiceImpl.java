package com.truesoft.ltl.service.impl;

import com.truesoft.ltl.service.PostalCodeService;
import com.truesoft.ltl.domain.PostalCode;
import com.truesoft.ltl.repository.PostalCodeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing PostalCode.
 */
@Service
@Transactional
public class PostalCodeServiceImpl implements PostalCodeService{

    private final Logger log = LoggerFactory.getLogger(PostalCodeServiceImpl.class);

    private final PostalCodeRepository postalCodeRepository;

    public PostalCodeServiceImpl(PostalCodeRepository postalCodeRepository) {
        this.postalCodeRepository = postalCodeRepository;
    }

    /**
     * Save a postalCode.
     *
     * @param postalCode the entity to save
     * @return the persisted entity
     */
    @Override
    public PostalCode save(PostalCode postalCode) {
        log.debug("Request to save PostalCode : {}", postalCode);
        return postalCodeRepository.save(postalCode);
    }

    /**
     *  Get all the postalCodes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PostalCode> findAll(Pageable pageable) {
        log.debug("Request to get all PostalCodes");
        return postalCodeRepository.findAll(pageable);
    }

    /**
     *  Get one postalCode by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public PostalCode findOne(Long id) {
        log.debug("Request to get PostalCode : {}", id);
        return postalCodeRepository.findOne(id);
    }

    /**
     *  Delete the  postalCode by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PostalCode : {}", id);
        postalCodeRepository.delete(id);
    }
}
