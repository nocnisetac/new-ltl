package com.truesoft.ltl.service.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * A CityDto.
 */
public class CityDto {

    private Long id;
    private String name;
    private Integer serviceDays;
    private List<ODFLRateQuoteDto> responses = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public CityDto name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getServiceDays() {
        return serviceDays;
    }

    public CityDto serviceDays(Integer serviceDays) {
        this.serviceDays = serviceDays;
        return this;
    }

    public void setServiceDays(Integer serviceDays) {
        this.serviceDays = serviceDays;
    }

    public List<ODFLRateQuoteDto> getResponses() {
        return responses;
    }

    public CityDto responses(List<ODFLRateQuoteDto> oDFLRateQuoteDtos) {
        this.responses = oDFLRateQuoteDtos;
        return this;
    }

    public CityDto addResponse(ODFLRateQuoteDto oDFLRateQuoteDto) {
        this.responses.add(oDFLRateQuoteDto);
        oDFLRateQuoteDto.getDestinationCities().add(this);
        return this;
    }

    public CityDto removeResponse(ODFLRateQuoteDto oDFLRateQuoteDto) {
        this.responses.remove(oDFLRateQuoteDto);
        oDFLRateQuoteDto.getDestinationCities().remove(this);
        return this;
    }

    public void setResponses(List<ODFLRateQuoteDto> oDFLRateQuoteDtos) {
        this.responses = oDFLRateQuoteDtos;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CityDto cityDto = (CityDto) o;
        if (cityDto.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), cityDto.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CityDto{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", serviceDays='" + getServiceDays() + "'" +
            "}";
    }
}
