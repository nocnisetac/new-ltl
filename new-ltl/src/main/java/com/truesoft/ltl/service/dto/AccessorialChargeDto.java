package com.truesoft.ltl.service.dto;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * A AccessorialChargeDto.
 */

public class AccessorialChargeDto {

    private Long id;
    private BigDecimal amount;
    private String description;
    private List<EstimateDetailDto> estimatedDetails = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public AccessorialChargeDto amount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public AccessorialChargeDto description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<EstimateDetailDto> getEstimatedDetails() {
        return estimatedDetails;
    }

    public AccessorialChargeDto estimatedDetails(List<EstimateDetailDto> estimateDetailDtos) {
        this.estimatedDetails = estimateDetailDtos;
        return this;
    }

    public AccessorialChargeDto addEstimatedDetails(EstimateDetailDto estimateDetailDto) {
        this.estimatedDetails.add(estimateDetailDto);
        estimateDetailDto.getAccessorialCharges().add(this);
        return this;
    }

    public AccessorialChargeDto removeEstimatedDetails(EstimateDetailDto estimateDetailDto) {
        this.estimatedDetails.remove(estimateDetailDto);
        estimateDetailDto.getAccessorialCharges().remove(this);
        return this;
    }

    public void setEstimatedDetails(List<EstimateDetailDto> estimateDetailDtos) {
        this.estimatedDetails = estimateDetailDtos;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AccessorialChargeDto accessorialChargeDto = (AccessorialChargeDto) o;
        if (accessorialChargeDto.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), accessorialChargeDto.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AccessorialChargeDto{" +
            "id=" + getId() +
            ", amount='" + getAmount() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
