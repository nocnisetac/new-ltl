package com.truesoft.ltl.service;

import com.truesoft.ltl.domain.PickupDeliveryTime;
import java.util.List;

/**
 * Service Interface for managing PickupDeliveryTime.
 */
public interface PickupDeliveryTimeService {

    /**
     * Save a pickupDeliveryTime.
     *
     * @param pickupDeliveryTime the entity to save
     * @return the persisted entity
     */
    PickupDeliveryTime save(PickupDeliveryTime pickupDeliveryTime);

    /**
     *  Get all the pickupDeliveryTimes.
     *
     *  @return the list of entities
     */
    List<PickupDeliveryTime> findAll();

    /**
     *  Get the "id" pickupDeliveryTime.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    PickupDeliveryTime findOne(Long id);

    /**
     *  Delete the "id" pickupDeliveryTime.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
