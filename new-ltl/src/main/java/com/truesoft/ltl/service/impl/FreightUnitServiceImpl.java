package com.truesoft.ltl.service.impl;

import com.truesoft.ltl.service.FreightUnitService;
import com.truesoft.ltl.domain.FreightUnit;
import com.truesoft.ltl.repository.FreightUnitRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing FreightUnit.
 */
@Service
@Transactional
public class FreightUnitServiceImpl implements FreightUnitService{

    private final Logger log = LoggerFactory.getLogger(FreightUnitServiceImpl.class);

    private final FreightUnitRepository freightUnitRepository;

    public FreightUnitServiceImpl(FreightUnitRepository freightUnitRepository) {
        this.freightUnitRepository = freightUnitRepository;
    }

    /**
     * Save a freightUnit.
     *
     * @param freightUnit the entity to save
     * @return the persisted entity
     */
    @Override
    public FreightUnit save(FreightUnit freightUnit) {
        log.debug("Request to save FreightUnit : {}", freightUnit);
        return freightUnitRepository.save(freightUnit);
    }

    /**
     *  Get all the freightUnits.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<FreightUnit> findAll() {
        log.debug("Request to get all FreightUnits");
        return freightUnitRepository.findAll();
    }

    /**
     *  Get one freightUnit by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public FreightUnit findOne(Long id) {
        log.debug("Request to get FreightUnit : {}", id);
        return freightUnitRepository.findOne(id);
    }

    /**
     *  Delete the  freightUnit by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete FreightUnit : {}", id);
        freightUnitRepository.delete(id);
    }
}
