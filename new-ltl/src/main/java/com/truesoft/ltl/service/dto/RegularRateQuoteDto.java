package com.truesoft.ltl.service.dto;


import java.math.BigDecimal;
import java.util.Objects;

/**
 * A RegularRateQuoteDto.
 */
public class RegularRateQuoteDto {

    private Long id;
    private String carrier;
    private String shippingService;
    private Integer transit;
    private BigDecimal cost;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCarrier() {
        return carrier;
    }

    public RegularRateQuoteDto carrier(String carrier) {
        this.carrier = carrier;
        return this;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    public String getShippingService() {
        return shippingService;
    }

    public RegularRateQuoteDto shippingService(String shippingService) {
        this.shippingService = shippingService;
        return this;
    }

    public void setShippingService(String shippingService) {
        this.shippingService = shippingService;
    }

    public Integer getTransit() {
        return transit;
    }

    public RegularRateQuoteDto transit(Integer transit) {
        this.transit = transit;
        return this;
    }

    public void setTransit(Integer transit) {
        this.transit = transit;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public RegularRateQuoteDto cost(BigDecimal cost) {
        this.cost = cost;
        return this;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RegularRateQuoteDto regularRateQuoteDto = (RegularRateQuoteDto) o;
        if (regularRateQuoteDto.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), regularRateQuoteDto.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RegularRateQuoteDto{" +
            "id=" + getId() +
            ", carrier='" + getCarrier() + "'" +
            ", shippingService='" + getShippingService() + "'" +
            ", transit='" + getTransit() + "'" +
            ", cost='" + getCost() + "'" +
            "}";
    }
}
