package com.truesoft.ltl.service;

import com.truesoft.ltl.domain.Accessorials;
import java.util.List;

/**
 * Service Interface for managing Accessorials.
 */
public interface AccessorialsService {

    /**
     * Save a accessorials.
     *
     * @param accessorials the entity to save
     * @return the persisted entity
     */
    Accessorials save(Accessorials accessorials);

    /**
     *  Get all the accessorials.
     *
     *  @return the list of entities
     */
    List<Accessorials> findAll();

    /**
     *  Get the "id" accessorials.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Accessorials findOne(Long id);

    /**
     *  Delete the "id" accessorials.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
