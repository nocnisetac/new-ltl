package com.truesoft.ltl.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.truesoft.ltl.domain.ServiceCenter;
import com.truesoft.ltl.service.ServiceCenterService;
import com.truesoft.ltl.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ServiceCenter.
 */
@RestController
@RequestMapping("/api")
public class ServiceCenterResource {

    private final Logger log = LoggerFactory.getLogger(ServiceCenterResource.class);

    private static final String ENTITY_NAME = "serviceCenter";

    private final ServiceCenterService serviceCenterService;

    public ServiceCenterResource(ServiceCenterService serviceCenterService) {
        this.serviceCenterService = serviceCenterService;
    }

    /**
     * POST  /service-centers : Create a new serviceCenter.
     *
     * @param serviceCenter the serviceCenter to create
     * @return the ResponseEntity with status 201 (Created) and with body the new serviceCenter, or with status 400 (Bad Request) if the serviceCenter has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/service-centers")
    @Timed
    public ResponseEntity<ServiceCenter> createServiceCenter(@RequestBody ServiceCenter serviceCenter) throws URISyntaxException {
        log.debug("REST request to save ServiceCenter : {}", serviceCenter);
        if (serviceCenter.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new serviceCenter cannot already have an ID")).body(null);
        }
        ServiceCenter result = serviceCenterService.save(serviceCenter);
        return ResponseEntity.created(new URI("/api/service-centers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /service-centers : Updates an existing serviceCenter.
     *
     * @param serviceCenter the serviceCenter to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated serviceCenter,
     * or with status 400 (Bad Request) if the serviceCenter is not valid,
     * or with status 500 (Internal Server Error) if the serviceCenter couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/service-centers")
    @Timed
    public ResponseEntity<ServiceCenter> updateServiceCenter(@RequestBody ServiceCenter serviceCenter) throws URISyntaxException {
        log.debug("REST request to update ServiceCenter : {}", serviceCenter);
        if (serviceCenter.getId() == null) {
            return createServiceCenter(serviceCenter);
        }
        ServiceCenter result = serviceCenterService.save(serviceCenter);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, serviceCenter.getId().toString()))
            .body(result);
    }

    /**
     * GET  /service-centers : get all the serviceCenters.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of serviceCenters in body
     */
    @GetMapping("/service-centers")
    @Timed
    public List<ServiceCenter> getAllServiceCenters() {
        log.debug("REST request to get all ServiceCenters");
        return serviceCenterService.findAll();
    }

    /**
     * GET  /service-centers/:id : get the "id" serviceCenter.
     *
     * @param id the id of the serviceCenter to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the serviceCenter, or with status 404 (Not Found)
     */
    @GetMapping("/service-centers/{id}")
    @Timed
    public ResponseEntity<ServiceCenter> getServiceCenter(@PathVariable Long id) {
        log.debug("REST request to get ServiceCenter : {}", id);
        ServiceCenter serviceCenter = serviceCenterService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(serviceCenter));
    }

    /**
     * DELETE  /service-centers/:id : delete the "id" serviceCenter.
     *
     * @param id the id of the serviceCenter to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/service-centers/{id}")
    @Timed
    public ResponseEntity<Void> deleteServiceCenter(@PathVariable Long id) {
        log.debug("REST request to delete ServiceCenter : {}", id);
        serviceCenterService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
