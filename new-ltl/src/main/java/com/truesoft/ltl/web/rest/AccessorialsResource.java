package com.truesoft.ltl.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.truesoft.ltl.domain.Accessorials;
import com.truesoft.ltl.service.AccessorialsService;
import com.truesoft.ltl.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Accessorials.
 */
@RestController
@RequestMapping("/api")
public class AccessorialsResource {

    private final Logger log = LoggerFactory.getLogger(AccessorialsResource.class);

    private static final String ENTITY_NAME = "accessorials";

    private final AccessorialsService accessorialsService;

    public AccessorialsResource(AccessorialsService accessorialsService) {
        this.accessorialsService = accessorialsService;
    }

    /**
     * POST  /accessorials : Create a new accessorials.
     *
     * @param accessorials the accessorials to create
     * @return the ResponseEntity with status 201 (Created) and with body the new accessorials, or with status 400 (Bad Request) if the accessorials has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/accessorials")
    @Timed
    public ResponseEntity<Accessorials> createAccessorials(@RequestBody Accessorials accessorials) throws URISyntaxException {
        log.debug("REST request to save Accessorials : {}", accessorials);
        if (accessorials.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new accessorials cannot already have an ID")).body(null);
        }
        Accessorials result = accessorialsService.save(accessorials);
        return ResponseEntity.created(new URI("/api/accessorials/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /accessorials : Updates an existing accessorials.
     *
     * @param accessorials the accessorials to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated accessorials,
     * or with status 400 (Bad Request) if the accessorials is not valid,
     * or with status 500 (Internal Server Error) if the accessorials couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/accessorials")
    @Timed
    public ResponseEntity<Accessorials> updateAccessorials(@RequestBody Accessorials accessorials) throws URISyntaxException {
        log.debug("REST request to update Accessorials : {}", accessorials);
        if (accessorials.getId() == null) {
            return createAccessorials(accessorials);
        }
        Accessorials result = accessorialsService.save(accessorials);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, accessorials.getId().toString()))
            .body(result);
    }

    /**
     * GET  /accessorials : get all the accessorials.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of accessorials in body
     */
    @GetMapping("/accessorials")
    @Timed
    public List<Accessorials> getAllAccessorials() {
        log.debug("REST request to get all Accessorials");
        return accessorialsService.findAll();
    }

    /**
     * GET  /accessorials/:id : get the "id" accessorials.
     *
     * @param id the id of the accessorials to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the accessorials, or with status 404 (Not Found)
     */
    @GetMapping("/accessorials/{id}")
    @Timed
    public ResponseEntity<Accessorials> getAccessorials(@PathVariable Long id) {
        log.debug("REST request to get Accessorials : {}", id);
        Accessorials accessorials = accessorialsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(accessorials));
    }

    /**
     * DELETE  /accessorials/:id : delete the "id" accessorials.
     *
     * @param id the id of the accessorials to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/accessorials/{id}")
    @Timed
    public ResponseEntity<Void> deleteAccessorials(@PathVariable Long id) {
        log.debug("REST request to delete Accessorials : {}", id);
        accessorialsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
