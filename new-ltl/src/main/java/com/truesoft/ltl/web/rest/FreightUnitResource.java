package com.truesoft.ltl.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.truesoft.ltl.domain.FreightUnit;
import com.truesoft.ltl.service.FreightUnitService;
import com.truesoft.ltl.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing FreightUnit.
 */
@RestController
@RequestMapping("/api")
public class FreightUnitResource {

    private final Logger log = LoggerFactory.getLogger(FreightUnitResource.class);

    private static final String ENTITY_NAME = "freightUnit";

    private final FreightUnitService freightUnitService;

    public FreightUnitResource(FreightUnitService freightUnitService) {
        this.freightUnitService = freightUnitService;
    }

    /**
     * POST  /freight-units : Create a new freightUnit.
     *
     * @param freightUnit the freightUnit to create
     * @return the ResponseEntity with status 201 (Created) and with body the new freightUnit, or with status 400 (Bad Request) if the freightUnit has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/freight-units")
    @Timed
    public ResponseEntity<FreightUnit> createFreightUnit(@Valid @RequestBody FreightUnit freightUnit) throws URISyntaxException {
        log.debug("REST request to save FreightUnit : {}", freightUnit);
        if (freightUnit.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new freightUnit cannot already have an ID")).body(null);
        }
        FreightUnit result = freightUnitService.save(freightUnit);
        return ResponseEntity.created(new URI("/api/freight-units/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /freight-units : Updates an existing freightUnit.
     *
     * @param freightUnit the freightUnit to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated freightUnit,
     * or with status 400 (Bad Request) if the freightUnit is not valid,
     * or with status 500 (Internal Server Error) if the freightUnit couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/freight-units")
    @Timed
    public ResponseEntity<FreightUnit> updateFreightUnit(@Valid @RequestBody FreightUnit freightUnit) throws URISyntaxException {
        log.debug("REST request to update FreightUnit : {}", freightUnit);
        if (freightUnit.getId() == null) {
            return createFreightUnit(freightUnit);
        }
        FreightUnit result = freightUnitService.save(freightUnit);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, freightUnit.getId().toString()))
            .body(result);
    }

    /**
     * GET  /freight-units : get all the freightUnits.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of freightUnits in body
     */
    @GetMapping("/freight-units")
    @Timed
    public List<FreightUnit> getAllFreightUnits() {
        log.debug("REST request to get all FreightUnits");
        return freightUnitService.findAll();
    }

    /**
     * GET  /freight-units/:id : get the "id" freightUnit.
     *
     * @param id the id of the freightUnit to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the freightUnit, or with status 404 (Not Found)
     */
    @GetMapping("/freight-units/{id}")
    @Timed
    public ResponseEntity<FreightUnit> getFreightUnit(@PathVariable Long id) {
        log.debug("REST request to get FreightUnit : {}", id);
        FreightUnit freightUnit = freightUnitService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(freightUnit));
    }

    /**
     * DELETE  /freight-units/:id : delete the "id" freightUnit.
     *
     * @param id the id of the freightUnit to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/freight-units/{id}")
    @Timed
    public ResponseEntity<Void> deleteFreightUnit(@PathVariable Long id) {
        log.debug("REST request to delete FreightUnit : {}", id);
        freightUnitService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
