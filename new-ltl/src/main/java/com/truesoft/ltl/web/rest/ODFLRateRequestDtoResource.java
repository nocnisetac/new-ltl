package com.truesoft.ltl.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.truesoft.ltl.partners.service.BasePartnersService;
import com.truesoft.ltl.service.dto.ODFLRateQuoteDto;
import com.truesoft.ltl.service.dto.ODFLRateRequestDto;


/**
 * REST controller for managing RateRequest.
 */
@RestController
@RequestMapping("/api")
public class ODFLRateRequestDtoResource {
	
	private final Logger log = LoggerFactory.getLogger(ODFLRateRequestDtoResource.class);

    private static final String DTO_NAME = "rateRequestDTO";
    
    //@Autowired
    //private RateRequestDTOService rateRequestDTOService;
    @Autowired
    private BasePartnersService basePartnersService;
    
    /**
     * POST  /rate-request-dtos : Rate request data from front-end.
     *
     * @param rateRequestDTO get a list of estimated quotes from all carriers
     * @return the ResponseEntity with status 201 (Created) and with body the list of estimated quotes, or with status 400 (Bad Request) if the rateRequestDTO is incomplete
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/rate-request-dtos")
    public ResponseEntity<List<ODFLRateQuoteDto>> createRateRequestDTO(@Valid @RequestBody ODFLRateRequestDto rateRequestDTO) throws URISyntaxException {
        
    	log.debug("REST request to get : {}", rateRequestDTO);

        //rateRequestDTOService.save(rateRequestDTO);
        // get the list of estimated quotes from service layer
        // return the list
        List<ODFLRateQuoteDto> list = basePartnersService.getAllRateQuotes(rateRequestDTO);
        return new ResponseEntity<>(list, HttpStatus.OK);
    }
    
}
