/**
 * View Models used by Spring MVC REST controllers.
 */
package com.truesoft.ltl.web.rest.vm;
