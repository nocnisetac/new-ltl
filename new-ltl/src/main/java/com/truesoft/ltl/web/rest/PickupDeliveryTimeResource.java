package com.truesoft.ltl.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.truesoft.ltl.domain.PickupDeliveryTime;
import com.truesoft.ltl.service.PickupDeliveryTimeService;
import com.truesoft.ltl.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing PickupDeliveryTime.
 */
@RestController
@RequestMapping("/api")
public class PickupDeliveryTimeResource {

    private final Logger log = LoggerFactory.getLogger(PickupDeliveryTimeResource.class);

    private static final String ENTITY_NAME = "pickupDeliveryTime";

    private final PickupDeliveryTimeService pickupDeliveryTimeService;

    public PickupDeliveryTimeResource(PickupDeliveryTimeService pickupDeliveryTimeService) {
        this.pickupDeliveryTimeService = pickupDeliveryTimeService;
    }

    /**
     * POST  /pickup-delivery-times : Create a new pickupDeliveryTime.
     *
     * @param pickupDeliveryTime the pickupDeliveryTime to create
     * @return the ResponseEntity with status 201 (Created) and with body the new pickupDeliveryTime, or with status 400 (Bad Request) if the pickupDeliveryTime has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/pickup-delivery-times")
    @Timed
    public ResponseEntity<PickupDeliveryTime> createPickupDeliveryTime(@RequestBody PickupDeliveryTime pickupDeliveryTime) throws URISyntaxException {
        log.debug("REST request to save PickupDeliveryTime : {}", pickupDeliveryTime);
        if (pickupDeliveryTime.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new pickupDeliveryTime cannot already have an ID")).body(null);
        }
        PickupDeliveryTime result = pickupDeliveryTimeService.save(pickupDeliveryTime);
        return ResponseEntity.created(new URI("/api/pickup-delivery-times/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /pickup-delivery-times : Updates an existing pickupDeliveryTime.
     *
     * @param pickupDeliveryTime the pickupDeliveryTime to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated pickupDeliveryTime,
     * or with status 400 (Bad Request) if the pickupDeliveryTime is not valid,
     * or with status 500 (Internal Server Error) if the pickupDeliveryTime couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/pickup-delivery-times")
    @Timed
    public ResponseEntity<PickupDeliveryTime> updatePickupDeliveryTime(@RequestBody PickupDeliveryTime pickupDeliveryTime) throws URISyntaxException {
        log.debug("REST request to update PickupDeliveryTime : {}", pickupDeliveryTime);
        if (pickupDeliveryTime.getId() == null) {
            return createPickupDeliveryTime(pickupDeliveryTime);
        }
        PickupDeliveryTime result = pickupDeliveryTimeService.save(pickupDeliveryTime);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, pickupDeliveryTime.getId().toString()))
            .body(result);
    }

    /**
     * GET  /pickup-delivery-times : get all the pickupDeliveryTimes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of pickupDeliveryTimes in body
     */
    @GetMapping("/pickup-delivery-times")
    @Timed
    public List<PickupDeliveryTime> getAllPickupDeliveryTimes() {
        log.debug("REST request to get all PickupDeliveryTimes");
        return pickupDeliveryTimeService.findAll();
    }

    /**
     * GET  /pickup-delivery-times/:id : get the "id" pickupDeliveryTime.
     *
     * @param id the id of the pickupDeliveryTime to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the pickupDeliveryTime, or with status 404 (Not Found)
     */
    @GetMapping("/pickup-delivery-times/{id}")
    @Timed
    public ResponseEntity<PickupDeliveryTime> getPickupDeliveryTime(@PathVariable Long id) {
        log.debug("REST request to get PickupDeliveryTime : {}", id);
        PickupDeliveryTime pickupDeliveryTime = pickupDeliveryTimeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(pickupDeliveryTime));
    }

    /**
     * DELETE  /pickup-delivery-times/:id : delete the "id" pickupDeliveryTime.
     *
     * @param id the id of the pickupDeliveryTime to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/pickup-delivery-times/{id}")
    @Timed
    public ResponseEntity<Void> deletePickupDeliveryTime(@PathVariable Long id) {
        log.debug("REST request to delete PickupDeliveryTime : {}", id);
        pickupDeliveryTimeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
