package com.truesoft.ltl.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.truesoft.ltl.domain.PostalCode;
import com.truesoft.ltl.service.PostalCodeService;
import com.truesoft.ltl.web.rest.util.HeaderUtil;
import com.truesoft.ltl.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing PostalCode.
 */
@RestController
@RequestMapping("/api")
public class PostalCodeResource {

    private final Logger log = LoggerFactory.getLogger(PostalCodeResource.class);

    private static final String ENTITY_NAME = "postalCode";

    private final PostalCodeService postalCodeService;

    public PostalCodeResource(PostalCodeService postalCodeService) {
        this.postalCodeService = postalCodeService;
    }

    /**
     * POST  /postal-codes : Create a new postalCode.
     *
     * @param postalCode the postalCode to create
     * @return the ResponseEntity with status 201 (Created) and with body the new postalCode, or with status 400 (Bad Request) if the postalCode has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/postal-codes")
    @Timed
    public ResponseEntity<PostalCode> createPostalCode(@RequestBody PostalCode postalCode) throws URISyntaxException {
        log.debug("REST request to save PostalCode : {}", postalCode);
        if (postalCode.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new postalCode cannot already have an ID")).body(null);
        }
        PostalCode result = postalCodeService.save(postalCode);
        return ResponseEntity.created(new URI("/api/postal-codes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /postal-codes : Updates an existing postalCode.
     *
     * @param postalCode the postalCode to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated postalCode,
     * or with status 400 (Bad Request) if the postalCode is not valid,
     * or with status 500 (Internal Server Error) if the postalCode couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/postal-codes")
    @Timed
    public ResponseEntity<PostalCode> updatePostalCode(@RequestBody PostalCode postalCode) throws URISyntaxException {
        log.debug("REST request to update PostalCode : {}", postalCode);
        if (postalCode.getId() == null) {
            return createPostalCode(postalCode);
        }
        PostalCode result = postalCodeService.save(postalCode);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, postalCode.getId().toString()))
            .body(result);
    }

    /**
     * GET  /postal-codes : get all the postalCodes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of postalCodes in body
     */
    @GetMapping("/postal-codes")
    @Timed
    public ResponseEntity<List<PostalCode>> getAllPostalCodes(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of PostalCodes");
        Page<PostalCode> page = postalCodeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/postal-codes");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /postal-codes/:id : get the "id" postalCode.
     *
     * @param id the id of the postalCode to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the postalCode, or with status 404 (Not Found)
     */
    @GetMapping("/postal-codes/{id}")
    @Timed
    public ResponseEntity<PostalCode> getPostalCode(@PathVariable Long id) {
        log.debug("REST request to get PostalCode : {}", id);
        PostalCode postalCode = postalCodeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(postalCode));
    }

    /**
     * DELETE  /postal-codes/:id : delete the "id" postalCode.
     *
     * @param id the id of the postalCode to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/postal-codes/{id}")
    @Timed
    public ResponseEntity<Void> deletePostalCode(@PathVariable Long id) {
        log.debug("REST request to delete PostalCode : {}", id);
        postalCodeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
