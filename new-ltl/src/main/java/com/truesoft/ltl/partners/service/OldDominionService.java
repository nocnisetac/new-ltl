package com.truesoft.ltl.partners.service;

import java.util.concurrent.CompletableFuture;

import com.truesoft.ltl.service.dto.ODFLRateQuoteDto;
import com.truesoft.ltl.service.dto.ODFLRateRequestDto;


public interface OldDominionService {
	
	CompletableFuture<ODFLRateQuoteDto> getMyLTLRate (ODFLRateRequestDto request);
	
	//CompletableFuture<RateQuoteDTO> getMyHomeMoveRate (RateRequestDTO request);

}
