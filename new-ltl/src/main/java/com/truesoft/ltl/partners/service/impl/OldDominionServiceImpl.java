package com.truesoft.ltl.partners.service.impl;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.truesoft.ltl.partners.odfl.ws.myrate.Freight;
import com.truesoft.ltl.partners.odfl.ws.myrate.MyHomeMoveRateResponse;
import com.truesoft.ltl.partners.odfl.ws.myrate.MyLTLRateResponse;
import com.truesoft.ltl.partners.odfl.ws.myrate.MyRateRequest;
import com.truesoft.ltl.partners.odfl.ws.myrate.RateDelegate;
import com.truesoft.ltl.partners.odfl.ws.myrate.RateService;
import com.truesoft.ltl.partners.service.OldDominionService;
import com.truesoft.ltl.service.dto.ODFLRateQuoteDto;
import com.truesoft.ltl.service.dto.ODFLRateRequestDto;
import com.truesoft.ltl.domain.FreightUnit;

@Service
public class OldDominionServiceImpl implements OldDominionService{
	
	private final Logger log = LoggerFactory.getLogger(OldDominionServiceImpl.class);
	
	private static final String ODLF_USER = "DOCCKONE";
	private static final String ODLF_PASSWORD = "SHIPPING";
	private static final long ODLF_CUSTOMER_ACCOUNT_NUMBER = 13168044L;
	
	@Autowired
	private RateService rateService;
	
	@Async
	@Override
	public CompletableFuture<ODFLRateQuoteDto> getMyLTLRate(ODFLRateRequestDto rateRequestDTO) {
		ODFLRateQuoteDto rateQuoteDTO = new ODFLRateQuoteDto();
		try {
			RateDelegate rateDelegate = rateService.getRatePort();
			MyRateRequest myRateRequest = createMyRateRequest(rateRequestDTO);
			MyLTLRateResponse myLTLRateResponse = rateDelegate.getLTLRateEstimate(myRateRequest);
			rateQuoteDTO = createMyLtlRateQuote(myLTLRateResponse);
		} catch (Exception e) {
			log.error("Exception in Old Dominion getLTLRateEstimate ", e);
			rateQuoteDTO.setSuccess(false);
		}
		System.out.println("4===========================3");
		System.out.println(rateQuoteDTO);
		System.out.println("4===========================3");
		return CompletableFuture.completedFuture(rateQuoteDTO);
	}
	/*
	@Async
	@Override
	public CompletableFuture<RateQuoteDTO> getMyHomeMoveRate(RateRequestDTO request) {
		RateQuoteDTO rateQuote = new RateQuoteDTO();
		try {
			RateDelegate rateDelegate = rateService.getRatePort();
				
			MyRateRequest rateRequest = createMyRateRequest(request);
	
			MyHomeMoveRateResponse response = rateDelegate.getHomeMoveRateEstimate(rateRequest);
			rateQuote = createMyHomeMoveRateQuote(response);
				
		} catch (Exception e) {
			log.error("Exception in Old Dominion getHomeMoveRateEstimate service", e);
			rateQuote.setSuccess(false);
		}
		return CompletableFuture.completedFuture(rateQuote);
	}
	*/
	
	// OVE METODE SE SELE U KLASU ODFLRateRequestDto tj ODFLRateQuoteDto  !!!!!
	private MyRateRequest createMyRateRequest(ODFLRateRequestDto requestDto){
		
		MyRateRequest request = new MyRateRequest();
		
		request.setOdfl4MeUser(ODLF_USER);
		request.setOdfl4MePassword(ODLF_PASSWORD);
		request.setOdflCustomerAccount(ODLF_CUSTOMER_ACCOUNT_NUMBER);
		
		for (FreightUnit temp : requestDto.getFreightUnits()) {
			
			Freight freight = new Freight();
			
			freight.setActualClass(temp.getActualClass().toString());
			freight.setHeight(temp.getDimH());
			freight.setLength(temp.getDimL());
			freight.setWidth(temp.getDimW());
			freight.setWeight(temp.getWeight()); //mandatory
			freight.setRatedClass(temp.getRatedClass());
			freight.setNmfc(temp.getNmfc().toString());
			freight.setNmfcSub(temp.getNmfcSub().toString());
			freight.setNumberOfUnits(temp.getNumberOfUnits());
			freight.setDimensionUnits(temp.getDimUnit());
			
			request.getFreightItems().add(freight); //mandatory
		}
		
		//request.setCodAmount(requestDto.getCodAmount());
		//request.setContactPhoneNumber(requestDto.getContactPhoneNumber());
		request.setCubicUnits(requestDto.getCubicUnits());
		request.setCurrencyFormat(requestDto.getCurrencyFormat());
		//request.setDeliveryDateTime(requestDto.getDeliveryDateTime()); //TODO  XMLGregorian
		request.setDestinationCountry(requestDto.getDestinationCountry().toString());
		request.setDestinationPostalCode(requestDto.getDestinationPostalCode()); //mandatory
		//request.setDiscount(requestDto.getDiscount());  TODO BigDecimal !!
		//request.setDropDateTime(requestDto.getDropDateTime());
		request.setEmail(requestDto.getEmail());
		//request.setEquipmentType(value);
		request.setFirstName(requestDto.getUser().getFirstName());
		request.setInsuranceAmount(requestDto.getInsuranceAmount()); 
		request.setLastName(requestDto.getUser().getLastName());
		request.setLinearFeet(requestDto.getLinearFeet());
		request.setLoosePieces(requestDto.getLoosePieces());
		request.setMexicoServiceCenter(requestDto.getMexicoServiceCenter());
		request.setNumberPallets(requestDto.getNumberPallets());
		request.setNumberStackablePallets(requestDto.getNumberStackablePallets());
		request.setOriginCountry(requestDto.getOriginCountry().toString());
		request.setOriginPostalCode(requestDto.getOriginPostalCode()); //mandatory
		//request.setPickupDateTime(requestDto.getPickupDateTime());
		request.setRequestReferenceNumber(requestDto.isRequestReferenceNumber()); // mandatory
		request.setSendEmailOffers(requestDto.isSendEmailOffers());
		request.setShipType(requestDto.getShipType());
		request.setTariff(requestDto.getTariff());
		request.setTotalCubicVolume(requestDto.getTotalCubicVolume());
		request.setWeightUnits(requestDto.getWeightUnits());
		
		List<String> temp = request.getAccessorials();
		if (requestDto.getAccessorials().isHazardousMaterials()) temp.add("HAZ");
		if (requestDto.getAccessorials().isSelfStorageDelivery()) temp.add("SWD");
		//TODO
		return request;
	}
	
	private ODFLRateQuoteDto createMyLtlRateQuote(MyLTLRateResponse response) {
		ODFLRateQuoteDto rateQuote = new ODFLRateQuoteDto();
		rateQuote.setSuccess(response.isSuccess());
		//TODO set rest of values
		return rateQuote;
	}
	/*
	private RateQuoteDTO createMyHomeMoveRateQuote(MyHomeMoveRateResponse response) {
		RateQuoteDTO rateQuote = new RateQuoteDTO();
		rateQuote.setSuccess(response.isSuccess());
		//TODO set rest of values
		return rateQuote;
	}
	*/
}
