
package com.truesoft.ltl.partners.odfl.ws.myrate;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.truesoft.ltl.partners.odfl.ws.myrate package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetHomeMoveRateEstimateResponse_QNAME = new QName("http://myRate.ws.odfl.com/", "getHomeMoveRateEstimateResponse");
    private final static QName _GetLTLRateEstimate_QNAME = new QName("http://myRate.ws.odfl.com/", "getLTLRateEstimate");
    private final static QName _GetLTLRateEstimateResponse_QNAME = new QName("http://myRate.ws.odfl.com/", "getLTLRateEstimateResponse");
    private final static QName _GetHomeMoveRateEstimate_QNAME = new QName("http://myRate.ws.odfl.com/", "getHomeMoveRateEstimate");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.truesoft.ltl.partners.odfl.ws.myrate
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetHomeMoveRateEstimate }
     * 
     */
    public GetHomeMoveRateEstimate createGetHomeMoveRateEstimate() {
        return new GetHomeMoveRateEstimate();
    }

    /**
     * Create an instance of {@link GetLTLRateEstimate }
     * 
     */
    public GetLTLRateEstimate createGetLTLRateEstimate() {
        return new GetLTLRateEstimate();
    }

    /**
     * Create an instance of {@link GetLTLRateEstimateResponse }
     * 
     */
    public GetLTLRateEstimateResponse createGetLTLRateEstimateResponse() {
        return new GetLTLRateEstimateResponse();
    }

    /**
     * Create an instance of {@link GetHomeMoveRateEstimateResponse }
     * 
     */
    public GetHomeMoveRateEstimateResponse createGetHomeMoveRateEstimateResponse() {
        return new GetHomeMoveRateEstimateResponse();
    }

    /**
     * Create an instance of {@link MyHomeMoveRateResponse }
     * 
     */
    public MyHomeMoveRateResponse createMyHomeMoveRateResponse() {
        return new MyHomeMoveRateResponse();
    }

    /**
     * Create an instance of {@link HomeMoveEstimateDetail }
     * 
     */
    public HomeMoveEstimateDetail createHomeMoveEstimateDetail() {
        return new HomeMoveEstimateDetail();
    }

    /**
     * Create an instance of {@link City }
     * 
     */
    public City createCity() {
        return new City();
    }

    /**
     * Create an instance of {@link MyLTLRateResponse }
     * 
     */
    public MyLTLRateResponse createMyLTLRateResponse() {
        return new MyLTLRateResponse();
    }

    /**
     * Create an instance of {@link Freight }
     * 
     */
    public Freight createFreight() {
        return new Freight();
    }

    /**
     * Create an instance of {@link MyRateResponse }
     * 
     */
    public MyRateResponse createMyRateResponse() {
        return new MyRateResponse();
    }

    /**
     * Create an instance of {@link AccessorialCharge }
     * 
     */
    public AccessorialCharge createAccessorialCharge() {
        return new AccessorialCharge();
    }

    /**
     * Create an instance of {@link EstimateDetail }
     * 
     */
    public EstimateDetail createEstimateDetail() {
        return new EstimateDetail();
    }

    /**
     * Create an instance of {@link MyRateRequest }
     * 
     */
    public MyRateRequest createMyRateRequest() {
        return new MyRateRequest();
    }

    /**
     * Create an instance of {@link ServiceCenter }
     * 
     */
    public ServiceCenter createServiceCenter() {
        return new ServiceCenter();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetHomeMoveRateEstimateResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://myRate.ws.odfl.com/", name = "getHomeMoveRateEstimateResponse")
    public JAXBElement<GetHomeMoveRateEstimateResponse> createGetHomeMoveRateEstimateResponse(GetHomeMoveRateEstimateResponse value) {
        return new JAXBElement<GetHomeMoveRateEstimateResponse>(_GetHomeMoveRateEstimateResponse_QNAME, GetHomeMoveRateEstimateResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLTLRateEstimate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://myRate.ws.odfl.com/", name = "getLTLRateEstimate")
    public JAXBElement<GetLTLRateEstimate> createGetLTLRateEstimate(GetLTLRateEstimate value) {
        return new JAXBElement<GetLTLRateEstimate>(_GetLTLRateEstimate_QNAME, GetLTLRateEstimate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLTLRateEstimateResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://myRate.ws.odfl.com/", name = "getLTLRateEstimateResponse")
    public JAXBElement<GetLTLRateEstimateResponse> createGetLTLRateEstimateResponse(GetLTLRateEstimateResponse value) {
        return new JAXBElement<GetLTLRateEstimateResponse>(_GetLTLRateEstimateResponse_QNAME, GetLTLRateEstimateResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetHomeMoveRateEstimate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://myRate.ws.odfl.com/", name = "getHomeMoveRateEstimate")
    public JAXBElement<GetHomeMoveRateEstimate> createGetHomeMoveRateEstimate(GetHomeMoveRateEstimate value) {
        return new JAXBElement<GetHomeMoveRateEstimate>(_GetHomeMoveRateEstimate_QNAME, GetHomeMoveRateEstimate.class, null, value);
    }

}
