package com.truesoft.ltl.partners.service;

import java.util.List;

import com.truesoft.ltl.service.dto.ODFLRateQuoteDto;
import com.truesoft.ltl.service.dto.ODFLRateRequestDto;


public interface BasePartnersService {
	
	/**
	 * Returns rateQuotes given from all partners
	 * 
	 * @param request
	 * @return
	 */
	List<ODFLRateQuoteDto> getAllRateQuotes(ODFLRateRequestDto request);

}
