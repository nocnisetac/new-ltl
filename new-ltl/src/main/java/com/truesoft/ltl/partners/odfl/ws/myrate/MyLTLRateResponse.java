
package com.truesoft.ltl.partners.odfl.ws.myrate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for myLTLRateResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="myLTLRateResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://myRate.ws.odfl.com/}myRateResponse">
 *       &lt;sequence>
 *         &lt;element name="rateEstimate" type="{http://myRate.ws.odfl.com/}estimateDetail" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "myLTLRateResponse", propOrder = {
    "rateEstimate"
})
public class MyLTLRateResponse
    extends MyRateResponse
{

    protected EstimateDetail rateEstimate;

    /**
     * Gets the value of the rateEstimate property.
     * 
     * @return
     *     possible object is
     *     {@link EstimateDetail }
     *     
     */
    public EstimateDetail getRateEstimate() {
        return rateEstimate;
    }

    /**
     * Sets the value of the rateEstimate property.
     * 
     * @param value
     *     allowed object is
     *     {@link EstimateDetail }
     *     
     */
    public void setRateEstimate(EstimateDetail value) {
        this.rateEstimate = value;
    }

}
