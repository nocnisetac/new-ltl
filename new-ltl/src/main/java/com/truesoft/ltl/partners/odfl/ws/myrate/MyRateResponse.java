
package com.truesoft.ltl.partners.odfl.ws.myrate;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for myRateResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="myRateResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="destinationCities" type="{http://myRate.ws.odfl.com/}city" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="destinationInterlineCarrierName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="destinationInterlineScac" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="destinationServiceCenter" type="{http://myRate.ws.odfl.com/}serviceCenter" minOccurs="0"/>
 *         &lt;element name="errorMessages" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="originatingInterlineCarrierName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="originatingInterlineScac" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="originatingServiceCenter" type="{http://myRate.ws.odfl.com/}serviceCenter" minOccurs="0"/>
 *         &lt;element name="referenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="success" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "myRateResponse", propOrder = {
    "destinationCities",
    "destinationInterlineCarrierName",
    "destinationInterlineScac",
    "destinationServiceCenter",
    "errorMessages",
    "originatingInterlineCarrierName",
    "originatingInterlineScac",
    "originatingServiceCenter",
    "referenceNumber",
    "success"
})
@XmlSeeAlso({
    MyHomeMoveRateResponse.class,
    MyLTLRateResponse.class
})
public class MyRateResponse {

    @XmlElement(nillable = true)
    protected List<City> destinationCities;
    protected String destinationInterlineCarrierName;
    protected String destinationInterlineScac;
    protected ServiceCenter destinationServiceCenter;
    @XmlElement(nillable = true)
    protected List<String> errorMessages;
    protected String originatingInterlineCarrierName;
    protected String originatingInterlineScac;
    protected ServiceCenter originatingServiceCenter;
    protected String referenceNumber;
    protected boolean success;

    /**
     * Gets the value of the destinationCities property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the destinationCities property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDestinationCities().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link City }
     * 
     * 
     */
    public List<City> getDestinationCities() {
        if (destinationCities == null) {
            destinationCities = new ArrayList<City>();
        }
        return this.destinationCities;
    }

    /**
     * Gets the value of the destinationInterlineCarrierName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestinationInterlineCarrierName() {
        return destinationInterlineCarrierName;
    }

    /**
     * Sets the value of the destinationInterlineCarrierName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestinationInterlineCarrierName(String value) {
        this.destinationInterlineCarrierName = value;
    }

    /**
     * Gets the value of the destinationInterlineScac property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestinationInterlineScac() {
        return destinationInterlineScac;
    }

    /**
     * Sets the value of the destinationInterlineScac property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestinationInterlineScac(String value) {
        this.destinationInterlineScac = value;
    }

    /**
     * Gets the value of the destinationServiceCenter property.
     * 
     * @return
     *     possible object is
     *     {@link ServiceCenter }
     *     
     */
    public ServiceCenter getDestinationServiceCenter() {
        return destinationServiceCenter;
    }

    /**
     * Sets the value of the destinationServiceCenter property.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceCenter }
     *     
     */
    public void setDestinationServiceCenter(ServiceCenter value) {
        this.destinationServiceCenter = value;
    }

    /**
     * Gets the value of the errorMessages property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the errorMessages property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getErrorMessages().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getErrorMessages() {
        if (errorMessages == null) {
            errorMessages = new ArrayList<String>();
        }
        return this.errorMessages;
    }

    /**
     * Gets the value of the originatingInterlineCarrierName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginatingInterlineCarrierName() {
        return originatingInterlineCarrierName;
    }

    /**
     * Sets the value of the originatingInterlineCarrierName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginatingInterlineCarrierName(String value) {
        this.originatingInterlineCarrierName = value;
    }

    /**
     * Gets the value of the originatingInterlineScac property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginatingInterlineScac() {
        return originatingInterlineScac;
    }

    /**
     * Sets the value of the originatingInterlineScac property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginatingInterlineScac(String value) {
        this.originatingInterlineScac = value;
    }

    /**
     * Gets the value of the originatingServiceCenter property.
     * 
     * @return
     *     possible object is
     *     {@link ServiceCenter }
     *     
     */
    public ServiceCenter getOriginatingServiceCenter() {
        return originatingServiceCenter;
    }

    /**
     * Sets the value of the originatingServiceCenter property.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceCenter }
     *     
     */
    public void setOriginatingServiceCenter(ServiceCenter value) {
        this.originatingServiceCenter = value;
    }

    /**
     * Gets the value of the referenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenceNumber() {
        return referenceNumber;
    }

    /**
     * Sets the value of the referenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenceNumber(String value) {
        this.referenceNumber = value;
    }

    /**
     * Gets the value of the success property.
     * 
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * Sets the value of the success property.
     * 
     */
    public void setSuccess(boolean value) {
        this.success = value;
    }

}
