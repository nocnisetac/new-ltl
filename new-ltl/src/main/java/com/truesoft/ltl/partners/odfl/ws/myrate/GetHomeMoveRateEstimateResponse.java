
package com.truesoft.ltl.partners.odfl.ws.myrate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getHomeMoveRateEstimateResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getHomeMoveRateEstimateResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http://myRate.ws.odfl.com/}myHomeMoveRateResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getHomeMoveRateEstimateResponse", propOrder = {
    "_return"
})
public class GetHomeMoveRateEstimateResponse {

    @XmlElement(name = "return")
    protected MyHomeMoveRateResponse _return;

    /**
     * Gets the value of the return property.
     * 
     * @return
     *     possible object is
     *     {@link MyHomeMoveRateResponse }
     *     
     */
    public MyHomeMoveRateResponse getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     * 
     * @param value
     *     allowed object is
     *     {@link MyHomeMoveRateResponse }
     *     
     */
    public void setReturn(MyHomeMoveRateResponse value) {
        this._return = value;
    }

}
