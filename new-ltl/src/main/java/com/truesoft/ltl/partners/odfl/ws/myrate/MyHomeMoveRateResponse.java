
package com.truesoft.ltl.partners.odfl.ws.myrate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for myHomeMoveRateResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="myHomeMoveRateResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://myRate.ws.odfl.com/}myRateResponse">
 *       &lt;sequence>
 *         &lt;element name="rateEstimate" type="{http://myRate.ws.odfl.com/}homeMoveEstimateDetail" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "myHomeMoveRateResponse", propOrder = {
    "rateEstimate"
})
public class MyHomeMoveRateResponse
    extends MyRateResponse
{

    protected HomeMoveEstimateDetail rateEstimate;

    /**
     * Gets the value of the rateEstimate property.
     * 
     * @return
     *     possible object is
     *     {@link HomeMoveEstimateDetail }
     *     
     */
    public HomeMoveEstimateDetail getRateEstimate() {
        return rateEstimate;
    }

    /**
     * Sets the value of the rateEstimate property.
     * 
     * @param value
     *     allowed object is
     *     {@link HomeMoveEstimateDetail }
     *     
     */
    public void setRateEstimate(HomeMoveEstimateDetail value) {
        this.rateEstimate = value;
    }

}
