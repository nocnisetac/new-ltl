
package com.truesoft.ltl.partners.odfl.ws.myrate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for myRateRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="myRateRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="accessorials" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="codAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="contactPhoneNumber" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="cubicUnits" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="currencyFormat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="deliveryDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="destinationCountry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="destinationPostalCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="discount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="dropDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="equipmentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="firstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="freightItems" type="{http://myRate.ws.odfl.com/}freight" maxOccurs="unbounded"/>
 *         &lt;element name="insuranceAmount" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="lastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="linearFeet" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="loosePieces" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="mexicoServiceCenter" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="movement" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numberPallets" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="numberStackablePallets" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="odfl4MePassword" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="odfl4MeUser" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="odflCustomerAccount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="originCountry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="originPostalCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="pickupDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="requestReferenceNumber" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="sendEmailOffers" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="shipType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tariff" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="totalCubicVolume" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="weightUnits" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "myRateRequest", propOrder = {
    "accessorials",
    "codAmount",
    "contactPhoneNumber",
    "cubicUnits",
    "currencyFormat",
    "deliveryDateTime",
    "destinationCountry",
    "destinationPostalCode",
    "discount",
    "dropDateTime",
    "email",
    "equipmentType",
    "firstName",
    "freightItems",
    "insuranceAmount",
    "lastName",
    "linearFeet",
    "loosePieces",
    "mexicoServiceCenter",
    "movement",
    "numberPallets",
    "numberStackablePallets",
    "odfl4MePassword",
    "odfl4MeUser",
    "odflCustomerAccount",
    "originCountry",
    "originPostalCode",
    "pickupDateTime",
    "requestReferenceNumber",
    "sendEmailOffers",
    "shipType",
    "tariff",
    "totalCubicVolume",
    "weightUnits"
})
public class MyRateRequest {

    @XmlElement(nillable = true)
    protected List<String> accessorials;
    protected BigDecimal codAmount;
    protected BigDecimal contactPhoneNumber;
    protected String cubicUnits;
    protected String currencyFormat;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar deliveryDateTime;
    protected String destinationCountry;
    @XmlElement(required = true)
    protected String destinationPostalCode;
    protected BigDecimal discount;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dropDateTime;
    protected String email;
    protected String equipmentType;
    protected String firstName;
    @XmlElement(required = true, nillable = true)
    protected List<Freight> freightItems;
    protected Integer insuranceAmount;
    protected String lastName;
    protected Integer linearFeet;
    protected Integer loosePieces;
    protected String mexicoServiceCenter;
    protected String movement;
    protected Integer numberPallets;
    protected Integer numberStackablePallets;
    @XmlElement(required = true)
    protected String odfl4MePassword;
    @XmlElement(required = true)
    protected String odfl4MeUser;
    protected long odflCustomerAccount;
    protected String originCountry;
    @XmlElement(required = true)
    protected String originPostalCode;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar pickupDateTime;
    protected boolean requestReferenceNumber;
    protected Boolean sendEmailOffers;
    protected String shipType;
    protected Integer tariff;
    protected Integer totalCubicVolume;
    protected String weightUnits;

    /**
     * Gets the value of the accessorials property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the accessorials property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccessorials().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getAccessorials() {
        if (accessorials == null) {
            accessorials = new ArrayList<String>();
        }
        return this.accessorials;
    }

    /**
     * Gets the value of the codAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCodAmount() {
        return codAmount;
    }

    /**
     * Sets the value of the codAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCodAmount(BigDecimal value) {
        this.codAmount = value;
    }

    /**
     * Gets the value of the contactPhoneNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getContactPhoneNumber() {
        return contactPhoneNumber;
    }

    /**
     * Sets the value of the contactPhoneNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setContactPhoneNumber(BigDecimal value) {
        this.contactPhoneNumber = value;
    }

    /**
     * Gets the value of the cubicUnits property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCubicUnits() {
        return cubicUnits;
    }

    /**
     * Sets the value of the cubicUnits property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCubicUnits(String value) {
        this.cubicUnits = value;
    }

    /**
     * Gets the value of the currencyFormat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyFormat() {
        return currencyFormat;
    }

    /**
     * Sets the value of the currencyFormat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyFormat(String value) {
        this.currencyFormat = value;
    }

    /**
     * Gets the value of the deliveryDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDeliveryDateTime() {
        return deliveryDateTime;
    }

    /**
     * Sets the value of the deliveryDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDeliveryDateTime(XMLGregorianCalendar value) {
        this.deliveryDateTime = value;
    }

    /**
     * Gets the value of the destinationCountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestinationCountry() {
        return destinationCountry;
    }

    /**
     * Sets the value of the destinationCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestinationCountry(String value) {
        this.destinationCountry = value;
    }

    /**
     * Gets the value of the destinationPostalCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestinationPostalCode() {
        return destinationPostalCode;
    }

    /**
     * Sets the value of the destinationPostalCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestinationPostalCode(String value) {
        this.destinationPostalCode = value;
    }

    /**
     * Gets the value of the discount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDiscount() {
        return discount;
    }

    /**
     * Sets the value of the discount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDiscount(BigDecimal value) {
        this.discount = value;
    }

    /**
     * Gets the value of the dropDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDropDateTime() {
        return dropDateTime;
    }

    /**
     * Sets the value of the dropDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDropDateTime(XMLGregorianCalendar value) {
        this.dropDateTime = value;
    }

    /**
     * Gets the value of the email property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the value of the email property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Gets the value of the equipmentType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentType() {
        return equipmentType;
    }

    /**
     * Sets the value of the equipmentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentType(String value) {
        this.equipmentType = value;
    }

    /**
     * Gets the value of the firstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the value of the firstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Gets the value of the freightItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the freightItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFreightItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Freight }
     * 
     * 
     */
    public List<Freight> getFreightItems() {
        if (freightItems == null) {
            freightItems = new ArrayList<Freight>();
        }
        return this.freightItems;
    }

    /**
     * Gets the value of the insuranceAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInsuranceAmount() {
        return insuranceAmount;
    }

    /**
     * Sets the value of the insuranceAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInsuranceAmount(Integer value) {
        this.insuranceAmount = value;
    }

    /**
     * Gets the value of the lastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the value of the lastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastName(String value) {
        this.lastName = value;
    }

    /**
     * Gets the value of the linearFeet property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLinearFeet() {
        return linearFeet;
    }

    /**
     * Sets the value of the linearFeet property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLinearFeet(Integer value) {
        this.linearFeet = value;
    }

    /**
     * Gets the value of the loosePieces property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLoosePieces() {
        return loosePieces;
    }

    /**
     * Sets the value of the loosePieces property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLoosePieces(Integer value) {
        this.loosePieces = value;
    }

    /**
     * Gets the value of the mexicoServiceCenter property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMexicoServiceCenter() {
        return mexicoServiceCenter;
    }

    /**
     * Sets the value of the mexicoServiceCenter property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMexicoServiceCenter(String value) {
        this.mexicoServiceCenter = value;
    }

    /**
     * Gets the value of the movement property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMovement() {
        return movement;
    }

    /**
     * Sets the value of the movement property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMovement(String value) {
        this.movement = value;
    }

    /**
     * Gets the value of the numberPallets property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberPallets() {
        return numberPallets;
    }

    /**
     * Sets the value of the numberPallets property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberPallets(Integer value) {
        this.numberPallets = value;
    }

    /**
     * Gets the value of the numberStackablePallets property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberStackablePallets() {
        return numberStackablePallets;
    }

    /**
     * Sets the value of the numberStackablePallets property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberStackablePallets(Integer value) {
        this.numberStackablePallets = value;
    }

    /**
     * Gets the value of the odfl4MePassword property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOdfl4MePassword() {
        return odfl4MePassword;
    }

    /**
     * Sets the value of the odfl4MePassword property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOdfl4MePassword(String value) {
        this.odfl4MePassword = value;
    }

    /**
     * Gets the value of the odfl4MeUser property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOdfl4MeUser() {
        return odfl4MeUser;
    }

    /**
     * Sets the value of the odfl4MeUser property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOdfl4MeUser(String value) {
        this.odfl4MeUser = value;
    }

    /**
     * Gets the value of the odflCustomerAccount property.
     * 
     */
    public long getOdflCustomerAccount() {
        return odflCustomerAccount;
    }

    /**
     * Sets the value of the odflCustomerAccount property.
     * 
     */
    public void setOdflCustomerAccount(long value) {
        this.odflCustomerAccount = value;
    }

    /**
     * Gets the value of the originCountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginCountry() {
        return originCountry;
    }

    /**
     * Sets the value of the originCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginCountry(String value) {
        this.originCountry = value;
    }

    /**
     * Gets the value of the originPostalCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginPostalCode() {
        return originPostalCode;
    }

    /**
     * Sets the value of the originPostalCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginPostalCode(String value) {
        this.originPostalCode = value;
    }

    /**
     * Gets the value of the pickupDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPickupDateTime() {
        return pickupDateTime;
    }

    /**
     * Sets the value of the pickupDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPickupDateTime(XMLGregorianCalendar value) {
        this.pickupDateTime = value;
    }

    /**
     * Gets the value of the requestReferenceNumber property.
     * 
     */
    public boolean isRequestReferenceNumber() {
        return requestReferenceNumber;
    }

    /**
     * Sets the value of the requestReferenceNumber property.
     * 
     */
    public void setRequestReferenceNumber(boolean value) {
        this.requestReferenceNumber = value;
    }

    /**
     * Gets the value of the sendEmailOffers property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSendEmailOffers() {
        return sendEmailOffers;
    }

    /**
     * Sets the value of the sendEmailOffers property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSendEmailOffers(Boolean value) {
        this.sendEmailOffers = value;
    }

    /**
     * Gets the value of the shipType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipType() {
        return shipType;
    }

    /**
     * Sets the value of the shipType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipType(String value) {
        this.shipType = value;
    }

    /**
     * Gets the value of the tariff property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTariff() {
        return tariff;
    }

    /**
     * Sets the value of the tariff property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTariff(Integer value) {
        this.tariff = value;
    }

    /**
     * Gets the value of the totalCubicVolume property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTotalCubicVolume() {
        return totalCubicVolume;
    }

    /**
     * Sets the value of the totalCubicVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTotalCubicVolume(Integer value) {
        this.totalCubicVolume = value;
    }

    /**
     * Gets the value of the weightUnits property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWeightUnits() {
        return weightUnits;
    }

    /**
     * Sets the value of the weightUnits property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWeightUnits(String value) {
        this.weightUnits = value;
    }

}
