package com.truesoft.ltl.partners.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.truesoft.ltl.partners.service.BasePartnersService;
import com.truesoft.ltl.partners.service.OldDominionService;
import com.truesoft.ltl.service.dto.ODFLRateQuoteDto;
import com.truesoft.ltl.service.dto.ODFLRateRequestDto;

@Service
public class BasePartnersServiceImpl implements BasePartnersService{
	
	private final Logger log = LoggerFactory.getLogger(BasePartnersServiceImpl.class);
	
	@Autowired
	private OldDominionService oldDominionService;

	@Override
	public List<ODFLRateQuoteDto> getAllRateQuotes(ODFLRateRequestDto request) {
		
		List<ODFLRateQuoteDto> rateQuotes = new ArrayList<>();
		
		CompletableFuture<ODFLRateQuoteDto> oldDominionMyLtlRate = oldDominionService.getMyLTLRate(request); // starts service in separate thread
		//CompletableFuture<RateQuoteDTO> oldDominionMyHomeMoveRate = oldDominionService.getMyHomeMoveRate(request);
		// here will be invoked all partners services 
		
		//CompletableFuture.allOf(oldDominionMyLtlRate, oldDominionMyHomeMoveRate).join(); // wait for all service calls to finish
		CompletableFuture.allOf(oldDominionMyLtlRate).join();
		
		try {
			if(oldDominionMyLtlRate.get().isSuccess()) {
				rateQuotes.add(oldDominionMyLtlRate.get());
			}
		} catch (InterruptedException|ExecutionException e) {
			log.error("Error invoking Old Dominion Service getMyLTLRate", e);
		}
		
		/*try {
			if(oldDominionMyHomeMoveRate.get().isSuccess()) {
				rateQuotes.add(oldDominionMyHomeMoveRate.get());
			}
		} catch (InterruptedException|ExecutionException e) {
			log.error("Error invoking Old Dominion Service getMyHomeMoveRate", e);
		}*/
		
		return rateQuotes;
	}

}
