
package com.truesoft.ltl.partners.odfl.ws.myrate;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for homeMoveEstimateDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="homeMoveEstimateDetail">
 *   &lt;complexContent>
 *     &lt;extension base="{http://myRate.ws.odfl.com/}estimateDetail">
 *       &lt;sequence>
 *         &lt;element name="linearFeetCharge" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="milesFromDestination" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="milesFromOrigin" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "homeMoveEstimateDetail", propOrder = {
    "linearFeetCharge",
    "milesFromDestination",
    "milesFromOrigin"
})
public class HomeMoveEstimateDetail
    extends EstimateDetail
{

    protected BigDecimal linearFeetCharge;
    protected BigDecimal milesFromDestination;
    protected BigDecimal milesFromOrigin;

    /**
     * Gets the value of the linearFeetCharge property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLinearFeetCharge() {
        return linearFeetCharge;
    }

    /**
     * Sets the value of the linearFeetCharge property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLinearFeetCharge(BigDecimal value) {
        this.linearFeetCharge = value;
    }

    /**
     * Gets the value of the milesFromDestination property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMilesFromDestination() {
        return milesFromDestination;
    }

    /**
     * Sets the value of the milesFromDestination property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMilesFromDestination(BigDecimal value) {
        this.milesFromDestination = value;
    }

    /**
     * Gets the value of the milesFromOrigin property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMilesFromOrigin() {
        return milesFromOrigin;
    }

    /**
     * Sets the value of the milesFromOrigin property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMilesFromOrigin(BigDecimal value) {
        this.milesFromOrigin = value;
    }

}
