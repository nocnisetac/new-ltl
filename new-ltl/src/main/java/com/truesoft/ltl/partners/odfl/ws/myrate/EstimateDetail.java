
package com.truesoft.ltl.partners.odfl.ws.myrate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for estimateDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="estimateDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="accessorialCharges" type="{http://myRate.ws.odfl.com/}accessorialCharge" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="discountAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="discountPercentage" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="discountedFreightCharge" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="fuelSurcharge" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="grossFreightCharge" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="guaranteedServiceOption" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="internationalCharge" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="netFreightCharge" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="totalAccessorialCharge" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="variableAccessorialRate" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "estimateDetail", propOrder = {
    "accessorialCharges",
    "discountAmount",
    "discountPercentage",
    "discountedFreightCharge",
    "fuelSurcharge",
    "grossFreightCharge",
    "guaranteedServiceOption",
    "internationalCharge",
    "netFreightCharge",
    "totalAccessorialCharge",
    "variableAccessorialRate"
})
@XmlSeeAlso({
    HomeMoveEstimateDetail.class
})
public class EstimateDetail {

    @XmlElement(nillable = true)
    protected List<AccessorialCharge> accessorialCharges;
    protected BigDecimal discountAmount;
    protected BigDecimal discountPercentage;
    protected BigDecimal discountedFreightCharge;
    protected BigDecimal fuelSurcharge;
    protected BigDecimal grossFreightCharge;
    protected BigDecimal guaranteedServiceOption;
    protected BigDecimal internationalCharge;
    protected BigDecimal netFreightCharge;
    protected BigDecimal totalAccessorialCharge;
    protected boolean variableAccessorialRate;

    /**
     * Gets the value of the accessorialCharges property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the accessorialCharges property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccessorialCharges().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AccessorialCharge }
     * 
     * 
     */
    public List<AccessorialCharge> getAccessorialCharges() {
        if (accessorialCharges == null) {
            accessorialCharges = new ArrayList<AccessorialCharge>();
        }
        return this.accessorialCharges;
    }

    /**
     * Gets the value of the discountAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    /**
     * Sets the value of the discountAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDiscountAmount(BigDecimal value) {
        this.discountAmount = value;
    }

    /**
     * Gets the value of the discountPercentage property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDiscountPercentage() {
        return discountPercentage;
    }

    /**
     * Sets the value of the discountPercentage property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDiscountPercentage(BigDecimal value) {
        this.discountPercentage = value;
    }

    /**
     * Gets the value of the discountedFreightCharge property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDiscountedFreightCharge() {
        return discountedFreightCharge;
    }

    /**
     * Sets the value of the discountedFreightCharge property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDiscountedFreightCharge(BigDecimal value) {
        this.discountedFreightCharge = value;
    }

    /**
     * Gets the value of the fuelSurcharge property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFuelSurcharge() {
        return fuelSurcharge;
    }

    /**
     * Sets the value of the fuelSurcharge property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFuelSurcharge(BigDecimal value) {
        this.fuelSurcharge = value;
    }

    /**
     * Gets the value of the grossFreightCharge property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getGrossFreightCharge() {
        return grossFreightCharge;
    }

    /**
     * Sets the value of the grossFreightCharge property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setGrossFreightCharge(BigDecimal value) {
        this.grossFreightCharge = value;
    }

    /**
     * Gets the value of the guaranteedServiceOption property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getGuaranteedServiceOption() {
        return guaranteedServiceOption;
    }

    /**
     * Sets the value of the guaranteedServiceOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setGuaranteedServiceOption(BigDecimal value) {
        this.guaranteedServiceOption = value;
    }

    /**
     * Gets the value of the internationalCharge property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getInternationalCharge() {
        return internationalCharge;
    }

    /**
     * Sets the value of the internationalCharge property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setInternationalCharge(BigDecimal value) {
        this.internationalCharge = value;
    }

    /**
     * Gets the value of the netFreightCharge property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNetFreightCharge() {
        return netFreightCharge;
    }

    /**
     * Sets the value of the netFreightCharge property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNetFreightCharge(BigDecimal value) {
        this.netFreightCharge = value;
    }

    /**
     * Gets the value of the totalAccessorialCharge property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalAccessorialCharge() {
        return totalAccessorialCharge;
    }

    /**
     * Sets the value of the totalAccessorialCharge property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalAccessorialCharge(BigDecimal value) {
        this.totalAccessorialCharge = value;
    }

    /**
     * Gets the value of the variableAccessorialRate property.
     * 
     */
    public boolean isVariableAccessorialRate() {
        return variableAccessorialRate;
    }

    /**
     * Sets the value of the variableAccessorialRate property.
     * 
     */
    public void setVariableAccessorialRate(boolean value) {
        this.variableAccessorialRate = value;
    }

}
