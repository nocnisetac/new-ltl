package com.truesoft.ltl.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A PostalCode.
 */
@Entity
@Table(name = "postal_code")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PostalCode implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "zip_code")
    private String zipCode;

    @Column(name = "state")
    private String state;

    @Column(name = "city")
    private String city;

    @Column(name = "county")
    private String county;

    @Column(name = "state_fips")
    private Integer stateFips;

    @Column(name = "county_fip")
    private Integer countyFip;

    @Column(name = "latitude")
    private Double latitude;

    @Column(name = "longitude")
    private Double longitude;

    @Column(name = "preference")
    private String preference;

    @Column(name = "jhi_type")
    private String type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getZipCode() {
        return zipCode;
    }

    public PostalCode zipCode(String zipCode) {
        this.zipCode = zipCode;
        return this;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getState() {
        return state;
    }

    public PostalCode state(String state) {
        this.state = state;
        return this;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public PostalCode city(String city) {
        this.city = city;
        return this;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public PostalCode county(String county) {
        this.county = county;
        return this;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public Integer getStateFips() {
        return stateFips;
    }

    public PostalCode stateFips(Integer stateFips) {
        this.stateFips = stateFips;
        return this;
    }

    public void setStateFips(Integer stateFips) {
        this.stateFips = stateFips;
    }

    public Integer getCountyFip() {
        return countyFip;
    }

    public PostalCode countyFip(Integer countyFip) {
        this.countyFip = countyFip;
        return this;
    }

    public void setCountyFip(Integer countyFip) {
        this.countyFip = countyFip;
    }

    public Double getLatitude() {
        return latitude;
    }

    public PostalCode latitude(Double latitude) {
        this.latitude = latitude;
        return this;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public PostalCode longitude(Double longitude) {
        this.longitude = longitude;
        return this;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getPreference() {
        return preference;
    }

    public PostalCode preference(String preference) {
        this.preference = preference;
        return this;
    }

    public void setPreference(String preference) {
        this.preference = preference;
    }

    public String getType() {
        return type;
    }

    public PostalCode type(String type) {
        this.type = type;
        return this;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PostalCode postalCode = (PostalCode) o;
        if (postalCode.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), postalCode.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PostalCode{" +
            "id=" + getId() +
            ", zipCode='" + getZipCode() + "'" +
            ", state='" + getState() + "'" +
            ", city='" + getCity() + "'" +
            ", county='" + getCounty() + "'" +
            ", stateFips='" + getStateFips() + "'" +
            ", countyFip='" + getCountyFip() + "'" +
            ", latitude='" + getLatitude() + "'" +
            ", longitude='" + getLongitude() + "'" +
            ", preference='" + getPreference() + "'" +
            ", type='" + getType() + "'" +
            "}";
    }
}
