package com.truesoft.ltl.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A PickupDeliveryTime.
 */
@Entity
@Table(name = "pickup_delivery_time")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PickupDeliveryTime implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "pickup_date")
    private LocalDate pickupDate;

    @Column(name = "pickup_window_from")
    private String pickupWindowFrom;

    @Column(name = "pickup_window_to")
    private String pickupWindowTo;

    @Column(name = "delivery_date")
    private LocalDate deliveryDate;

    @Column(name = "delivery_window_from")
    private String deliveryWindowFrom;

    @Column(name = "delivery_window_to")
    private String deliveryWindowTo;

    @ManyToOne
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getPickupDate() {
        return pickupDate;
    }

    public PickupDeliveryTime pickupDate(LocalDate pickupDate) {
        this.pickupDate = pickupDate;
        return this;
    }

    public void setPickupDate(LocalDate pickupDate) {
        this.pickupDate = pickupDate;
    }

    public String getPickupWindowFrom() {
        return pickupWindowFrom;
    }

    public PickupDeliveryTime pickupWindowFrom(String pickupWindowFrom) {
        this.pickupWindowFrom = pickupWindowFrom;
        return this;
    }

    public void setPickupWindowFrom(String pickupWindowFrom) {
        this.pickupWindowFrom = pickupWindowFrom;
    }

    public String getPickupWindowTo() {
        return pickupWindowTo;
    }

    public PickupDeliveryTime pickupWindowTo(String pickupWindowTo) {
        this.pickupWindowTo = pickupWindowTo;
        return this;
    }

    public void setPickupWindowTo(String pickupWindowTo) {
        this.pickupWindowTo = pickupWindowTo;
    }

    public LocalDate getDeliveryDate() {
        return deliveryDate;
    }

    public PickupDeliveryTime deliveryDate(LocalDate deliveryDate) {
        this.deliveryDate = deliveryDate;
        return this;
    }

    public void setDeliveryDate(LocalDate deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getDeliveryWindowFrom() {
        return deliveryWindowFrom;
    }

    public PickupDeliveryTime deliveryWindowFrom(String deliveryWindowFrom) {
        this.deliveryWindowFrom = deliveryWindowFrom;
        return this;
    }

    public void setDeliveryWindowFrom(String deliveryWindowFrom) {
        this.deliveryWindowFrom = deliveryWindowFrom;
    }

    public String getDeliveryWindowTo() {
        return deliveryWindowTo;
    }

    public PickupDeliveryTime deliveryWindowTo(String deliveryWindowTo) {
        this.deliveryWindowTo = deliveryWindowTo;
        return this;
    }

    public void setDeliveryWindowTo(String deliveryWindowTo) {
        this.deliveryWindowTo = deliveryWindowTo;
    }

    public User getUser() {
        return user;
    }

    public PickupDeliveryTime user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PickupDeliveryTime pickupDeliveryTime = (PickupDeliveryTime) o;
        if (pickupDeliveryTime.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), pickupDeliveryTime.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PickupDeliveryTime{" +
            "id=" + getId() +
            ", pickupDate='" + getPickupDate() + "'" +
            ", pickupWindowFrom='" + getPickupWindowFrom() + "'" +
            ", pickupWindowTo='" + getPickupWindowTo() + "'" +
            ", deliveryDate='" + getDeliveryDate() + "'" +
            ", deliveryWindowFrom='" + getDeliveryWindowFrom() + "'" +
            ", deliveryWindowTo='" + getDeliveryWindowTo() + "'" +
            "}";
    }
}
