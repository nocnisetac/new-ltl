package com.truesoft.ltl.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A ServiceCenter.
 */
@Entity
@Table(name = "service_center")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ServiceCenter implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "address")
    private String address;

    @Column(name = "alpha_code")
    private String alphaCode;

    @Column(name = "city_state_zip")
    private String cityStateZip;

    @Column(name = "country")
    private String country;

    @Column(name = "email_address")
    private String emailAddress;

    @Column(name = "fax")
    private String fax;

    @Column(name = "manager")
    private String manager;

    @Column(name = "name")
    private String name;

    @Column(name = "phone")
    private String phone;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public ServiceCenter address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAlphaCode() {
        return alphaCode;
    }

    public ServiceCenter alphaCode(String alphaCode) {
        this.alphaCode = alphaCode;
        return this;
    }

    public void setAlphaCode(String alphaCode) {
        this.alphaCode = alphaCode;
    }

    public String getCityStateZip() {
        return cityStateZip;
    }

    public ServiceCenter cityStateZip(String cityStateZip) {
        this.cityStateZip = cityStateZip;
        return this;
    }

    public void setCityStateZip(String cityStateZip) {
        this.cityStateZip = cityStateZip;
    }

    public String getCountry() {
        return country;
    }

    public ServiceCenter country(String country) {
        this.country = country;
        return this;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public ServiceCenter emailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
        return this;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getFax() {
        return fax;
    }

    public ServiceCenter fax(String fax) {
        this.fax = fax;
        return this;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getManager() {
        return manager;
    }

    public ServiceCenter manager(String manager) {
        this.manager = manager;
        return this;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    public String getName() {
        return name;
    }

    public ServiceCenter name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public ServiceCenter phone(String phone) {
        this.phone = phone;
        return this;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ServiceCenter serviceCenter = (ServiceCenter) o;
        if (serviceCenter.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), serviceCenter.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ServiceCenter{" +
            "id=" + getId() +
            ", address='" + getAddress() + "'" +
            ", alphaCode='" + getAlphaCode() + "'" +
            ", cityStateZip='" + getCityStateZip() + "'" +
            ", country='" + getCountry() + "'" +
            ", emailAddress='" + getEmailAddress() + "'" +
            ", fax='" + getFax() + "'" +
            ", manager='" + getManager() + "'" +
            ", name='" + getName() + "'" +
            ", phone='" + getPhone() + "'" +
            "}";
    }
}
