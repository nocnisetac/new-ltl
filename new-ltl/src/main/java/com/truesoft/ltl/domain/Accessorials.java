package com.truesoft.ltl.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A Accessorials.
 */
@Entity
@Table(name = "accessorials")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Accessorials implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "arrival_notification")
    private Boolean arrivalNotification;

    @Column(name = "appointment")
    private Boolean appointment;

    @Column(name = "liftgate_pickup")
    private Boolean liftgatePickup;

    @Column(name = "liftgate_delivery")
    private Boolean liftgateDelivery;

    @Column(name = "residential_noncommercial_pickup")
    private Boolean residentialNoncommercialPickup;

    @Column(name = "residential_noncommercial_delivery")
    private Boolean residentialNoncommercialDelivery;

    @Column(name = "inside_pickup")
    private Boolean insidePickup;

    @Column(name = "inside_delivery")
    private Boolean insideDelivery;

    @Column(name = "tradeshow_pickup")
    private Boolean tradeshowPickup;

    @Column(name = "tradeshow_delivery")
    private Boolean tradeshowDelivery;

    @Column(name = "airport_pickup")
    private Boolean airportPickup;

    @Column(name = "airport_delivery")
    private Boolean airportDelivery;

    @Column(name = "schools_colleges_churches_pickup")
    private Boolean schoolsCollegesChurchesPickup;

    @Column(name = "schools_colleges_churches_delivery")
    private Boolean schoolsCollegesChurchesDelivery;

    @Column(name = "construction_site_pickup")
    private Boolean constructionSitePickup;

    @Column(name = "construction_site_delivery")
    private Boolean constructionSiteDelivery;

    @Column(name = "secured_limited_access_pickup")
    private Boolean securedLimitedAccessPickup;

    @Column(name = "secured_limited_access_delivery")
    private Boolean securedLimitedAccessDelivery;

    @Column(name = "overlength_12_to_20")
    private Boolean overlength12to20;

    @Column(name = "overlength_20_to_28")
    private Boolean overlength20to28;

    @Column(name = "mines_or_ski_resorts_delivery")
    private Boolean minesOrSkiResortsDelivery;

    @Column(name = "self_storage_delivery")
    private Boolean selfStorageDelivery;

    @Column(name = "protect_from_freezing")
    private Boolean protectFromFreezing;

    @Column(name = "hazardous_materials")
    private Boolean hazardousMaterials;

    @Column(name = "notification_prior_to_delivery")
    private Boolean notificationPriorToDelivery;

    @Column(name = "notification_prior_to_pickup")
    private Boolean notificationPriorToPickup;

    @Column(name = "add_l_cargo_liability", precision=10, scale=2)
    private BigDecimal addLCargoLiability;

    @Column(name = "collect_on_delivery", precision=10, scale=2)
    private BigDecimal collectOnDelivery;

    @Column(name = "special_instructions")
    private String specialInstructions;

    @ManyToOne
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isArrivalNotification() {
        return arrivalNotification;
    }

    public Accessorials arrivalNotification(Boolean arrivalNotification) {
        this.arrivalNotification = arrivalNotification;
        return this;
    }

    public void setArrivalNotification(Boolean arrivalNotification) {
        this.arrivalNotification = arrivalNotification;
    }

    public Boolean isAppointment() {
        return appointment;
    }

    public Accessorials appointment(Boolean appointment) {
        this.appointment = appointment;
        return this;
    }

    public void setAppointment(Boolean appointment) {
        this.appointment = appointment;
    }

    public Boolean isLiftgatePickup() {
        return liftgatePickup;
    }

    public Accessorials liftgatePickup(Boolean liftgatePickup) {
        this.liftgatePickup = liftgatePickup;
        return this;
    }

    public void setLiftgatePickup(Boolean liftgatePickup) {
        this.liftgatePickup = liftgatePickup;
    }

    public Boolean isLiftgateDelivery() {
        return liftgateDelivery;
    }

    public Accessorials liftgateDelivery(Boolean liftgateDelivery) {
        this.liftgateDelivery = liftgateDelivery;
        return this;
    }

    public void setLiftgateDelivery(Boolean liftgateDelivery) {
        this.liftgateDelivery = liftgateDelivery;
    }

    public Boolean isResidentialNoncommercialPickup() {
        return residentialNoncommercialPickup;
    }

    public Accessorials residentialNoncommercialPickup(Boolean residentialNoncommercialPickup) {
        this.residentialNoncommercialPickup = residentialNoncommercialPickup;
        return this;
    }

    public void setResidentialNoncommercialPickup(Boolean residentialNoncommercialPickup) {
        this.residentialNoncommercialPickup = residentialNoncommercialPickup;
    }

    public Boolean isResidentialNoncommercialDelivery() {
        return residentialNoncommercialDelivery;
    }

    public Accessorials residentialNoncommercialDelivery(Boolean residentialNoncommercialDelivery) {
        this.residentialNoncommercialDelivery = residentialNoncommercialDelivery;
        return this;
    }

    public void setResidentialNoncommercialDelivery(Boolean residentialNoncommercialDelivery) {
        this.residentialNoncommercialDelivery = residentialNoncommercialDelivery;
    }

    public Boolean isInsidePickup() {
        return insidePickup;
    }

    public Accessorials insidePickup(Boolean insidePickup) {
        this.insidePickup = insidePickup;
        return this;
    }

    public void setInsidePickup(Boolean insidePickup) {
        this.insidePickup = insidePickup;
    }

    public Boolean isInsideDelivery() {
        return insideDelivery;
    }

    public Accessorials insideDelivery(Boolean insideDelivery) {
        this.insideDelivery = insideDelivery;
        return this;
    }

    public void setInsideDelivery(Boolean insideDelivery) {
        this.insideDelivery = insideDelivery;
    }

    public Boolean isTradeshowPickup() {
        return tradeshowPickup;
    }

    public Accessorials tradeshowPickup(Boolean tradeshowPickup) {
        this.tradeshowPickup = tradeshowPickup;
        return this;
    }

    public void setTradeshowPickup(Boolean tradeshowPickup) {
        this.tradeshowPickup = tradeshowPickup;
    }

    public Boolean isTradeshowDelivery() {
        return tradeshowDelivery;
    }

    public Accessorials tradeshowDelivery(Boolean tradeshowDelivery) {
        this.tradeshowDelivery = tradeshowDelivery;
        return this;
    }

    public void setTradeshowDelivery(Boolean tradeshowDelivery) {
        this.tradeshowDelivery = tradeshowDelivery;
    }

    public Boolean isAirportPickup() {
        return airportPickup;
    }

    public Accessorials airportPickup(Boolean airportPickup) {
        this.airportPickup = airportPickup;
        return this;
    }

    public void setAirportPickup(Boolean airportPickup) {
        this.airportPickup = airportPickup;
    }

    public Boolean isAirportDelivery() {
        return airportDelivery;
    }

    public Accessorials airportDelivery(Boolean airportDelivery) {
        this.airportDelivery = airportDelivery;
        return this;
    }

    public void setAirportDelivery(Boolean airportDelivery) {
        this.airportDelivery = airportDelivery;
    }

    public Boolean isSchoolsCollegesChurchesPickup() {
        return schoolsCollegesChurchesPickup;
    }

    public Accessorials schoolsCollegesChurchesPickup(Boolean schoolsCollegesChurchesPickup) {
        this.schoolsCollegesChurchesPickup = schoolsCollegesChurchesPickup;
        return this;
    }

    public void setSchoolsCollegesChurchesPickup(Boolean schoolsCollegesChurchesPickup) {
        this.schoolsCollegesChurchesPickup = schoolsCollegesChurchesPickup;
    }

    public Boolean isSchoolsCollegesChurchesDelivery() {
        return schoolsCollegesChurchesDelivery;
    }

    public Accessorials schoolsCollegesChurchesDelivery(Boolean schoolsCollegesChurchesDelivery) {
        this.schoolsCollegesChurchesDelivery = schoolsCollegesChurchesDelivery;
        return this;
    }

    public void setSchoolsCollegesChurchesDelivery(Boolean schoolsCollegesChurchesDelivery) {
        this.schoolsCollegesChurchesDelivery = schoolsCollegesChurchesDelivery;
    }

    public Boolean isConstructionSitePickup() {
        return constructionSitePickup;
    }

    public Accessorials constructionSitePickup(Boolean constructionSitePickup) {
        this.constructionSitePickup = constructionSitePickup;
        return this;
    }

    public void setConstructionSitePickup(Boolean constructionSitePickup) {
        this.constructionSitePickup = constructionSitePickup;
    }

    public Boolean isConstructionSiteDelivery() {
        return constructionSiteDelivery;
    }

    public Accessorials constructionSiteDelivery(Boolean constructionSiteDelivery) {
        this.constructionSiteDelivery = constructionSiteDelivery;
        return this;
    }

    public void setConstructionSiteDelivery(Boolean constructionSiteDelivery) {
        this.constructionSiteDelivery = constructionSiteDelivery;
    }

    public Boolean isSecuredLimitedAccessPickup() {
        return securedLimitedAccessPickup;
    }

    public Accessorials securedLimitedAccessPickup(Boolean securedLimitedAccessPickup) {
        this.securedLimitedAccessPickup = securedLimitedAccessPickup;
        return this;
    }

    public void setSecuredLimitedAccessPickup(Boolean securedLimitedAccessPickup) {
        this.securedLimitedAccessPickup = securedLimitedAccessPickup;
    }

    public Boolean isSecuredLimitedAccessDelivery() {
        return securedLimitedAccessDelivery;
    }

    public Accessorials securedLimitedAccessDelivery(Boolean securedLimitedAccessDelivery) {
        this.securedLimitedAccessDelivery = securedLimitedAccessDelivery;
        return this;
    }

    public void setSecuredLimitedAccessDelivery(Boolean securedLimitedAccessDelivery) {
        this.securedLimitedAccessDelivery = securedLimitedAccessDelivery;
    }

    public Boolean isOverlength12to20() {
        return overlength12to20;
    }

    public Accessorials overlength12to20(Boolean overlength12to20) {
        this.overlength12to20 = overlength12to20;
        return this;
    }

    public void setOverlength12to20(Boolean overlength12to20) {
        this.overlength12to20 = overlength12to20;
    }

    public Boolean isOverlength20to28() {
        return overlength20to28;
    }

    public Accessorials overlength20to28(Boolean overlength20to28) {
        this.overlength20to28 = overlength20to28;
        return this;
    }

    public void setOverlength20to28(Boolean overlength20to28) {
        this.overlength20to28 = overlength20to28;
    }

    public Boolean isMinesOrSkiResortsDelivery() {
        return minesOrSkiResortsDelivery;
    }

    public Accessorials minesOrSkiResortsDelivery(Boolean minesOrSkiResortsDelivery) {
        this.minesOrSkiResortsDelivery = minesOrSkiResortsDelivery;
        return this;
    }

    public void setMinesOrSkiResortsDelivery(Boolean minesOrSkiResortsDelivery) {
        this.minesOrSkiResortsDelivery = minesOrSkiResortsDelivery;
    }

    public Boolean isSelfStorageDelivery() {
        return selfStorageDelivery;
    }

    public Accessorials selfStorageDelivery(Boolean selfStorageDelivery) {
        this.selfStorageDelivery = selfStorageDelivery;
        return this;
    }

    public void setSelfStorageDelivery(Boolean selfStorageDelivery) {
        this.selfStorageDelivery = selfStorageDelivery;
    }

    public Boolean isProtectFromFreezing() {
        return protectFromFreezing;
    }

    public Accessorials protectFromFreezing(Boolean protectFromFreezing) {
        this.protectFromFreezing = protectFromFreezing;
        return this;
    }

    public void setProtectFromFreezing(Boolean protectFromFreezing) {
        this.protectFromFreezing = protectFromFreezing;
    }

    public Boolean isHazardousMaterials() {
        return hazardousMaterials;
    }

    public Accessorials hazardousMaterials(Boolean hazardousMaterials) {
        this.hazardousMaterials = hazardousMaterials;
        return this;
    }

    public void setHazardousMaterials(Boolean hazardousMaterials) {
        this.hazardousMaterials = hazardousMaterials;
    }

    public Boolean isNotificationPriorToDelivery() {
        return notificationPriorToDelivery;
    }

    public Accessorials notificationPriorToDelivery(Boolean notificationPriorToDelivery) {
        this.notificationPriorToDelivery = notificationPriorToDelivery;
        return this;
    }

    public void setNotificationPriorToDelivery(Boolean notificationPriorToDelivery) {
        this.notificationPriorToDelivery = notificationPriorToDelivery;
    }

    public Boolean isNotificationPriorToPickup() {
        return notificationPriorToPickup;
    }

    public Accessorials notificationPriorToPickup(Boolean notificationPriorToPickup) {
        this.notificationPriorToPickup = notificationPriorToPickup;
        return this;
    }

    public void setNotificationPriorToPickup(Boolean notificationPriorToPickup) {
        this.notificationPriorToPickup = notificationPriorToPickup;
    }

    public BigDecimal getAddLCargoLiability() {
        return addLCargoLiability;
    }

    public Accessorials addLCargoLiability(BigDecimal addLCargoLiability) {
        this.addLCargoLiability = addLCargoLiability;
        return this;
    }

    public void setAddLCargoLiability(BigDecimal addLCargoLiability) {
        this.addLCargoLiability = addLCargoLiability;
    }

    public BigDecimal getCollectOnDelivery() {
        return collectOnDelivery;
    }

    public Accessorials collectOnDelivery(BigDecimal collectOnDelivery) {
        this.collectOnDelivery = collectOnDelivery;
        return this;
    }

    public void setCollectOnDelivery(BigDecimal collectOnDelivery) {
        this.collectOnDelivery = collectOnDelivery;
    }

    public String getSpecialInstructions() {
        return specialInstructions;
    }

    public Accessorials specialInstructions(String specialInstructions) {
        this.specialInstructions = specialInstructions;
        return this;
    }

    public void setSpecialInstructions(String specialInstructions) {
        this.specialInstructions = specialInstructions;
    }

    public User getUser() {
        return user;
    }

    public Accessorials user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Accessorials accessorials = (Accessorials) o;
        if (accessorials.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), accessorials.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Accessorials{" +
            "id=" + getId() +
            ", arrivalNotification='" + isArrivalNotification() + "'" +
            ", appointment='" + isAppointment() + "'" +
            ", liftgatePickup='" + isLiftgatePickup() + "'" +
            ", liftgateDelivery='" + isLiftgateDelivery() + "'" +
            ", residentialNoncommercialPickup='" + isResidentialNoncommercialPickup() + "'" +
            ", residentialNoncommercialDelivery='" + isResidentialNoncommercialDelivery() + "'" +
            ", insidePickup='" + isInsidePickup() + "'" +
            ", insideDelivery='" + isInsideDelivery() + "'" +
            ", tradeshowPickup='" + isTradeshowPickup() + "'" +
            ", tradeshowDelivery='" + isTradeshowDelivery() + "'" +
            ", airportPickup='" + isAirportPickup() + "'" +
            ", airportDelivery='" + isAirportDelivery() + "'" +
            ", schoolsCollegesChurchesPickup='" + isSchoolsCollegesChurchesPickup() + "'" +
            ", schoolsCollegesChurchesDelivery='" + isSchoolsCollegesChurchesDelivery() + "'" +
            ", constructionSitePickup='" + isConstructionSitePickup() + "'" +
            ", constructionSiteDelivery='" + isConstructionSiteDelivery() + "'" +
            ", securedLimitedAccessPickup='" + isSecuredLimitedAccessPickup() + "'" +
            ", securedLimitedAccessDelivery='" + isSecuredLimitedAccessDelivery() + "'" +
            ", overlength12to20='" + isOverlength12to20() + "'" +
            ", overlength20to28='" + isOverlength20to28() + "'" +
            ", minesOrSkiResortsDelivery='" + isMinesOrSkiResortsDelivery() + "'" +
            ", selfStorageDelivery='" + isSelfStorageDelivery() + "'" +
            ", protectFromFreezing='" + isProtectFromFreezing() + "'" +
            ", hazardousMaterials='" + isHazardousMaterials() + "'" +
            ", notificationPriorToDelivery='" + isNotificationPriorToDelivery() + "'" +
            ", notificationPriorToPickup='" + isNotificationPriorToPickup() + "'" +
            ", addLCargoLiability='" + getAddLCargoLiability() + "'" +
            ", collectOnDelivery='" + getCollectOnDelivery() + "'" +
            ", specialInstructions='" + getSpecialInstructions() + "'" +
            "}";
    }
}
