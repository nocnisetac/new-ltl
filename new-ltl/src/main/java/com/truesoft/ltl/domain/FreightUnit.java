package com.truesoft.ltl.domain;

import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A FreightUnit.
 */
@Entity
@Table(name = "freight_unit")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class FreightUnit implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * actualClass needed only for Mexico destinations​
     */
    @ApiModelProperty(value = "actualClass needed only for Mexico destinations​")
    @Column(name = "actual_class")
    private Integer actualClass;

    @Column(name = "dim_l")
    private Integer dimL;

    @Column(name = "dim_w")
    private Integer dimW;

    @Column(name = "dim_h")
    private Integer dimH;

    @Column(name = "dim_unit")
    private String dimUnit;

    @Column(name = "nmfc")
    private Integer nmfc;

    @Column(name = "nmfc_sub")
    private Integer nmfcSub;

    @Column(name = "number_of_units")
    private Integer numberOfUnits;

    @Column(name = "rated_class")
    private Integer ratedClass;

    @NotNull
    @Column(name = "weight", nullable = false)
    private Integer weight;

    @ManyToOne
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getActualClass() {
        return actualClass;
    }

    public FreightUnit actualClass(Integer actualClass) {
        this.actualClass = actualClass;
        return this;
    }

    public void setActualClass(Integer actualClass) {
        this.actualClass = actualClass;
    }

    public Integer getDimL() {
        return dimL;
    }

    public FreightUnit dimL(Integer dimL) {
        this.dimL = dimL;
        return this;
    }

    public void setDimL(Integer dimL) {
        this.dimL = dimL;
    }

    public Integer getDimW() {
        return dimW;
    }

    public FreightUnit dimW(Integer dimW) {
        this.dimW = dimW;
        return this;
    }

    public void setDimW(Integer dimW) {
        this.dimW = dimW;
    }

    public Integer getDimH() {
        return dimH;
    }

    public FreightUnit dimH(Integer dimH) {
        this.dimH = dimH;
        return this;
    }

    public void setDimH(Integer dimH) {
        this.dimH = dimH;
    }

    public String getDimUnit() {
        return dimUnit;
    }

    public FreightUnit dimUnit(String dimUnit) {
        this.dimUnit = dimUnit;
        return this;
    }

    public void setDimUnit(String dimUnit) {
        this.dimUnit = dimUnit;
    }

    public Integer getNmfc() {
        return nmfc;
    }

    public FreightUnit nmfc(Integer nmfc) {
        this.nmfc = nmfc;
        return this;
    }

    public void setNmfc(Integer nmfc) {
        this.nmfc = nmfc;
    }

    public Integer getNmfcSub() {
        return nmfcSub;
    }

    public FreightUnit nmfcSub(Integer nmfcSub) {
        this.nmfcSub = nmfcSub;
        return this;
    }

    public void setNmfcSub(Integer nmfcSub) {
        this.nmfcSub = nmfcSub;
    }

    public Integer getNumberOfUnits() {
        return numberOfUnits;
    }

    public FreightUnit numberOfUnits(Integer numberOfUnits) {
        this.numberOfUnits = numberOfUnits;
        return this;
    }

    public void setNumberOfUnits(Integer numberOfUnits) {
        this.numberOfUnits = numberOfUnits;
    }

    public Integer getRatedClass() {
        return ratedClass;
    }

    public FreightUnit ratedClass(Integer ratedClass) {
        this.ratedClass = ratedClass;
        return this;
    }

    public void setRatedClass(Integer ratedClass) {
        this.ratedClass = ratedClass;
    }

    public Integer getWeight() {
        return weight;
    }

    public FreightUnit weight(Integer weight) {
        this.weight = weight;
        return this;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public User getUser() {
        return user;
    }

    public FreightUnit user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FreightUnit freightUnit = (FreightUnit) o;
        if (freightUnit.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), freightUnit.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "FreightUnit{" +
            "id=" + getId() +
            ", actualClass='" + getActualClass() + "'" +
            ", dimL='" + getDimL() + "'" +
            ", dimW='" + getDimW() + "'" +
            ", dimH='" + getDimH() + "'" +
            ", dimUnit='" + getDimUnit() + "'" +
            ", nmfc='" + getNmfc() + "'" +
            ", nmfcSub='" + getNmfcSub() + "'" +
            ", numberOfUnits='" + getNumberOfUnits() + "'" +
            ", ratedClass='" + getRatedClass() + "'" +
            ", weight='" + getWeight() + "'" +
            "}";
    }
}
