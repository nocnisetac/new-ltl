(function() {
    'use strict';

    angular
        .module('newLtlApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
