(function() {
    'use strict';

    angular
        .module('newLtlApp')
        .controller('AddressBookDetailController', AddressBookDetailController);

    AddressBookDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'AddressBook', 'User', 'PostalCode'];

    function AddressBookDetailController($scope, $rootScope, $stateParams, previousState, entity, AddressBook, User, PostalCode) {
        var vm = this;

        vm.addressBook = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('newLtlApp:addressBookUpdate', function(event, result) {
            vm.addressBook = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
