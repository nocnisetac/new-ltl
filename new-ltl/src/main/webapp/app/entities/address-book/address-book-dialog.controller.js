(function() {
    'use strict';

    angular
        .module('newLtlApp')
        .controller('AddressBookDialogController', AddressBookDialogController);

    AddressBookDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'AddressBook', 'User', 'PostalCode'];

    function AddressBookDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, AddressBook, User, PostalCode) {
        var vm = this;

        vm.addressBook = entity;
        vm.clear = clear;
        vm.save = save;
        vm.users = User.query();
        vm.postalcodes = PostalCode.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.addressBook.id !== null) {
                AddressBook.update(vm.addressBook, onSaveSuccess, onSaveError);
            } else {
                AddressBook.save(vm.addressBook, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newLtlApp:addressBookUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
