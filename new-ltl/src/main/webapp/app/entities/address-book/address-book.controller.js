(function() {
    'use strict';

    angular
        .module('newLtlApp')
        .controller('AddressBookController', AddressBookController);

    AddressBookController.$inject = ['AddressBook'];

    function AddressBookController(AddressBook) {

        var vm = this;

        vm.addressBooks = [];

        loadAll();

        function loadAll() {
            AddressBook.query(function(result) {
                vm.addressBooks = result;
                vm.searchQuery = null;
            });
        }
    }
})();
