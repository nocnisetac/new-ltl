(function() {
    'use strict';

    angular
        .module('newLtlApp')
        .controller('PostalCodeDetailController', PostalCodeDetailController);

    PostalCodeDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'PostalCode'];

    function PostalCodeDetailController($scope, $rootScope, $stateParams, previousState, entity, PostalCode) {
        var vm = this;

        vm.postalCode = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('newLtlApp:postalCodeUpdate', function(event, result) {
            vm.postalCode = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
