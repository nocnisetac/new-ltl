(function() {
    'use strict';

    angular
        .module('newLtlApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('postal-code', {
            parent: 'entity',
            url: '/postal-code?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'newLtlApp.postalCode.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/postal-code/postal-codes.html',
                    controller: 'PostalCodeController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('postalCode');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('postal-code-detail', {
            parent: 'postal-code',
            url: '/postal-code/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'newLtlApp.postalCode.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/postal-code/postal-code-detail.html',
                    controller: 'PostalCodeDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('postalCode');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'PostalCode', function($stateParams, PostalCode) {
                    return PostalCode.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'postal-code',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('postal-code-detail.edit', {
            parent: 'postal-code-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/postal-code/postal-code-dialog.html',
                    controller: 'PostalCodeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['PostalCode', function(PostalCode) {
                            return PostalCode.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('postal-code.new', {
            parent: 'postal-code',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/postal-code/postal-code-dialog.html',
                    controller: 'PostalCodeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                zipCode: null,
                                state: null,
                                city: null,
                                county: null,
                                stateFips: null,
                                countyFip: null,
                                latitude: null,
                                longitude: null,
                                preference: null,
                                type: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('postal-code', null, { reload: 'postal-code' });
                }, function() {
                    $state.go('postal-code');
                });
            }]
        })
        .state('postal-code.edit', {
            parent: 'postal-code',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/postal-code/postal-code-dialog.html',
                    controller: 'PostalCodeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['PostalCode', function(PostalCode) {
                            return PostalCode.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('postal-code', null, { reload: 'postal-code' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('postal-code.delete', {
            parent: 'postal-code',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/postal-code/postal-code-delete-dialog.html',
                    controller: 'PostalCodeDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['PostalCode', function(PostalCode) {
                            return PostalCode.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('postal-code', null, { reload: 'postal-code' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
