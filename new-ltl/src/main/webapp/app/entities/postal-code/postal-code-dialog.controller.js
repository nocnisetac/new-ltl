(function() {
    'use strict';

    angular
        .module('newLtlApp')
        .controller('PostalCodeDialogController', PostalCodeDialogController);

    PostalCodeDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'PostalCode'];

    function PostalCodeDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, PostalCode) {
        var vm = this;

        vm.postalCode = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.postalCode.id !== null) {
                PostalCode.update(vm.postalCode, onSaveSuccess, onSaveError);
            } else {
                PostalCode.save(vm.postalCode, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newLtlApp:postalCodeUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
