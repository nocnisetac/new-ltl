(function() {
    'use strict';
    angular
        .module('newLtlApp')
        .factory('Accessorials', Accessorials);

    Accessorials.$inject = ['$resource'];

    function Accessorials ($resource) {
        var resourceUrl =  'api/accessorials/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
