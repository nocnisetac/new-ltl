(function() {
    'use strict';

    angular
        .module('newLtlApp')
        .controller('AccessorialsController', AccessorialsController);

    AccessorialsController.$inject = ['Accessorials'];

    function AccessorialsController(Accessorials) {

        var vm = this;

        vm.accessorials = [];

        loadAll();

        function loadAll() {
            Accessorials.query(function(result) {
                vm.accessorials = result;
                vm.searchQuery = null;
            });
        }
    }
})();
