(function() {
    'use strict';

    angular
        .module('newLtlApp')
        .controller('AccessorialsDetailController', AccessorialsDetailController);

    AccessorialsDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Accessorials', 'User'];

    function AccessorialsDetailController($scope, $rootScope, $stateParams, previousState, entity, Accessorials, User) {
        var vm = this;

        vm.accessorials = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('newLtlApp:accessorialsUpdate', function(event, result) {
            vm.accessorials = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
