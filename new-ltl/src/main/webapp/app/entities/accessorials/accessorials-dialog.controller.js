(function() {
    'use strict';

    angular
        .module('newLtlApp')
        .controller('AccessorialsDialogController', AccessorialsDialogController);

    AccessorialsDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Accessorials', 'User'];

    function AccessorialsDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Accessorials, User) {
        var vm = this;

        vm.accessorials = entity;
        vm.clear = clear;
        vm.save = save;
        vm.users = User.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.accessorials.id !== null) {
                Accessorials.update(vm.accessorials, onSaveSuccess, onSaveError);
            } else {
                Accessorials.save(vm.accessorials, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newLtlApp:accessorialsUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
