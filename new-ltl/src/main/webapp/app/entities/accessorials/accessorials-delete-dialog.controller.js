(function() {
    'use strict';

    angular
        .module('newLtlApp')
        .controller('AccessorialsDeleteController',AccessorialsDeleteController);

    AccessorialsDeleteController.$inject = ['$uibModalInstance', 'entity', 'Accessorials'];

    function AccessorialsDeleteController($uibModalInstance, entity, Accessorials) {
        var vm = this;

        vm.accessorials = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Accessorials.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
