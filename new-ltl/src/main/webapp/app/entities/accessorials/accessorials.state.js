(function() {
    'use strict';

    angular
        .module('newLtlApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('accessorials', {
            parent: 'entity',
            url: '/accessorials',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'newLtlApp.accessorials.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/accessorials/accessorials.html',
                    controller: 'AccessorialsController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('accessorials');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('accessorials-detail', {
            parent: 'accessorials',
            url: '/accessorials/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'newLtlApp.accessorials.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/accessorials/accessorials-detail.html',
                    controller: 'AccessorialsDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('accessorials');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Accessorials', function($stateParams, Accessorials) {
                    return Accessorials.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'accessorials',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('accessorials-detail.edit', {
            parent: 'accessorials-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/accessorials/accessorials-dialog.html',
                    controller: 'AccessorialsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Accessorials', function(Accessorials) {
                            return Accessorials.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('accessorials.new', {
            parent: 'accessorials',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/accessorials/accessorials-dialog.html',
                    controller: 'AccessorialsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                arrivalNotification: null,
                                appointment: null,
                                liftgatePickup: null,
                                liftgateDelivery: null,
                                residentialNoncommercialPickup: null,
                                residentialNoncommercialDelivery: null,
                                insidePickup: null,
                                insideDelivery: null,
                                tradeshowPickup: null,
                                tradeshowDelivery: null,
                                airportPickup: null,
                                airportDelivery: null,
                                schoolsCollegesChurchesPickup: null,
                                schoolsCollegesChurchesDelivery: null,
                                constructionSitePickup: null,
                                constructionSiteDelivery: null,
                                securedLimitedAccessPickup: null,
                                securedLimitedAccessDelivery: null,
                                overlength12to20: null,
                                overlength20to28: null,
                                minesOrSkiResortsDelivery: null,
                                selfStorageDelivery: null,
                                protectFromFreezing: null,
                                hazardousMaterials: null,
                                notificationPriorToDelivery: null,
                                notificationPriorToPickup: null,
                                addLCargoLiability: null,
                                collectOnDelivery: null,
                                specialInstructions: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('accessorials', null, { reload: 'accessorials' });
                }, function() {
                    $state.go('accessorials');
                });
            }]
        })
        .state('accessorials.edit', {
            parent: 'accessorials',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/accessorials/accessorials-dialog.html',
                    controller: 'AccessorialsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Accessorials', function(Accessorials) {
                            return Accessorials.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('accessorials', null, { reload: 'accessorials' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('accessorials.delete', {
            parent: 'accessorials',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/accessorials/accessorials-delete-dialog.html',
                    controller: 'AccessorialsDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Accessorials', function(Accessorials) {
                            return Accessorials.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('accessorials', null, { reload: 'accessorials' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
