(function() {
    'use strict';

    angular
        .module('newLtlApp')
        .controller('ServiceCenterDeleteController',ServiceCenterDeleteController);

    ServiceCenterDeleteController.$inject = ['$uibModalInstance', 'entity', 'ServiceCenter'];

    function ServiceCenterDeleteController($uibModalInstance, entity, ServiceCenter) {
        var vm = this;

        vm.serviceCenter = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            ServiceCenter.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
