(function() {
    'use strict';

    angular
        .module('newLtlApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('service-center', {
            parent: 'entity',
            url: '/service-center',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'newLtlApp.serviceCenter.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/service-center/service-centers.html',
                    controller: 'ServiceCenterController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('serviceCenter');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('service-center-detail', {
            parent: 'service-center',
            url: '/service-center/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'newLtlApp.serviceCenter.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/service-center/service-center-detail.html',
                    controller: 'ServiceCenterDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('serviceCenter');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'ServiceCenter', function($stateParams, ServiceCenter) {
                    return ServiceCenter.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'service-center',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('service-center-detail.edit', {
            parent: 'service-center-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/service-center/service-center-dialog.html',
                    controller: 'ServiceCenterDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ServiceCenter', function(ServiceCenter) {
                            return ServiceCenter.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('service-center.new', {
            parent: 'service-center',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/service-center/service-center-dialog.html',
                    controller: 'ServiceCenterDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                address: null,
                                alphaCode: null,
                                cityStateZip: null,
                                country: null,
                                emailAddress: null,
                                fax: null,
                                manager: null,
                                name: null,
                                phone: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('service-center', null, { reload: 'service-center' });
                }, function() {
                    $state.go('service-center');
                });
            }]
        })
        .state('service-center.edit', {
            parent: 'service-center',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/service-center/service-center-dialog.html',
                    controller: 'ServiceCenterDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ServiceCenter', function(ServiceCenter) {
                            return ServiceCenter.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('service-center', null, { reload: 'service-center' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('service-center.delete', {
            parent: 'service-center',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/service-center/service-center-delete-dialog.html',
                    controller: 'ServiceCenterDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['ServiceCenter', function(ServiceCenter) {
                            return ServiceCenter.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('service-center', null, { reload: 'service-center' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
