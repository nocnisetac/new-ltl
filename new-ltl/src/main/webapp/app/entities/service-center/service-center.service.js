(function() {
    'use strict';
    angular
        .module('newLtlApp')
        .factory('ServiceCenter', ServiceCenter);

    ServiceCenter.$inject = ['$resource'];

    function ServiceCenter ($resource) {
        var resourceUrl =  'api/service-centers/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
