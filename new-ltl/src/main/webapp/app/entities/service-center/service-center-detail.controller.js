(function() {
    'use strict';

    angular
        .module('newLtlApp')
        .controller('ServiceCenterDetailController', ServiceCenterDetailController);

    ServiceCenterDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'ServiceCenter'];

    function ServiceCenterDetailController($scope, $rootScope, $stateParams, previousState, entity, ServiceCenter) {
        var vm = this;

        vm.serviceCenter = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('newLtlApp:serviceCenterUpdate', function(event, result) {
            vm.serviceCenter = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
