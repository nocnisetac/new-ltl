(function() {
    'use strict';

    angular
        .module('newLtlApp')
        .controller('ServiceCenterDialogController', ServiceCenterDialogController);

    ServiceCenterDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'ServiceCenter'];

    function ServiceCenterDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, ServiceCenter) {
        var vm = this;

        vm.serviceCenter = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.serviceCenter.id !== null) {
                ServiceCenter.update(vm.serviceCenter, onSaveSuccess, onSaveError);
            } else {
                ServiceCenter.save(vm.serviceCenter, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newLtlApp:serviceCenterUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
