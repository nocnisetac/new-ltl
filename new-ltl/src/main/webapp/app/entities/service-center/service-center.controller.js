(function() {
    'use strict';

    angular
        .module('newLtlApp')
        .controller('ServiceCenterController', ServiceCenterController);

    ServiceCenterController.$inject = ['ServiceCenter'];

    function ServiceCenterController(ServiceCenter) {

        var vm = this;

        vm.serviceCenters = [];

        loadAll();

        function loadAll() {
            ServiceCenter.query(function(result) {
                vm.serviceCenters = result;
                vm.searchQuery = null;
            });
        }
    }
})();
