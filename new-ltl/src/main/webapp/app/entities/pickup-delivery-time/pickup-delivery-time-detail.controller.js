(function() {
    'use strict';

    angular
        .module('newLtlApp')
        .controller('PickupDeliveryTimeDetailController', PickupDeliveryTimeDetailController);

    PickupDeliveryTimeDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'PickupDeliveryTime', 'User'];

    function PickupDeliveryTimeDetailController($scope, $rootScope, $stateParams, previousState, entity, PickupDeliveryTime, User) {
        var vm = this;

        vm.pickupDeliveryTime = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('newLtlApp:pickupDeliveryTimeUpdate', function(event, result) {
            vm.pickupDeliveryTime = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
