(function() {
    'use strict';
    angular
        .module('newLtlApp')
        .factory('PickupDeliveryTime', PickupDeliveryTime);

    PickupDeliveryTime.$inject = ['$resource', 'DateUtils'];

    function PickupDeliveryTime ($resource, DateUtils) {
        var resourceUrl =  'api/pickup-delivery-times/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.pickupDate = DateUtils.convertLocalDateFromServer(data.pickupDate);
                        data.deliveryDate = DateUtils.convertLocalDateFromServer(data.deliveryDate);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.pickupDate = DateUtils.convertLocalDateToServer(copy.pickupDate);
                    copy.deliveryDate = DateUtils.convertLocalDateToServer(copy.deliveryDate);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.pickupDate = DateUtils.convertLocalDateToServer(copy.pickupDate);
                    copy.deliveryDate = DateUtils.convertLocalDateToServer(copy.deliveryDate);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();
