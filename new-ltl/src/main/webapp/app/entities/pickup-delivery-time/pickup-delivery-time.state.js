(function() {
    'use strict';

    angular
        .module('newLtlApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('pickup-delivery-time', {
            parent: 'entity',
            url: '/pickup-delivery-time',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'newLtlApp.pickupDeliveryTime.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/pickup-delivery-time/pickup-delivery-times.html',
                    controller: 'PickupDeliveryTimeController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('pickupDeliveryTime');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('pickup-delivery-time-detail', {
            parent: 'pickup-delivery-time',
            url: '/pickup-delivery-time/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'newLtlApp.pickupDeliveryTime.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/pickup-delivery-time/pickup-delivery-time-detail.html',
                    controller: 'PickupDeliveryTimeDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('pickupDeliveryTime');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'PickupDeliveryTime', function($stateParams, PickupDeliveryTime) {
                    return PickupDeliveryTime.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'pickup-delivery-time',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('pickup-delivery-time-detail.edit', {
            parent: 'pickup-delivery-time-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/pickup-delivery-time/pickup-delivery-time-dialog.html',
                    controller: 'PickupDeliveryTimeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['PickupDeliveryTime', function(PickupDeliveryTime) {
                            return PickupDeliveryTime.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('pickup-delivery-time.new', {
            parent: 'pickup-delivery-time',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/pickup-delivery-time/pickup-delivery-time-dialog.html',
                    controller: 'PickupDeliveryTimeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                pickupDate: null,
                                pickupWindowFrom: null,
                                pickupWindowTo: null,
                                deliveryDate: null,
                                deliveryWindowFrom: null,
                                deliveryWindowTo: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('pickup-delivery-time', null, { reload: 'pickup-delivery-time' });
                }, function() {
                    $state.go('pickup-delivery-time');
                });
            }]
        })
        .state('pickup-delivery-time.edit', {
            parent: 'pickup-delivery-time',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/pickup-delivery-time/pickup-delivery-time-dialog.html',
                    controller: 'PickupDeliveryTimeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['PickupDeliveryTime', function(PickupDeliveryTime) {
                            return PickupDeliveryTime.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('pickup-delivery-time', null, { reload: 'pickup-delivery-time' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('pickup-delivery-time.delete', {
            parent: 'pickup-delivery-time',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/pickup-delivery-time/pickup-delivery-time-delete-dialog.html',
                    controller: 'PickupDeliveryTimeDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['PickupDeliveryTime', function(PickupDeliveryTime) {
                            return PickupDeliveryTime.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('pickup-delivery-time', null, { reload: 'pickup-delivery-time' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
