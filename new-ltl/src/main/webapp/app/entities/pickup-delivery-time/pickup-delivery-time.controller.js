(function() {
    'use strict';

    angular
        .module('newLtlApp')
        .controller('PickupDeliveryTimeController', PickupDeliveryTimeController);

    PickupDeliveryTimeController.$inject = ['PickupDeliveryTime'];

    function PickupDeliveryTimeController(PickupDeliveryTime) {

        var vm = this;

        vm.pickupDeliveryTimes = [];

        loadAll();

        function loadAll() {
            PickupDeliveryTime.query(function(result) {
                vm.pickupDeliveryTimes = result;
                vm.searchQuery = null;
            });
        }
    }
})();
