(function() {
    'use strict';

    angular
        .module('newLtlApp')
        .controller('PickupDeliveryTimeDeleteController',PickupDeliveryTimeDeleteController);

    PickupDeliveryTimeDeleteController.$inject = ['$uibModalInstance', 'entity', 'PickupDeliveryTime'];

    function PickupDeliveryTimeDeleteController($uibModalInstance, entity, PickupDeliveryTime) {
        var vm = this;

        vm.pickupDeliveryTime = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            PickupDeliveryTime.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
