(function() {
    'use strict';

    angular
        .module('newLtlApp')
        .controller('PickupDeliveryTimeDialogController', PickupDeliveryTimeDialogController);

    PickupDeliveryTimeDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'PickupDeliveryTime', 'User'];

    function PickupDeliveryTimeDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, PickupDeliveryTime, User) {
        var vm = this;

        vm.pickupDeliveryTime = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.users = User.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.pickupDeliveryTime.id !== null) {
                PickupDeliveryTime.update(vm.pickupDeliveryTime, onSaveSuccess, onSaveError);
            } else {
                PickupDeliveryTime.save(vm.pickupDeliveryTime, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newLtlApp:pickupDeliveryTimeUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.pickupDate = false;
        vm.datePickerOpenStatus.deliveryDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
