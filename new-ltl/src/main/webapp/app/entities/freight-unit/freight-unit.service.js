(function() {
    'use strict';
    angular
        .module('newLtlApp')
        .factory('FreightUnit', FreightUnit);

    FreightUnit.$inject = ['$resource'];

    function FreightUnit ($resource) {
        var resourceUrl =  'api/freight-units/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
