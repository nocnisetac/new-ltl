(function() {
    'use strict';

    angular
        .module('newLtlApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('freight-unit', {
            parent: 'entity',
            url: '/freight-unit',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'newLtlApp.freightUnit.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/freight-unit/freight-units.html',
                    controller: 'FreightUnitController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('freightUnit');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('freight-unit-detail', {
            parent: 'freight-unit',
            url: '/freight-unit/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'newLtlApp.freightUnit.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/freight-unit/freight-unit-detail.html',
                    controller: 'FreightUnitDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('freightUnit');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'FreightUnit', function($stateParams, FreightUnit) {
                    return FreightUnit.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'freight-unit',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('freight-unit-detail.edit', {
            parent: 'freight-unit-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/freight-unit/freight-unit-dialog.html',
                    controller: 'FreightUnitDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['FreightUnit', function(FreightUnit) {
                            return FreightUnit.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('freight-unit.new', {
            parent: 'freight-unit',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/freight-unit/freight-unit-dialog.html',
                    controller: 'FreightUnitDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                actualClass: null,
                                dimL: null,
                                dimW: null,
                                dimH: null,
                                dimUnit: null,
                                nmfc: null,
                                nmfcSub: null,
                                numberOfUnits: null,
                                ratedClass: null,
                                weight: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('freight-unit', null, { reload: 'freight-unit' });
                }, function() {
                    $state.go('freight-unit');
                });
            }]
        })
        .state('freight-unit.edit', {
            parent: 'freight-unit',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/freight-unit/freight-unit-dialog.html',
                    controller: 'FreightUnitDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['FreightUnit', function(FreightUnit) {
                            return FreightUnit.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('freight-unit', null, { reload: 'freight-unit' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('freight-unit.delete', {
            parent: 'freight-unit',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/freight-unit/freight-unit-delete-dialog.html',
                    controller: 'FreightUnitDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['FreightUnit', function(FreightUnit) {
                            return FreightUnit.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('freight-unit', null, { reload: 'freight-unit' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
