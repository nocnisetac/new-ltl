(function() {
    'use strict';

    angular
        .module('newLtlApp')
        .controller('FreightUnitDialogController', FreightUnitDialogController);

    FreightUnitDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'FreightUnit', 'User'];

    function FreightUnitDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, FreightUnit, User) {
        var vm = this;

        vm.freightUnit = entity;
        vm.clear = clear;
        vm.save = save;
        vm.users = User.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.freightUnit.id !== null) {
                FreightUnit.update(vm.freightUnit, onSaveSuccess, onSaveError);
            } else {
                FreightUnit.save(vm.freightUnit, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newLtlApp:freightUnitUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
