(function() {
    'use strict';

    angular
        .module('newLtlApp')
        .controller('FreightUnitDetailController', FreightUnitDetailController);

    FreightUnitDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'FreightUnit', 'User'];

    function FreightUnitDetailController($scope, $rootScope, $stateParams, previousState, entity, FreightUnit, User) {
        var vm = this;

        vm.freightUnit = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('newLtlApp:freightUnitUpdate', function(event, result) {
            vm.freightUnit = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
