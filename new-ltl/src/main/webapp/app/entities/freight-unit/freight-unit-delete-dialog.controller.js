(function() {
    'use strict';

    angular
        .module('newLtlApp')
        .controller('FreightUnitDeleteController',FreightUnitDeleteController);

    FreightUnitDeleteController.$inject = ['$uibModalInstance', 'entity', 'FreightUnit'];

    function FreightUnitDeleteController($uibModalInstance, entity, FreightUnit) {
        var vm = this;

        vm.freightUnit = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            FreightUnit.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
