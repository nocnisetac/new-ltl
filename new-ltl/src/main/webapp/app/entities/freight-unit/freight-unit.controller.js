(function() {
    'use strict';

    angular
        .module('newLtlApp')
        .controller('FreightUnitController', FreightUnitController);

    FreightUnitController.$inject = ['FreightUnit'];

    function FreightUnitController(FreightUnit) {

        var vm = this;

        vm.freightUnits = [];

        loadAll();

        function loadAll() {
            FreightUnit.query(function(result) {
                vm.freightUnits = result;
                vm.searchQuery = null;
            });
        }
    }
})();
